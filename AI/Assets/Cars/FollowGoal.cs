﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;

public class FollowGoal : MonoBehaviour
{
	[SerializeField] Transform goal;
	[SerializeField] float speed = 10f;
	[SerializeField] float rotationSpeed = 10f;
	[SerializeField] float accuracy = 0.5f;

	int currentWpIndex = 0;
	

	private void Update()
	{
		FollowWaypoints();
	}

	private void FollowWaypoints()
	{
		Vector3 lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
		Vector3 direction = lookAtGoal - transform.position;
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
		this.transform.Translate(0, 0, speed * Time.deltaTime);

	}


}
