﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Utility;

public class SimpleAICar : MonoBehaviour
{
	[SerializeField] Transform goal;
	[SerializeField] Text readout;
	[SerializeField] float rotationSpeed = 1f;
	[SerializeField] float accuracy = 0.5f;
	[SerializeField]  float acceleration = 5f;
	[SerializeField]  float deceleration = 5f;
	[SerializeField]  float minSpeed = 0.0f;
	[SerializeField]  float maxSpeed = 100.0f;
	[SerializeField]  float breakAngle = 20f;

	float speed = 10f;

	private void Update()
	{
		FollowWaypoints();
	}

	private void FollowWaypoints()
	{
		Vector3 lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
		Vector3 direction = lookAtGoal - transform.position;
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);

		AccelerationAndDeloration();

		this.transform.Translate(0, 0, speed);

		ViewingInfo();
	}

	private void AccelerationAndDeloration()
	{
		if (Vector3.Angle(goal.forward, transform.forward) > breakAngle && speed > maxSpeed/10)
		{
			speed = Mathf.Clamp(speed - (acceleration * Time.deltaTime), minSpeed, maxSpeed);
		}
		else
		{
			speed = Mathf.Clamp(speed + (acceleration * Time.deltaTime), minSpeed, maxSpeed);
		}
	}

	private void ViewingInfo()
	{
		if (readout == null) return;
		AnalogueSpeedConverter.ShowSpeed(speed, minSpeed, maxSpeed);
		readout.text = "" + (int)speed;
	}
}
