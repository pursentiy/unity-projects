﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionHandler : MonoBehaviour {

	public GameObject deathFX;

	private void OnTriggerEnter(Collider other)
	{
		StartDeathSequence();
		deathFX.SetActive(true);
	}

	private void StartDeathSequence()
	{
		gameObject.SendMessage("OnPlayerDeath");
		FindObjectOfType<LevelLoader>().RestartLevel(1f);
	}
}
