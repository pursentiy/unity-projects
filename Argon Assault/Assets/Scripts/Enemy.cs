﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public GameObject enemyDeathFX;
	public Transform parent;
	public float timeToWaitBeforeDestraction = 0.5f;
	public int score = 100;
	public int hits = 100;

	ScoreBoard scoreBoard;

	// Use this for initialization
	void Start () {
		AddNonTRiggerBoxCollider();
		scoreBoard = FindObjectOfType<ScoreBoard>();

	}

	private void AddNonTRiggerBoxCollider()
	{
		if (GetComponent<BoxCollider>() == null)
		{
			Collider boxCollider = gameObject.AddComponent<BoxCollider>();
			boxCollider.isTrigger = false;
		}
	}

	// Update is called once per frame
	void Update () {
		
	}

	private void OnParticleCollision(GameObject other)
	{

		hits--;
		if(hits <= 0)
		{
			Invoke("KillEnemy", timeToWaitBeforeDestraction);
			print("Enemy is dead");
		}
		
	}

	private void KillEnemy()
	{
		GameObject particlesDead = Instantiate(enemyDeathFX, transform.position, Quaternion.identity);
		particlesDead.transform.parent = parent;
		Destroy(gameObject);
		scoreBoard.UpdateScore(score);
		//Destroy(particlesDead, timeToWaitBeforeDestraction);
	}


}
