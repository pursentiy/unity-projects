﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour {

	[SerializeField] float secondsToWait = 3f;

	private int sceneIndex;

	void Start()
	{
		sceneIndex = SceneManager.sceneCountInBuildSettings;
	}

	public void LoadNextScene()
	{
		StartCoroutine(WaitForSeconds(secondsToWait, sceneIndex + 1));
	}

	public void RestartLevel(float seconds)
	{
		StartCoroutine(WaitForSeconds(seconds, sceneIndex-1));
	}

	private IEnumerator WaitForSeconds(float seconds, int index)
	{
		yield return new WaitForSeconds(secondsToWait);
		SceneManager.LoadScene(index);
	}
}
