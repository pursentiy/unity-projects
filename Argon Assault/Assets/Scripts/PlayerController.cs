﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour {

	float xThrow, yThrow;

	[Header("General")]
	[SerializeField] float xSpeed = 4f;
	[SerializeField] float ySpeed = 4f;
	[SerializeField] float Xrange = 10f;
	[SerializeField] float Yrange = 5f;
	[SerializeField] GameObject[] guns;

	[Header("Screen-position Based")]
	[SerializeField] float positionPitchFactor = 5f;
	[SerializeField] float controlPitchFactor = -5f;

	[SerializeField] float positionYawFactor = -5f;

	[SerializeField] float controlRollFactor = -30f;

	private bool isControlEnabled = true;

	// Update is called once per frame
	void Update ()
	{
		if (isControlEnabled)
		{
			ProcessRotation();
			ProcessTranslation();
			ProcessFiring();
		}
	}

	private void ProcessRotation()
	{
		float pitch = transform.localPosition.y * positionPitchFactor + yThrow * controlPitchFactor;
		float yaw = -transform.localPosition.x * positionYawFactor;
		float roll = xThrow * controlRollFactor;
		transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
	}

	private void ProcessTranslation()
	{
		xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
		yThrow = CrossPlatformInputManager.GetAxis("Vertical");

		float xOffset = xThrow * xSpeed * Time.deltaTime;
		float yOffset = yThrow * ySpeed * Time.deltaTime;

		float rawNewXPos = transform.localPosition.x + xOffset;
		float rawNewYPos = transform.localPosition.y + yOffset;

		float clampedXpos = Mathf.Clamp(rawNewXPos, -Xrange, Xrange);
		float clampedYpos = Mathf.Clamp(rawNewYPos, -Yrange, Yrange);

		//GetComponent<Rigidbody>().velocity = new Vector2(horizontalThrow*30, 0f);
		transform.localPosition = new Vector3(clampedXpos, clampedYpos, transform.localPosition.z);
	}

	private void ProcessFiring()
	{
		if(CrossPlatformInputManager.GetButton("Fire")){
			SetGunsActive(true);
		}
		else
		{
			SetGunsActive(false);
		}
	}

	private void SetGunsActive(bool isActive)
	{
		foreach (GameObject gun in guns)
		{
			var emissionModule = gun.GetComponent<ParticleSystem>().emission;
			emissionModule.enabled = isActive;
		}
	}



	public void OnPlayerDeath()
	{
		isControlEnabled = false;
		print("Dying");
	}
}
