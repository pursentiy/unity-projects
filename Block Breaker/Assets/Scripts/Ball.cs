﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Ball : MonoBehaviour {

	[SerializeField] Paddle paddle1;
	[SerializeField] private float xPush = 2f;
	[SerializeField] private float yPush = 15f;
	[SerializeField] AudioClip[] ballSounds;
	[SerializeField] float randomFactor = 0;

	Vector2 paddleToBallVector;
	Vector3 unityMousePos = new Vector3();


	bool hasStarted = false;

	// Chashed component referenced
	AudioSource audioSource;
	Rigidbody2D myRigidbody2D;


	// Use this for initialization
	void Start () {
		paddleToBallVector = transform.position - paddle1.transform.position;
		audioSource = GetComponent<AudioSource>();
		myRigidbody2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
	{

		//unityMousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
		//Debug.Log(unityMousePos);
		if (!hasStarted)
		{
			LockBallToPaddle();
			LaunchOnMouseClick();
		}
		else
		{
			LaunchOnMouseClick();
		}
	}

	private void LaunchOnMouseClick()
	{
		if (!hasStarted)
		{
			if (Input.GetMouseButtonDown(0))
			{
				myRigidbody2D.velocity = new Vector2(Random.Range(-xPush, xPush), yPush);
				hasStarted = true;
			}
		}
	}

	private void LockBallToPaddle()
	{
		Vector2 paddlePos = new Vector2(paddle1.transform.position.x, paddle1.transform.position.y);
		transform.position = paddlePos + paddleToBallVector;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		Vector2 velocityTweak = new Vector2
			(Random.Range(0f, randomFactor),
			Random.Range(0f, randomFactor));
		if (hasStarted)
		{
			AudioClip clip = ballSounds[Random.Range(0, ballSounds.Length)];
			audioSource.PlayOneShot(clip);
			myRigidbody2D.velocity += velocityTweak;
		}
	}

}
