﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {

	//config params
	[SerializeField] AudioClip breakSound;
	[SerializeField] GameObject blockSparklesVFX;
	[SerializeField] Sprite[] hitSprites;

	//cached reference
	Level level;
	private void Start()
	{
		CountBreakableBlocks();
	}

	//state variables
	[SerializeField] int timesHit = 0; //TODO only serialized for debug

	private void CountBreakableBlocks()
	{
		level = FindObjectOfType<Level>();
		if (tag == "Breakable")
		{
			level.CountBlocks();
		}
	}

	private void HandleHit()
	{
		timesHit++;
		int maxHits = hitSprites.Length + 1;
		if (timesHit >= maxHits)
		{
			DestroyBlock();
		}
		else
		{
			ShowNextSprite();
		}
	}


	private void ShowNextSprite()
	{
		int spriteIndex = timesHit - 1;
		if (hitSprites[spriteIndex] != null)
		{
			GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
		}
		else
		{
			Debug.Log("Block sprite is missing from an array " + gameObject.name);
		}
	}


	private void OnCollisionEnter2D(Collision2D collision)
	{
		if(tag == "Breakable")
		{
			HandleHit();
		}

	}

	private void DestroyBlock()
	{
		PlayBlockDestroySFX();
		//Debug.Log("Now the block amount is = " + GameObject.Find("Level").GetComponent<Level>().breakableBlocks);
		Destroy(gameObject);
		TriggerSparklesVFX();
		level.DecreaseBlocks();
	}

	private void PlayBlockDestroySFX()
	{
		FindObjectOfType<GameSession>().AddToScore();
		AudioSource.PlayClipAtPoint(breakSound, Camera.main.transform.position, 0.05f);
	}

	private void TriggerSparklesVFX()
	{
		GameObject sparkles = Instantiate(blockSparklesVFX, transform.position, transform.rotation);
		Destroy(sparkles, 2f);
	}
}
