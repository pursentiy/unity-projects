﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour {

	[SerializeField] public int breakableBlocks;
	SceneManeger sceneManeger;

	public void Start()
	{
		sceneManeger = FindObjectOfType<SceneManeger>();
	}

	public void CountBlocks()
	{
		breakableBlocks++;
	}

	public void DecreaseBlocks()
	{
		breakableBlocks--;
		if (breakableBlocks <= 0)
		{
			sceneManeger.LoadNextScene();
		}
		Debug.Log(breakableBlocks);
	}

}
