﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {

	[SerializeField] float minX = 1f;
	[SerializeField] float maxX = 15f;
	Vector3 unityPaddlePos = new Vector3();

	GameSession gameSession;
	Ball ball;

	[SerializeField] public float screenWidthInUnits = 16;

	// Use this for initialization
	void Start () {
		gameSession = FindObjectOfType<GameSession>();
		ball = FindObjectOfType<Ball>();
	}
	
	// Update is called once per frame
	void Update () {
		unityPaddlePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, 0.25f, 0));
		Vector2 paddlePos = new Vector2(Mathf.Clamp(GetXPos(), minX, maxX), 0.25f);
		transform.position = paddlePos;
	}

	private float GetXPos()
	{
		if (gameSession.IsAutoPlayEnable())
		{
			return ball.transform.position.x;
		}
		else
		{
			return unityPaddlePos.x;
		}
	}
}
