﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Ball
struct Ball_t2206666566;
// EnvMapAnimator
struct EnvMapAnimator_t1140999784;
// GameSession
struct GameSession_t4087811243;
// Level
struct Level_t2237665516;
// Paddle
struct Paddle_t4140593882;
// SceneManeger
struct SceneManeger_t385596978;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget>
struct List_1_t235857739;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// TMPro.Examples.Benchmark01
struct Benchmark01_t1571072624;
// TMPro.Examples.Benchmark01_UGUI
struct Benchmark01_UGUI_t3264177817;
// TMPro.Examples.ShaderPropAnimator
struct ShaderPropAnimator_t3617420994;
// TMPro.Examples.SkewTextExample
struct SkewTextExample_t3460249701;
// TMPro.Examples.TeleType
struct TeleType_t2409835159;
// TMPro.Examples.TextConsoleSimulator
struct TextConsoleSimulator_t3766250034;
// TMPro.Examples.TextMeshProFloatingText
struct TextMeshProFloatingText_t845872552;
// TMPro.Examples.VertexColorCycler
struct VertexColorCycler_t3003193665;
// TMPro.Examples.VertexJitter
struct VertexJitter_t4087429332;
// TMPro.Examples.VertexJitter/VertexAnim[]
struct VertexAnimU5BU5D_t1611656175;
// TMPro.Examples.VertexShakeA
struct VertexShakeA_t4262048139;
// TMPro.Examples.VertexShakeB
struct VertexShakeB_t1533164784;
// TMPro.Examples.VertexZoom
struct VertexZoom_t550798657;
// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0
struct U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008;
// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1
struct U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514;
// TMPro.Examples.WarpTextExample
struct WarpTextExample_t3821118074;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TMP_InputField
struct TMP_InputField_t1099764886;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t3365986247;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// TMPro.TMP_TextEventHandler
struct TMP_TextEventHandler_t1869054637;
// TMPro.TMP_TextEventHandler/CharacterSelectionEvent
struct CharacterSelectionEvent_t3109943174;
// TMPro.TMP_TextEventHandler/LineSelectionEvent
struct LineSelectionEvent_t2868010532;
// TMPro.TMP_TextEventHandler/LinkSelectionEvent
struct LinkSelectionEvent_t1590929858;
// TMPro.TMP_TextEventHandler/SpriteSelectionEvent
struct SpriteSelectionEvent_t2798445241;
// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct WordSelectionEvent_t1841909953;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t3598145122;
// TMPro.TextContainer
struct TextContainer_t97923372;
// TMPro.TextMeshPro
struct TextMeshPro_t2393593166;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.TrackableProperty
struct TrackableProperty_t3943537984;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t143221404;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.Font
struct Font_t1956802104;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Vector3[][]
struct Vector3U5BU5DU5BU5D_t546443028;




#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2216151886_H
#define U3CSTARTU3EC__ITERATOR0_T2216151886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2216151886  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$this
	Benchmark01_t1571072624 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24this_1)); }
	inline Benchmark01_t1571072624 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_t1571072624 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_t1571072624 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2216151886_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2622988697_H
#define U3CSTARTU3EC__ITERATOR0_T2622988697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2622988697  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01_UGUI TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$this
	Benchmark01_UGUI_t3264177817 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24this_1)); }
	inline Benchmark01_UGUI_t3264177817 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_UGUI_t3264177817 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_UGUI_t3264177817 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2622988697_H
#ifndef U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#define U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0
struct  U3CAnimatePropertiesU3Ec__Iterator0_t4041402054  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::<glowPower>__1
	float ___U3CglowPowerU3E__1_0;
	// TMPro.Examples.ShaderPropAnimator TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$this
	ShaderPropAnimator_t3617420994 * ___U24this_1;
	// System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CglowPowerU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U3CglowPowerU3E__1_0)); }
	inline float get_U3CglowPowerU3E__1_0() const { return ___U3CglowPowerU3E__1_0; }
	inline float* get_address_of_U3CglowPowerU3E__1_0() { return &___U3CglowPowerU3E__1_0; }
	inline void set_U3CglowPowerU3E__1_0(float value)
	{
		___U3CglowPowerU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24this_1)); }
	inline ShaderPropAnimator_t3617420994 * get_U24this_1() const { return ___U24this_1; }
	inline ShaderPropAnimator_t3617420994 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ShaderPropAnimator_t3617420994 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#ifndef U3CSTARTU3EC__ITERATOR0_T3341539328_H
#define U3CSTARTU3EC__ITERATOR0_T3341539328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TeleType/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3341539328  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_0;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<counter>__0
	int32_t ___U3CcounterU3E__0_1;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_2;
	// TMPro.Examples.TeleType TMPro.Examples.TeleType/<Start>c__Iterator0::$this
	TeleType_t2409835159 * ___U24this_3;
	// System.Object TMPro.Examples.TeleType/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean TMPro.Examples.TeleType/<Start>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U3CtotalVisibleCharactersU3E__0_0)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_0() const { return ___U3CtotalVisibleCharactersU3E__0_0; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_0() { return &___U3CtotalVisibleCharactersU3E__0_0; }
	inline void set_U3CtotalVisibleCharactersU3E__0_0(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U3CcounterU3E__0_1)); }
	inline int32_t get_U3CcounterU3E__0_1() const { return ___U3CcounterU3E__0_1; }
	inline int32_t* get_address_of_U3CcounterU3E__0_1() { return &___U3CcounterU3E__0_1; }
	inline void set_U3CcounterU3E__0_1(int32_t value)
	{
		___U3CcounterU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U3CvisibleCountU3E__0_2)); }
	inline int32_t get_U3CvisibleCountU3E__0_2() const { return ___U3CvisibleCountU3E__0_2; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_2() { return &___U3CvisibleCountU3E__0_2; }
	inline void set_U3CvisibleCountU3E__0_2(int32_t value)
	{
		___U3CvisibleCountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24this_3)); }
	inline TeleType_t2409835159 * get_U24this_3() const { return ___U24this_3; }
	inline TeleType_t2409835159 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(TeleType_t2409835159 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T3341539328_H
#ifndef U3CREVEALCHARACTERSU3EC__ITERATOR0_T860191687_H
#define U3CREVEALCHARACTERSU3EC__ITERATOR0_T860191687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0
struct  U3CRevealCharactersU3Ec__Iterator0_t860191687  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// TMPro.TMP_TextInfo TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__0_1;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_2;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_3;
	// TMPro.Examples.TextConsoleSimulator TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$this
	TextConsoleSimulator_t3766250034 * ___U24this_4;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U3CtextInfoU3E__0_1)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__0_1() const { return ___U3CtextInfoU3E__0_1; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__0_1() { return &___U3CtextInfoU3E__0_1; }
	inline void set_U3CtextInfoU3E__0_1(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U3CtotalVisibleCharactersU3E__0_2)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_2() const { return ___U3CtotalVisibleCharactersU3E__0_2; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_2() { return &___U3CtotalVisibleCharactersU3E__0_2; }
	inline void set_U3CtotalVisibleCharactersU3E__0_2(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U3CvisibleCountU3E__0_3)); }
	inline int32_t get_U3CvisibleCountU3E__0_3() const { return ___U3CvisibleCountU3E__0_3; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_3() { return &___U3CvisibleCountU3E__0_3; }
	inline void set_U3CvisibleCountU3E__0_3(int32_t value)
	{
		___U3CvisibleCountU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24this_4)); }
	inline TextConsoleSimulator_t3766250034 * get_U24this_4() const { return ___U24this_4; }
	inline TextConsoleSimulator_t3766250034 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TextConsoleSimulator_t3766250034 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREVEALCHARACTERSU3EC__ITERATOR0_T860191687_H
#ifndef U3CREVEALWORDSU3EC__ITERATOR1_T1343183262_H
#define U3CREVEALWORDSU3EC__ITERATOR1_T1343183262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1
struct  U3CRevealWordsU3Ec__Iterator1_t1343183262  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<totalWordCount>__0
	int32_t ___U3CtotalWordCountU3E__0_1;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_2;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<counter>__0
	int32_t ___U3CcounterU3E__0_3;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<currentWord>__0
	int32_t ___U3CcurrentWordU3E__0_4;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_5;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_U3CtotalWordCountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CtotalWordCountU3E__0_1)); }
	inline int32_t get_U3CtotalWordCountU3E__0_1() const { return ___U3CtotalWordCountU3E__0_1; }
	inline int32_t* get_address_of_U3CtotalWordCountU3E__0_1() { return &___U3CtotalWordCountU3E__0_1; }
	inline void set_U3CtotalWordCountU3E__0_1(int32_t value)
	{
		___U3CtotalWordCountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CtotalVisibleCharactersU3E__0_2)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_2() const { return ___U3CtotalVisibleCharactersU3E__0_2; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_2() { return &___U3CtotalVisibleCharactersU3E__0_2; }
	inline void set_U3CtotalVisibleCharactersU3E__0_2(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CcounterU3E__0_3)); }
	inline int32_t get_U3CcounterU3E__0_3() const { return ___U3CcounterU3E__0_3; }
	inline int32_t* get_address_of_U3CcounterU3E__0_3() { return &___U3CcounterU3E__0_3; }
	inline void set_U3CcounterU3E__0_3(int32_t value)
	{
		___U3CcounterU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentWordU3E__0_4() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CcurrentWordU3E__0_4)); }
	inline int32_t get_U3CcurrentWordU3E__0_4() const { return ___U3CcurrentWordU3E__0_4; }
	inline int32_t* get_address_of_U3CcurrentWordU3E__0_4() { return &___U3CcurrentWordU3E__0_4; }
	inline void set_U3CcurrentWordU3E__0_4(int32_t value)
	{
		___U3CcurrentWordU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_5() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CvisibleCountU3E__0_5)); }
	inline int32_t get_U3CvisibleCountU3E__0_5() const { return ___U3CvisibleCountU3E__0_5; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_5() { return &___U3CvisibleCountU3E__0_5; }
	inline void set_U3CvisibleCountU3E__0_5(int32_t value)
	{
		___U3CvisibleCountU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREVEALWORDSU3EC__ITERATOR1_T1343183262_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ANONSTOREY1_T446847514_H
#define U3CANIMATEVERTEXCOLORSU3EC__ANONSTOREY1_T446847514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1
struct  U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Single> TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1::modifiedCharScale
	List_1_t2869341516 * ___modifiedCharScale_0;
	// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1::<>f__ref$0
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_modifiedCharScale_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514, ___modifiedCharScale_0)); }
	inline List_1_t2869341516 * get_modifiedCharScale_0() const { return ___modifiedCharScale_0; }
	inline List_1_t2869341516 ** get_address_of_modifiedCharScale_0() { return &___modifiedCharScale_0; }
	inline void set_modifiedCharScale_0(List_1_t2869341516 * value)
	{
		___modifiedCharScale_0 = value;
		Il2CppCodeGenWriteBarrier((&___modifiedCharScale_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514, ___U3CU3Ef__refU240_1)); }
	inline U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ANONSTOREY1_T446847514_H
#ifndef TRACKABLEPROPERTY_T3943537984_H
#define TRACKABLEPROPERTY_T3943537984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty
struct  TrackableProperty_t3943537984  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget> UnityEngine.Analytics.TrackableProperty::m_Fields
	List_1_t235857739 * ___m_Fields_1;

public:
	inline static int32_t get_offset_of_m_Fields_1() { return static_cast<int32_t>(offsetof(TrackableProperty_t3943537984, ___m_Fields_1)); }
	inline List_1_t235857739 * get_m_Fields_1() const { return ___m_Fields_1; }
	inline List_1_t235857739 ** get_address_of_m_Fields_1() { return &___m_Fields_1; }
	inline void set_m_Fields_1(List_1_t235857739 * value)
	{
		___m_Fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTY_T3943537984_H
#ifndef FIELDWITHTARGET_T3058750293_H
#define FIELDWITHTARGET_T3058750293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty/FieldWithTarget
struct  FieldWithTarget_t3058750293  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_ParamName
	String_t* ___m_ParamName_0;
	// UnityEngine.Object UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_Target
	Object_t631007953 * ___m_Target_1;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_FieldPath
	String_t* ___m_FieldPath_2;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_TypeString
	String_t* ___m_TypeString_3;
	// System.Boolean UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_DoStatic
	bool ___m_DoStatic_4;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_StaticString
	String_t* ___m_StaticString_5;

public:
	inline static int32_t get_offset_of_m_ParamName_0() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_ParamName_0)); }
	inline String_t* get_m_ParamName_0() const { return ___m_ParamName_0; }
	inline String_t** get_address_of_m_ParamName_0() { return &___m_ParamName_0; }
	inline void set_m_ParamName_0(String_t* value)
	{
		___m_ParamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParamName_0), value);
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_Target_1)); }
	inline Object_t631007953 * get_m_Target_1() const { return ___m_Target_1; }
	inline Object_t631007953 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(Object_t631007953 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_1), value);
	}

	inline static int32_t get_offset_of_m_FieldPath_2() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_FieldPath_2)); }
	inline String_t* get_m_FieldPath_2() const { return ___m_FieldPath_2; }
	inline String_t** get_address_of_m_FieldPath_2() { return &___m_FieldPath_2; }
	inline void set_m_FieldPath_2(String_t* value)
	{
		___m_FieldPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FieldPath_2), value);
	}

	inline static int32_t get_offset_of_m_TypeString_3() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_TypeString_3)); }
	inline String_t* get_m_TypeString_3() const { return ___m_TypeString_3; }
	inline String_t** get_address_of_m_TypeString_3() { return &___m_TypeString_3; }
	inline void set_m_TypeString_3(String_t* value)
	{
		___m_TypeString_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeString_3), value);
	}

	inline static int32_t get_offset_of_m_DoStatic_4() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_DoStatic_4)); }
	inline bool get_m_DoStatic_4() const { return ___m_DoStatic_4; }
	inline bool* get_address_of_m_DoStatic_4() { return &___m_DoStatic_4; }
	inline void set_m_DoStatic_4(bool value)
	{
		___m_DoStatic_4 = value;
	}

	inline static int32_t get_offset_of_m_StaticString_5() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_StaticString_5)); }
	inline String_t* get_m_StaticString_5() const { return ___m_StaticString_5; }
	inline String_t** get_address_of_m_StaticString_5() { return &___m_StaticString_5; }
	inline void set_m_StaticString_5(String_t* value)
	{
		___m_StaticString_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StaticString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDWITHTARGET_T3058750293_H
#ifndef TRACKABLEPROPERTYBASE_T2121532948_H
#define TRACKABLEPROPERTYBASE_T2121532948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackablePropertyBase
struct  TrackablePropertyBase_t2121532948  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Analytics.TrackablePropertyBase::m_Target
	Object_t631007953 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackablePropertyBase::m_Path
	String_t* ___m_Path_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Target_0)); }
	inline Object_t631007953 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t631007953 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t631007953 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Path_1)); }
	inline String_t* get_m_Path_1() const { return ___m_Path_1; }
	inline String_t** get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(String_t* value)
	{
		___m_Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTYBASE_T2121532948_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VERTEXANIM_T2231884842_H
#define VERTEXANIM_T2231884842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexJitter/VertexAnim
struct  VertexAnim_t2231884842 
{
public:
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::angleRange
	float ___angleRange_0;
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::angle
	float ___angle_1;
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::speed
	float ___speed_2;

public:
	inline static int32_t get_offset_of_angleRange_0() { return static_cast<int32_t>(offsetof(VertexAnim_t2231884842, ___angleRange_0)); }
	inline float get_angleRange_0() const { return ___angleRange_0; }
	inline float* get_address_of_angleRange_0() { return &___angleRange_0; }
	inline void set_angleRange_0(float value)
	{
		___angleRange_0 = value;
	}

	inline static int32_t get_offset_of_angle_1() { return static_cast<int32_t>(offsetof(VertexAnim_t2231884842, ___angle_1)); }
	inline float get_angle_1() const { return ___angle_1; }
	inline float* get_address_of_angle_1() { return &___angle_1; }
	inline void set_angle_1(float value)
	{
		___angle_1 = value;
	}

	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(VertexAnim_t2231884842, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXANIM_T2231884842_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef UNITYEVENT_2_T1169440328_H
#define UNITYEVENT_2_T1169440328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Char,System.Int32>
struct  UnityEvent_2_t1169440328  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1169440328, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1169440328_H
#ifndef UNITYEVENT_3_T1597070127_H
#define UNITYEVENT_3_T1597070127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.Int32,System.Int32>
struct  UnityEvent_3_t1597070127  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t1597070127, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T1597070127_H
#ifndef UNITYEVENT_3_T2493613095_H
#define UNITYEVENT_3_T2493613095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.String,System.Int32>
struct  UnityEvent_3_t2493613095  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t2493613095, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T2493613095_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1520811813_H
#define U3CSTARTU3EC__ITERATOR0_T1520811813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1520811813  : public RuntimeObject
{
public:
	// UnityEngine.Matrix4x4 EnvMapAnimator/<Start>c__Iterator0::<matrix>__0
	Matrix4x4_t1817901843  ___U3CmatrixU3E__0_0;
	// EnvMapAnimator EnvMapAnimator/<Start>c__Iterator0::$this
	EnvMapAnimator_t1140999784 * ___U24this_1;
	// System.Object EnvMapAnimator/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean EnvMapAnimator/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 EnvMapAnimator/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CmatrixU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U3CmatrixU3E__0_0)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__0_0() const { return ___U3CmatrixU3E__0_0; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__0_0() { return &___U3CmatrixU3E__0_0; }
	inline void set_U3CmatrixU3E__0_0(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24this_1)); }
	inline EnvMapAnimator_t1140999784 * get_U24this_1() const { return ___U24this_1; }
	inline EnvMapAnimator_t1140999784 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(EnvMapAnimator_t1140999784 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1520811813_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef CAMERAMODES_T3200559075_H
#define CAMERAMODES_T3200559075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController/CameraModes
struct  CameraModes_t3200559075 
{
public:
	// System.Int32 TMPro.Examples.CameraController/CameraModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraModes_t3200559075, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMODES_T3200559075_H
#ifndef MOTIONTYPE_T1905163921_H
#define MOTIONTYPE_T1905163921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin/MotionType
struct  MotionType_t1905163921 
{
public:
	// System.Int32 TMPro.Examples.ObjectSpin/MotionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MotionType_t1905163921, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONTYPE_T1905163921_H
#ifndef U3CWARPTEXTU3EC__ITERATOR0_T116130919_H
#define U3CWARPTEXTU3EC__ITERATOR0_T116130919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0
struct  U3CWarpTextU3Ec__Iterator0_t116130919  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_CurveScale>__0
	float ___U3Cold_CurveScaleU3E__0_0;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_ShearValue>__0
	float ___U3Cold_ShearValueU3E__0_1;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_curve>__0
	AnimationCurve_t3046754366 * ___U3Cold_curveU3E__0_2;
	// TMPro.TMP_TextInfo TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<textInfo>__1
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__1_3;
	// System.Int32 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_4;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<boundsMinX>__1
	float ___U3CboundsMinXU3E__1_5;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<boundsMaxX>__1
	float ___U3CboundsMaxXU3E__1_6;
	// UnityEngine.Vector3[] TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<vertices>__2
	Vector3U5BU5D_t1718750761* ___U3CverticesU3E__2_7;
	// UnityEngine.Matrix4x4 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_8;
	// TMPro.Examples.SkewTextExample TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$this
	SkewTextExample_t3460249701 * ___U24this_9;
	// System.Object TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3Cold_CurveScaleU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3Cold_CurveScaleU3E__0_0)); }
	inline float get_U3Cold_CurveScaleU3E__0_0() const { return ___U3Cold_CurveScaleU3E__0_0; }
	inline float* get_address_of_U3Cold_CurveScaleU3E__0_0() { return &___U3Cold_CurveScaleU3E__0_0; }
	inline void set_U3Cold_CurveScaleU3E__0_0(float value)
	{
		___U3Cold_CurveScaleU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cold_ShearValueU3E__0_1() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3Cold_ShearValueU3E__0_1)); }
	inline float get_U3Cold_ShearValueU3E__0_1() const { return ___U3Cold_ShearValueU3E__0_1; }
	inline float* get_address_of_U3Cold_ShearValueU3E__0_1() { return &___U3Cold_ShearValueU3E__0_1; }
	inline void set_U3Cold_ShearValueU3E__0_1(float value)
	{
		___U3Cold_ShearValueU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Cold_curveU3E__0_2() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3Cold_curveU3E__0_2)); }
	inline AnimationCurve_t3046754366 * get_U3Cold_curveU3E__0_2() const { return ___U3Cold_curveU3E__0_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_U3Cold_curveU3E__0_2() { return &___U3Cold_curveU3E__0_2; }
	inline void set_U3Cold_curveU3E__0_2(AnimationCurve_t3046754366 * value)
	{
		___U3Cold_curveU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cold_curveU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__1_3() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CtextInfoU3E__1_3)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__1_3() const { return ___U3CtextInfoU3E__1_3; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__1_3() { return &___U3CtextInfoU3E__1_3; }
	inline void set_U3CtextInfoU3E__1_3(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_4() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CcharacterCountU3E__1_4)); }
	inline int32_t get_U3CcharacterCountU3E__1_4() const { return ___U3CcharacterCountU3E__1_4; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_4() { return &___U3CcharacterCountU3E__1_4; }
	inline void set_U3CcharacterCountU3E__1_4(int32_t value)
	{
		___U3CcharacterCountU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMinXU3E__1_5() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CboundsMinXU3E__1_5)); }
	inline float get_U3CboundsMinXU3E__1_5() const { return ___U3CboundsMinXU3E__1_5; }
	inline float* get_address_of_U3CboundsMinXU3E__1_5() { return &___U3CboundsMinXU3E__1_5; }
	inline void set_U3CboundsMinXU3E__1_5(float value)
	{
		___U3CboundsMinXU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMaxXU3E__1_6() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CboundsMaxXU3E__1_6)); }
	inline float get_U3CboundsMaxXU3E__1_6() const { return ___U3CboundsMaxXU3E__1_6; }
	inline float* get_address_of_U3CboundsMaxXU3E__1_6() { return &___U3CboundsMaxXU3E__1_6; }
	inline void set_U3CboundsMaxXU3E__1_6(float value)
	{
		___U3CboundsMaxXU3E__1_6 = value;
	}

	inline static int32_t get_offset_of_U3CverticesU3E__2_7() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CverticesU3E__2_7)); }
	inline Vector3U5BU5D_t1718750761* get_U3CverticesU3E__2_7() const { return ___U3CverticesU3E__2_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of_U3CverticesU3E__2_7() { return &___U3CverticesU3E__2_7; }
	inline void set_U3CverticesU3E__2_7(Vector3U5BU5D_t1718750761* value)
	{
		___U3CverticesU3E__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CverticesU3E__2_7), value);
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_8() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CmatrixU3E__2_8)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_8() const { return ___U3CmatrixU3E__2_8; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_8() { return &___U3CmatrixU3E__2_8; }
	inline void set_U3CmatrixU3E__2_8(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24this_9)); }
	inline SkewTextExample_t3460249701 * get_U24this_9() const { return ___U24this_9; }
	inline SkewTextExample_t3460249701 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(SkewTextExample_t3460249701 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWARPTEXTU3EC__ITERATOR0_T116130919_H
#ifndef OBJECTTYPE_T4082700821_H
#define OBJECTTYPE_T4082700821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_ExampleScript_01/objectType
struct  objectType_t4082700821 
{
public:
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01/objectType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(objectType_t4082700821, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTYPE_T4082700821_H
#ifndef FPSCOUNTERANCHORPOSITIONS_T1585798158_H
#define FPSCOUNTERANCHORPOSITIONS_T1585798158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_t1585798158 
{
public:
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t1585798158, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_T1585798158_H
#ifndef FPSCOUNTERANCHORPOSITIONS_T2550331785_H
#define FPSCOUNTERANCHORPOSITIONS_T2550331785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_t2550331785 
{
public:
	// System.Int32 TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t2550331785, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_T2550331785_H
#ifndef FPSCOUNTERANCHORPOSITIONS_T2334657565_H
#define FPSCOUNTERANCHORPOSITIONS_T2334657565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_t2334657565 
{
public:
	// System.Int32 TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t2334657565, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_T2334657565_H
#ifndef U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T865582314_H
#define U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T865582314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1
struct  U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<CountDuration>__0
	float ___U3CCountDurationU3E__0_0;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<starting_Count>__0
	float ___U3Cstarting_CountU3E__0_1;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<current_Count>__0
	float ___U3Ccurrent_CountU3E__0_2;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<start_pos>__0
	Vector3_t3722313464  ___U3Cstart_posU3E__0_3;
	// UnityEngine.Color32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<start_color>__0
	Color32_t2600501292  ___U3Cstart_colorU3E__0_4;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<alpha>__0
	float ___U3CalphaU3E__0_5;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<int_counter>__0
	int32_t ___U3Cint_counterU3E__0_6;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<fadeDuration>__0
	float ___U3CfadeDurationU3E__0_7;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$this
	TextMeshProFloatingText_t845872552 * ___U24this_8;
	// System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$disposing
	bool ___U24disposing_10;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CCountDurationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3CCountDurationU3E__0_0)); }
	inline float get_U3CCountDurationU3E__0_0() const { return ___U3CCountDurationU3E__0_0; }
	inline float* get_address_of_U3CCountDurationU3E__0_0() { return &___U3CCountDurationU3E__0_0; }
	inline void set_U3CCountDurationU3E__0_0(float value)
	{
		___U3CCountDurationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cstarting_CountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cstarting_CountU3E__0_1)); }
	inline float get_U3Cstarting_CountU3E__0_1() const { return ___U3Cstarting_CountU3E__0_1; }
	inline float* get_address_of_U3Cstarting_CountU3E__0_1() { return &___U3Cstarting_CountU3E__0_1; }
	inline void set_U3Cstarting_CountU3E__0_1(float value)
	{
		___U3Cstarting_CountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Ccurrent_CountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Ccurrent_CountU3E__0_2)); }
	inline float get_U3Ccurrent_CountU3E__0_2() const { return ___U3Ccurrent_CountU3E__0_2; }
	inline float* get_address_of_U3Ccurrent_CountU3E__0_2() { return &___U3Ccurrent_CountU3E__0_2; }
	inline void set_U3Ccurrent_CountU3E__0_2(float value)
	{
		___U3Ccurrent_CountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_posU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cstart_posU3E__0_3)); }
	inline Vector3_t3722313464  get_U3Cstart_posU3E__0_3() const { return ___U3Cstart_posU3E__0_3; }
	inline Vector3_t3722313464 * get_address_of_U3Cstart_posU3E__0_3() { return &___U3Cstart_posU3E__0_3; }
	inline void set_U3Cstart_posU3E__0_3(Vector3_t3722313464  value)
	{
		___U3Cstart_posU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_colorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cstart_colorU3E__0_4)); }
	inline Color32_t2600501292  get_U3Cstart_colorU3E__0_4() const { return ___U3Cstart_colorU3E__0_4; }
	inline Color32_t2600501292 * get_address_of_U3Cstart_colorU3E__0_4() { return &___U3Cstart_colorU3E__0_4; }
	inline void set_U3Cstart_colorU3E__0_4(Color32_t2600501292  value)
	{
		___U3Cstart_colorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_5() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3CalphaU3E__0_5)); }
	inline float get_U3CalphaU3E__0_5() const { return ___U3CalphaU3E__0_5; }
	inline float* get_address_of_U3CalphaU3E__0_5() { return &___U3CalphaU3E__0_5; }
	inline void set_U3CalphaU3E__0_5(float value)
	{
		___U3CalphaU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3Cint_counterU3E__0_6() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cint_counterU3E__0_6)); }
	inline int32_t get_U3Cint_counterU3E__0_6() const { return ___U3Cint_counterU3E__0_6; }
	inline int32_t* get_address_of_U3Cint_counterU3E__0_6() { return &___U3Cint_counterU3E__0_6; }
	inline void set_U3Cint_counterU3E__0_6(int32_t value)
	{
		___U3Cint_counterU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CfadeDurationU3E__0_7() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3CfadeDurationU3E__0_7)); }
	inline float get_U3CfadeDurationU3E__0_7() const { return ___U3CfadeDurationU3E__0_7; }
	inline float* get_address_of_U3CfadeDurationU3E__0_7() { return &___U3CfadeDurationU3E__0_7; }
	inline void set_U3CfadeDurationU3E__0_7(float value)
	{
		___U3CfadeDurationU3E__0_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24this_8)); }
	inline TextMeshProFloatingText_t845872552 * get_U24this_8() const { return ___U24this_8; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(TextMeshProFloatingText_t845872552 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T865582314_H
#ifndef U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T2967292235_H
#define U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T2967292235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0
struct  U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<CountDuration>__0
	float ___U3CCountDurationU3E__0_0;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<starting_Count>__0
	float ___U3Cstarting_CountU3E__0_1;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<current_Count>__0
	float ___U3Ccurrent_CountU3E__0_2;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<start_pos>__0
	Vector3_t3722313464  ___U3Cstart_posU3E__0_3;
	// UnityEngine.Color32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<start_color>__0
	Color32_t2600501292  ___U3Cstart_colorU3E__0_4;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<alpha>__0
	float ___U3CalphaU3E__0_5;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<int_counter>__0
	int32_t ___U3Cint_counterU3E__0_6;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<fadeDuration>__0
	float ___U3CfadeDurationU3E__0_7;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$this
	TextMeshProFloatingText_t845872552 * ___U24this_8;
	// System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CCountDurationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3CCountDurationU3E__0_0)); }
	inline float get_U3CCountDurationU3E__0_0() const { return ___U3CCountDurationU3E__0_0; }
	inline float* get_address_of_U3CCountDurationU3E__0_0() { return &___U3CCountDurationU3E__0_0; }
	inline void set_U3CCountDurationU3E__0_0(float value)
	{
		___U3CCountDurationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cstarting_CountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cstarting_CountU3E__0_1)); }
	inline float get_U3Cstarting_CountU3E__0_1() const { return ___U3Cstarting_CountU3E__0_1; }
	inline float* get_address_of_U3Cstarting_CountU3E__0_1() { return &___U3Cstarting_CountU3E__0_1; }
	inline void set_U3Cstarting_CountU3E__0_1(float value)
	{
		___U3Cstarting_CountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Ccurrent_CountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Ccurrent_CountU3E__0_2)); }
	inline float get_U3Ccurrent_CountU3E__0_2() const { return ___U3Ccurrent_CountU3E__0_2; }
	inline float* get_address_of_U3Ccurrent_CountU3E__0_2() { return &___U3Ccurrent_CountU3E__0_2; }
	inline void set_U3Ccurrent_CountU3E__0_2(float value)
	{
		___U3Ccurrent_CountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_posU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cstart_posU3E__0_3)); }
	inline Vector3_t3722313464  get_U3Cstart_posU3E__0_3() const { return ___U3Cstart_posU3E__0_3; }
	inline Vector3_t3722313464 * get_address_of_U3Cstart_posU3E__0_3() { return &___U3Cstart_posU3E__0_3; }
	inline void set_U3Cstart_posU3E__0_3(Vector3_t3722313464  value)
	{
		___U3Cstart_posU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_colorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cstart_colorU3E__0_4)); }
	inline Color32_t2600501292  get_U3Cstart_colorU3E__0_4() const { return ___U3Cstart_colorU3E__0_4; }
	inline Color32_t2600501292 * get_address_of_U3Cstart_colorU3E__0_4() { return &___U3Cstart_colorU3E__0_4; }
	inline void set_U3Cstart_colorU3E__0_4(Color32_t2600501292  value)
	{
		___U3Cstart_colorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_5() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3CalphaU3E__0_5)); }
	inline float get_U3CalphaU3E__0_5() const { return ___U3CalphaU3E__0_5; }
	inline float* get_address_of_U3CalphaU3E__0_5() { return &___U3CalphaU3E__0_5; }
	inline void set_U3CalphaU3E__0_5(float value)
	{
		___U3CalphaU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3Cint_counterU3E__0_6() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cint_counterU3E__0_6)); }
	inline int32_t get_U3Cint_counterU3E__0_6() const { return ___U3Cint_counterU3E__0_6; }
	inline int32_t* get_address_of_U3Cint_counterU3E__0_6() { return &___U3Cint_counterU3E__0_6; }
	inline void set_U3Cint_counterU3E__0_6(int32_t value)
	{
		___U3Cint_counterU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CfadeDurationU3E__0_7() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3CfadeDurationU3E__0_7)); }
	inline float get_U3CfadeDurationU3E__0_7() const { return ___U3CfadeDurationU3E__0_7; }
	inline float* get_address_of_U3CfadeDurationU3E__0_7() { return &___U3CfadeDurationU3E__0_7; }
	inline void set_U3CfadeDurationU3E__0_7(float value)
	{
		___U3CfadeDurationU3E__0_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24this_8)); }
	inline TextMeshProFloatingText_t845872552 * get_U24this_8() const { return ___U24this_8; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(TextMeshProFloatingText_t845872552 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T2967292235_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T897284962_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T897284962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t897284962  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__0_0;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<currentCharacter>__0
	int32_t ___U3CcurrentCharacterU3E__0_1;
	// UnityEngine.Color32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<c0>__0
	Color32_t2600501292  ___U3Cc0U3E__0_2;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_3;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<materialIndex>__1
	int32_t ___U3CmaterialIndexU3E__1_4;
	// UnityEngine.Color32[] TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<newVertexColors>__1
	Color32U5BU5D_t3850468773* ___U3CnewVertexColorsU3E__1_5;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<vertexIndex>__1
	int32_t ___U3CvertexIndexU3E__1_6;
	// TMPro.Examples.VertexColorCycler TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$this
	VertexColorCycler_t3003193665 * ___U24this_7;
	// System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcurrentCharacterU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U3CcurrentCharacterU3E__0_1)); }
	inline int32_t get_U3CcurrentCharacterU3E__0_1() const { return ___U3CcurrentCharacterU3E__0_1; }
	inline int32_t* get_address_of_U3CcurrentCharacterU3E__0_1() { return &___U3CcurrentCharacterU3E__0_1; }
	inline void set_U3CcurrentCharacterU3E__0_1(int32_t value)
	{
		___U3CcurrentCharacterU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Cc0U3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U3Cc0U3E__0_2)); }
	inline Color32_t2600501292  get_U3Cc0U3E__0_2() const { return ___U3Cc0U3E__0_2; }
	inline Color32_t2600501292 * get_address_of_U3Cc0U3E__0_2() { return &___U3Cc0U3E__0_2; }
	inline void set_U3Cc0U3E__0_2(Color32_t2600501292  value)
	{
		___U3Cc0U3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U3CcharacterCountU3E__1_3)); }
	inline int32_t get_U3CcharacterCountU3E__1_3() const { return ___U3CcharacterCountU3E__1_3; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_3() { return &___U3CcharacterCountU3E__1_3; }
	inline void set_U3CcharacterCountU3E__1_3(int32_t value)
	{
		___U3CcharacterCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmaterialIndexU3E__1_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U3CmaterialIndexU3E__1_4)); }
	inline int32_t get_U3CmaterialIndexU3E__1_4() const { return ___U3CmaterialIndexU3E__1_4; }
	inline int32_t* get_address_of_U3CmaterialIndexU3E__1_4() { return &___U3CmaterialIndexU3E__1_4; }
	inline void set_U3CmaterialIndexU3E__1_4(int32_t value)
	{
		___U3CmaterialIndexU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CnewVertexColorsU3E__1_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U3CnewVertexColorsU3E__1_5)); }
	inline Color32U5BU5D_t3850468773* get_U3CnewVertexColorsU3E__1_5() const { return ___U3CnewVertexColorsU3E__1_5; }
	inline Color32U5BU5D_t3850468773** get_address_of_U3CnewVertexColorsU3E__1_5() { return &___U3CnewVertexColorsU3E__1_5; }
	inline void set_U3CnewVertexColorsU3E__1_5(Color32U5BU5D_t3850468773* value)
	{
		___U3CnewVertexColorsU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnewVertexColorsU3E__1_5), value);
	}

	inline static int32_t get_offset_of_U3CvertexIndexU3E__1_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U3CvertexIndexU3E__1_6)); }
	inline int32_t get_U3CvertexIndexU3E__1_6() const { return ___U3CvertexIndexU3E__1_6; }
	inline int32_t* get_address_of_U3CvertexIndexU3E__1_6() { return &___U3CvertexIndexU3E__1_6; }
	inline void set_U3CvertexIndexU3E__1_6(int32_t value)
	{
		___U3CvertexIndexU3E__1_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U24this_7)); }
	inline VertexColorCycler_t3003193665 * get_U24this_7() const { return ___U24this_7; }
	inline VertexColorCycler_t3003193665 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(VertexColorCycler_t3003193665 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t897284962, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T897284962_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T225534713_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T225534713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t225534713  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__0_0;
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<loopCount>__0
	int32_t ___U3CloopCountU3E__0_1;
	// TMPro.Examples.VertexJitter/VertexAnim[] TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<vertexAnim>__0
	VertexAnimU5BU5D_t1611656175* ___U3CvertexAnimU3E__0_2;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<cachedMeshInfo>__0
	TMP_MeshInfoU5BU5D_t3365986247* ___U3CcachedMeshInfoU3E__0_3;
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_4;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_5;
	// TMPro.Examples.VertexJitter TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$this
	VertexJitter_t4087429332 * ___U24this_6;
	// System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CloopCountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U3CloopCountU3E__0_1)); }
	inline int32_t get_U3CloopCountU3E__0_1() const { return ___U3CloopCountU3E__0_1; }
	inline int32_t* get_address_of_U3CloopCountU3E__0_1() { return &___U3CloopCountU3E__0_1; }
	inline void set_U3CloopCountU3E__0_1(int32_t value)
	{
		___U3CloopCountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CvertexAnimU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U3CvertexAnimU3E__0_2)); }
	inline VertexAnimU5BU5D_t1611656175* get_U3CvertexAnimU3E__0_2() const { return ___U3CvertexAnimU3E__0_2; }
	inline VertexAnimU5BU5D_t1611656175** get_address_of_U3CvertexAnimU3E__0_2() { return &___U3CvertexAnimU3E__0_2; }
	inline void set_U3CvertexAnimU3E__0_2(VertexAnimU5BU5D_t1611656175* value)
	{
		___U3CvertexAnimU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvertexAnimU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CcachedMeshInfoU3E__0_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U3CcachedMeshInfoU3E__0_3)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_U3CcachedMeshInfoU3E__0_3() const { return ___U3CcachedMeshInfoU3E__0_3; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_U3CcachedMeshInfoU3E__0_3() { return &___U3CcachedMeshInfoU3E__0_3; }
	inline void set_U3CcachedMeshInfoU3E__0_3(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___U3CcachedMeshInfoU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcachedMeshInfoU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U3CcharacterCountU3E__1_4)); }
	inline int32_t get_U3CcharacterCountU3E__1_4() const { return ___U3CcharacterCountU3E__1_4; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_4() { return &___U3CcharacterCountU3E__1_4; }
	inline void set_U3CcharacterCountU3E__1_4(int32_t value)
	{
		___U3CcharacterCountU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U3CmatrixU3E__2_5)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_5() const { return ___U3CmatrixU3E__2_5; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_5() { return &___U3CmatrixU3E__2_5; }
	inline void set_U3CmatrixU3E__2_5(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U24this_6)); }
	inline VertexJitter_t4087429332 * get_U24this_6() const { return ___U24this_6; }
	inline VertexJitter_t4087429332 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(VertexJitter_t4087429332 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t225534713, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T225534713_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T956521787_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T956521787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t956521787  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__0_0;
	// UnityEngine.Vector3[][] TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<copyOfVertices>__0
	Vector3U5BU5DU5BU5D_t546443028* ___U3CcopyOfVerticesU3E__0_1;
	// System.Int32 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_2;
	// System.Int32 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<lineCount>__1
	int32_t ___U3ClineCountU3E__1_3;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_4;
	// TMPro.Examples.VertexShakeA TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$this
	VertexShakeA_t4262048139 * ___U24this_5;
	// System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcopyOfVerticesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U3CcopyOfVerticesU3E__0_1)); }
	inline Vector3U5BU5DU5BU5D_t546443028* get_U3CcopyOfVerticesU3E__0_1() const { return ___U3CcopyOfVerticesU3E__0_1; }
	inline Vector3U5BU5DU5BU5D_t546443028** get_address_of_U3CcopyOfVerticesU3E__0_1() { return &___U3CcopyOfVerticesU3E__0_1; }
	inline void set_U3CcopyOfVerticesU3E__0_1(Vector3U5BU5DU5BU5D_t546443028* value)
	{
		___U3CcopyOfVerticesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcopyOfVerticesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U3CcharacterCountU3E__1_2)); }
	inline int32_t get_U3CcharacterCountU3E__1_2() const { return ___U3CcharacterCountU3E__1_2; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_2() { return &___U3CcharacterCountU3E__1_2; }
	inline void set_U3CcharacterCountU3E__1_2(int32_t value)
	{
		___U3CcharacterCountU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3ClineCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U3ClineCountU3E__1_3)); }
	inline int32_t get_U3ClineCountU3E__1_3() const { return ___U3ClineCountU3E__1_3; }
	inline int32_t* get_address_of_U3ClineCountU3E__1_3() { return &___U3ClineCountU3E__1_3; }
	inline void set_U3ClineCountU3E__1_3(int32_t value)
	{
		___U3ClineCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U3CmatrixU3E__2_4)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_4() const { return ___U3CmatrixU3E__2_4; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_4() { return &___U3CmatrixU3E__2_4; }
	inline void set_U3CmatrixU3E__2_4(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U24this_5)); }
	inline VertexShakeA_t4262048139 * get_U24this_5() const { return ___U24this_5; }
	inline VertexShakeA_t4262048139 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(VertexShakeA_t4262048139 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t956521787, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T956521787_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T168300594_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T168300594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t168300594  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__0_0;
	// UnityEngine.Vector3[][] TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<copyOfVertices>__0
	Vector3U5BU5DU5BU5D_t546443028* ___U3CcopyOfVerticesU3E__0_1;
	// System.Int32 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_2;
	// System.Int32 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<lineCount>__1
	int32_t ___U3ClineCountU3E__1_3;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_4;
	// TMPro.Examples.VertexShakeB TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$this
	VertexShakeB_t1533164784 * ___U24this_5;
	// System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcopyOfVerticesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U3CcopyOfVerticesU3E__0_1)); }
	inline Vector3U5BU5DU5BU5D_t546443028* get_U3CcopyOfVerticesU3E__0_1() const { return ___U3CcopyOfVerticesU3E__0_1; }
	inline Vector3U5BU5DU5BU5D_t546443028** get_address_of_U3CcopyOfVerticesU3E__0_1() { return &___U3CcopyOfVerticesU3E__0_1; }
	inline void set_U3CcopyOfVerticesU3E__0_1(Vector3U5BU5DU5BU5D_t546443028* value)
	{
		___U3CcopyOfVerticesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcopyOfVerticesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U3CcharacterCountU3E__1_2)); }
	inline int32_t get_U3CcharacterCountU3E__1_2() const { return ___U3CcharacterCountU3E__1_2; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_2() { return &___U3CcharacterCountU3E__1_2; }
	inline void set_U3CcharacterCountU3E__1_2(int32_t value)
	{
		___U3CcharacterCountU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3ClineCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U3ClineCountU3E__1_3)); }
	inline int32_t get_U3ClineCountU3E__1_3() const { return ___U3ClineCountU3E__1_3; }
	inline int32_t* get_address_of_U3ClineCountU3E__1_3() { return &___U3ClineCountU3E__1_3; }
	inline void set_U3ClineCountU3E__1_3(int32_t value)
	{
		___U3ClineCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U3CmatrixU3E__2_4)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_4() const { return ___U3CmatrixU3E__2_4; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_4() { return &___U3CmatrixU3E__2_4; }
	inline void set_U3CmatrixU3E__2_4(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U24this_5)); }
	inline VertexShakeB_t1533164784 * get_U24this_5() const { return ___U24this_5; }
	inline VertexShakeB_t1533164784 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(VertexShakeB_t1533164784 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t168300594, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T168300594_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T3792186008_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T3792186008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__0_0;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<cachedMeshInfoVertexData>__0
	TMP_MeshInfoU5BU5D_t3365986247* ___U3CcachedMeshInfoVertexDataU3E__0_1;
	// System.Collections.Generic.List`1<System.Int32> TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<scaleSortingOrder>__0
	List_1_t128053199 * ___U3CscaleSortingOrderU3E__0_2;
	// System.Int32 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_3;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_4;
	// TMPro.Examples.VertexZoom TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$this
	VertexZoom_t550798657 * ___U24this_5;
	// System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_8;
	// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$locvar0
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514 * ___U24locvar0_9;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcachedMeshInfoVertexDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U3CcachedMeshInfoVertexDataU3E__0_1)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_U3CcachedMeshInfoVertexDataU3E__0_1() const { return ___U3CcachedMeshInfoVertexDataU3E__0_1; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_U3CcachedMeshInfoVertexDataU3E__0_1() { return &___U3CcachedMeshInfoVertexDataU3E__0_1; }
	inline void set_U3CcachedMeshInfoVertexDataU3E__0_1(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___U3CcachedMeshInfoVertexDataU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcachedMeshInfoVertexDataU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CscaleSortingOrderU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U3CscaleSortingOrderU3E__0_2)); }
	inline List_1_t128053199 * get_U3CscaleSortingOrderU3E__0_2() const { return ___U3CscaleSortingOrderU3E__0_2; }
	inline List_1_t128053199 ** get_address_of_U3CscaleSortingOrderU3E__0_2() { return &___U3CscaleSortingOrderU3E__0_2; }
	inline void set_U3CscaleSortingOrderU3E__0_2(List_1_t128053199 * value)
	{
		___U3CscaleSortingOrderU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscaleSortingOrderU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U3CcharacterCountU3E__1_3)); }
	inline int32_t get_U3CcharacterCountU3E__1_3() const { return ___U3CcharacterCountU3E__1_3; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_3() { return &___U3CcharacterCountU3E__1_3; }
	inline void set_U3CcharacterCountU3E__1_3(int32_t value)
	{
		___U3CcharacterCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U3CmatrixU3E__2_4)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_4() const { return ___U3CmatrixU3E__2_4; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_4() { return &___U3CmatrixU3E__2_4; }
	inline void set_U3CmatrixU3E__2_4(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U24this_5)); }
	inline VertexZoom_t550798657 * get_U24this_5() const { return ___U24this_5; }
	inline VertexZoom_t550798657 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(VertexZoom_t550798657 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_9() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008, ___U24locvar0_9)); }
	inline U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514 * get_U24locvar0_9() const { return ___U24locvar0_9; }
	inline U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514 ** get_address_of_U24locvar0_9() { return &___U24locvar0_9; }
	inline void set_U24locvar0_9(U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514 * value)
	{
		___U24locvar0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T3792186008_H
#ifndef U3CWARPTEXTU3EC__ITERATOR0_T4025661343_H
#define U3CWARPTEXTU3EC__ITERATOR0_T4025661343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0
struct  U3CWarpTextU3Ec__Iterator0_t4025661343  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<old_CurveScale>__0
	float ___U3Cold_CurveScaleU3E__0_0;
	// UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<old_curve>__0
	AnimationCurve_t3046754366 * ___U3Cold_curveU3E__0_1;
	// TMPro.TMP_TextInfo TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<textInfo>__1
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__1_2;
	// System.Int32 TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_3;
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<boundsMinX>__1
	float ___U3CboundsMinXU3E__1_4;
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<boundsMaxX>__1
	float ___U3CboundsMaxXU3E__1_5;
	// UnityEngine.Vector3[] TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<vertices>__2
	Vector3U5BU5D_t1718750761* ___U3CverticesU3E__2_6;
	// UnityEngine.Matrix4x4 TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_7;
	// TMPro.Examples.WarpTextExample TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$this
	WarpTextExample_t3821118074 * ___U24this_8;
	// System.Object TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3Cold_CurveScaleU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3Cold_CurveScaleU3E__0_0)); }
	inline float get_U3Cold_CurveScaleU3E__0_0() const { return ___U3Cold_CurveScaleU3E__0_0; }
	inline float* get_address_of_U3Cold_CurveScaleU3E__0_0() { return &___U3Cold_CurveScaleU3E__0_0; }
	inline void set_U3Cold_CurveScaleU3E__0_0(float value)
	{
		___U3Cold_CurveScaleU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cold_curveU3E__0_1() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3Cold_curveU3E__0_1)); }
	inline AnimationCurve_t3046754366 * get_U3Cold_curveU3E__0_1() const { return ___U3Cold_curveU3E__0_1; }
	inline AnimationCurve_t3046754366 ** get_address_of_U3Cold_curveU3E__0_1() { return &___U3Cold_curveU3E__0_1; }
	inline void set_U3Cold_curveU3E__0_1(AnimationCurve_t3046754366 * value)
	{
		___U3Cold_curveU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cold_curveU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__1_2() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CtextInfoU3E__1_2)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__1_2() const { return ___U3CtextInfoU3E__1_2; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__1_2() { return &___U3CtextInfoU3E__1_2; }
	inline void set_U3CtextInfoU3E__1_2(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CcharacterCountU3E__1_3)); }
	inline int32_t get_U3CcharacterCountU3E__1_3() const { return ___U3CcharacterCountU3E__1_3; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_3() { return &___U3CcharacterCountU3E__1_3; }
	inline void set_U3CcharacterCountU3E__1_3(int32_t value)
	{
		___U3CcharacterCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMinXU3E__1_4() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CboundsMinXU3E__1_4)); }
	inline float get_U3CboundsMinXU3E__1_4() const { return ___U3CboundsMinXU3E__1_4; }
	inline float* get_address_of_U3CboundsMinXU3E__1_4() { return &___U3CboundsMinXU3E__1_4; }
	inline void set_U3CboundsMinXU3E__1_4(float value)
	{
		___U3CboundsMinXU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMaxXU3E__1_5() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CboundsMaxXU3E__1_5)); }
	inline float get_U3CboundsMaxXU3E__1_5() const { return ___U3CboundsMaxXU3E__1_5; }
	inline float* get_address_of_U3CboundsMaxXU3E__1_5() { return &___U3CboundsMaxXU3E__1_5; }
	inline void set_U3CboundsMaxXU3E__1_5(float value)
	{
		___U3CboundsMaxXU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CverticesU3E__2_6() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CverticesU3E__2_6)); }
	inline Vector3U5BU5D_t1718750761* get_U3CverticesU3E__2_6() const { return ___U3CverticesU3E__2_6; }
	inline Vector3U5BU5D_t1718750761** get_address_of_U3CverticesU3E__2_6() { return &___U3CverticesU3E__2_6; }
	inline void set_U3CverticesU3E__2_6(Vector3U5BU5D_t1718750761* value)
	{
		___U3CverticesU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CverticesU3E__2_6), value);
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_7() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U3CmatrixU3E__2_7)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_7() const { return ___U3CmatrixU3E__2_7; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_7() { return &___U3CmatrixU3E__2_7; }
	inline void set_U3CmatrixU3E__2_7(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U24this_8)); }
	inline WarpTextExample_t3821118074 * get_U24this_8() const { return ___U24this_8; }
	inline WarpTextExample_t3821118074 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(WarpTextExample_t3821118074 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t4025661343, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWARPTEXTU3EC__ITERATOR0_T4025661343_H
#ifndef CHARACTERSELECTIONEVENT_T3109943174_H
#define CHARACTERSELECTIONEVENT_T3109943174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/CharacterSelectionEvent
struct  CharacterSelectionEvent_t3109943174  : public UnityEvent_2_t1169440328
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERSELECTIONEVENT_T3109943174_H
#ifndef LINESELECTIONEVENT_T2868010532_H
#define LINESELECTIONEVENT_T2868010532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/LineSelectionEvent
struct  LineSelectionEvent_t2868010532  : public UnityEvent_3_t1597070127
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESELECTIONEVENT_T2868010532_H
#ifndef LINKSELECTIONEVENT_T1590929858_H
#define LINKSELECTIONEVENT_T1590929858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/LinkSelectionEvent
struct  LinkSelectionEvent_t1590929858  : public UnityEvent_3_t2493613095
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKSELECTIONEVENT_T1590929858_H
#ifndef SPRITESELECTIONEVENT_T2798445241_H
#define SPRITESELECTIONEVENT_T2798445241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/SpriteSelectionEvent
struct  SpriteSelectionEvent_t2798445241  : public UnityEvent_2_t1169440328
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESELECTIONEVENT_T2798445241_H
#ifndef WORDSELECTIONEVENT_T1841909953_H
#define WORDSELECTIONEVENT_T1841909953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct  WordSelectionEvent_t1841909953  : public UnityEvent_3_t1597070127
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDSELECTIONEVENT_T1841909953_H
#ifndef TRIGGER_T4199345191_H
#define TRIGGER_T4199345191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker/Trigger
struct  Trigger_t4199345191 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t4199345191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T4199345191_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef PROPERTYTYPE_T4040930247_H
#define PROPERTYTYPE_T4040930247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty/PropertyType
struct  PropertyType_t4040930247 
{
public:
	// System.Int32 UnityEngine.Analytics.ValueProperty/PropertyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyType_t4040930247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPE_T4040930247_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef VALUEPROPERTY_T1868393739_H
#define VALUEPROPERTY_T1868393739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty
struct  ValueProperty_t1868393739  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EditingCustomValue
	bool ___m_EditingCustomValue_0;
	// System.Int32 UnityEngine.Analytics.ValueProperty::m_PopupIndex
	int32_t ___m_PopupIndex_1;
	// System.String UnityEngine.Analytics.ValueProperty::m_CustomValue
	String_t* ___m_CustomValue_2;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_FixedType
	bool ___m_FixedType_3;
	// System.String UnityEngine.Analytics.ValueProperty::m_EnumType
	String_t* ___m_EnumType_4;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EnumTypeIsCustomizable
	bool ___m_EnumTypeIsCustomizable_5;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_CanDisable
	bool ___m_CanDisable_6;
	// UnityEngine.Analytics.ValueProperty/PropertyType UnityEngine.Analytics.ValueProperty::m_PropertyType
	int32_t ___m_PropertyType_7;
	// System.String UnityEngine.Analytics.ValueProperty::m_ValueType
	String_t* ___m_ValueType_8;
	// System.String UnityEngine.Analytics.ValueProperty::m_Value
	String_t* ___m_Value_9;
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.ValueProperty::m_Target
	TrackableField_t1772682203 * ___m_Target_10;

public:
	inline static int32_t get_offset_of_m_EditingCustomValue_0() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EditingCustomValue_0)); }
	inline bool get_m_EditingCustomValue_0() const { return ___m_EditingCustomValue_0; }
	inline bool* get_address_of_m_EditingCustomValue_0() { return &___m_EditingCustomValue_0; }
	inline void set_m_EditingCustomValue_0(bool value)
	{
		___m_EditingCustomValue_0 = value;
	}

	inline static int32_t get_offset_of_m_PopupIndex_1() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PopupIndex_1)); }
	inline int32_t get_m_PopupIndex_1() const { return ___m_PopupIndex_1; }
	inline int32_t* get_address_of_m_PopupIndex_1() { return &___m_PopupIndex_1; }
	inline void set_m_PopupIndex_1(int32_t value)
	{
		___m_PopupIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_CustomValue_2() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CustomValue_2)); }
	inline String_t* get_m_CustomValue_2() const { return ___m_CustomValue_2; }
	inline String_t** get_address_of_m_CustomValue_2() { return &___m_CustomValue_2; }
	inline void set_m_CustomValue_2(String_t* value)
	{
		___m_CustomValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomValue_2), value);
	}

	inline static int32_t get_offset_of_m_FixedType_3() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_FixedType_3)); }
	inline bool get_m_FixedType_3() const { return ___m_FixedType_3; }
	inline bool* get_address_of_m_FixedType_3() { return &___m_FixedType_3; }
	inline void set_m_FixedType_3(bool value)
	{
		___m_FixedType_3 = value;
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}

	inline static int32_t get_offset_of_m_EnumTypeIsCustomizable_5() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumTypeIsCustomizable_5)); }
	inline bool get_m_EnumTypeIsCustomizable_5() const { return ___m_EnumTypeIsCustomizable_5; }
	inline bool* get_address_of_m_EnumTypeIsCustomizable_5() { return &___m_EnumTypeIsCustomizable_5; }
	inline void set_m_EnumTypeIsCustomizable_5(bool value)
	{
		___m_EnumTypeIsCustomizable_5 = value;
	}

	inline static int32_t get_offset_of_m_CanDisable_6() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CanDisable_6)); }
	inline bool get_m_CanDisable_6() const { return ___m_CanDisable_6; }
	inline bool* get_address_of_m_CanDisable_6() { return &___m_CanDisable_6; }
	inline void set_m_CanDisable_6(bool value)
	{
		___m_CanDisable_6 = value;
	}

	inline static int32_t get_offset_of_m_PropertyType_7() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PropertyType_7)); }
	inline int32_t get_m_PropertyType_7() const { return ___m_PropertyType_7; }
	inline int32_t* get_address_of_m_PropertyType_7() { return &___m_PropertyType_7; }
	inline void set_m_PropertyType_7(int32_t value)
	{
		___m_PropertyType_7 = value;
	}

	inline static int32_t get_offset_of_m_ValueType_8() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_ValueType_8)); }
	inline String_t* get_m_ValueType_8() const { return ___m_ValueType_8; }
	inline String_t** get_address_of_m_ValueType_8() { return &___m_ValueType_8; }
	inline void set_m_ValueType_8(String_t* value)
	{
		___m_ValueType_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValueType_8), value);
	}

	inline static int32_t get_offset_of_m_Value_9() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Value_9)); }
	inline String_t* get_m_Value_9() const { return ___m_Value_9; }
	inline String_t** get_address_of_m_Value_9() { return &___m_Value_9; }
	inline void set_m_Value_9(String_t* value)
	{
		___m_Value_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_9), value);
	}

	inline static int32_t get_offset_of_m_Target_10() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Target_10)); }
	inline TrackableField_t1772682203 * get_m_Target_10() const { return ___m_Target_10; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_10() { return &___m_Target_10; }
	inline void set_m_Target_10(TrackableField_t1772682203 * value)
	{
		___m_Target_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEPROPERTY_T1868393739_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef TMP_INPUTVALIDATOR_T1385053824_H
#define TMP_INPUTVALIDATOR_T1385053824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputValidator
struct  TMP_InputValidator_t1385053824  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_INPUTVALIDATOR_T1385053824_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef TMP_DIGITVALIDATOR_T573672104_H
#define TMP_DIGITVALIDATOR_T573672104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_DigitValidator
struct  TMP_DigitValidator_t573672104  : public TMP_InputValidator_t1385053824
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_DIGITVALIDATOR_T573672104_H
#ifndef TMP_PHONENUMBERVALIDATOR_T743649728_H
#define TMP_PHONENUMBERVALIDATOR_T743649728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PhoneNumberValidator
struct  TMP_PhoneNumberValidator_t743649728  : public TMP_InputValidator_t1385053824
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PHONENUMBERVALIDATOR_T743649728_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef BALL_T2206666566_H
#define BALL_T2206666566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ball
struct  Ball_t2206666566  : public MonoBehaviour_t3962482529
{
public:
	// Paddle Ball::paddle1
	Paddle_t4140593882 * ___paddle1_4;
	// System.Single Ball::xPush
	float ___xPush_5;
	// System.Single Ball::yPush
	float ___yPush_6;
	// UnityEngine.AudioClip[] Ball::ballSounds
	AudioClipU5BU5D_t143221404* ___ballSounds_7;
	// System.Single Ball::randomFactor
	float ___randomFactor_8;
	// UnityEngine.Vector2 Ball::paddleToBallVector
	Vector2_t2156229523  ___paddleToBallVector_9;
	// UnityEngine.Vector3 Ball::unityMousePos
	Vector3_t3722313464  ___unityMousePos_10;
	// System.Boolean Ball::hasStarted
	bool ___hasStarted_11;
	// UnityEngine.AudioSource Ball::audioSource
	AudioSource_t3935305588 * ___audioSource_12;
	// UnityEngine.Rigidbody2D Ball::myRigidbody2D
	Rigidbody2D_t939494601 * ___myRigidbody2D_13;

public:
	inline static int32_t get_offset_of_paddle1_4() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___paddle1_4)); }
	inline Paddle_t4140593882 * get_paddle1_4() const { return ___paddle1_4; }
	inline Paddle_t4140593882 ** get_address_of_paddle1_4() { return &___paddle1_4; }
	inline void set_paddle1_4(Paddle_t4140593882 * value)
	{
		___paddle1_4 = value;
		Il2CppCodeGenWriteBarrier((&___paddle1_4), value);
	}

	inline static int32_t get_offset_of_xPush_5() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___xPush_5)); }
	inline float get_xPush_5() const { return ___xPush_5; }
	inline float* get_address_of_xPush_5() { return &___xPush_5; }
	inline void set_xPush_5(float value)
	{
		___xPush_5 = value;
	}

	inline static int32_t get_offset_of_yPush_6() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___yPush_6)); }
	inline float get_yPush_6() const { return ___yPush_6; }
	inline float* get_address_of_yPush_6() { return &___yPush_6; }
	inline void set_yPush_6(float value)
	{
		___yPush_6 = value;
	}

	inline static int32_t get_offset_of_ballSounds_7() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___ballSounds_7)); }
	inline AudioClipU5BU5D_t143221404* get_ballSounds_7() const { return ___ballSounds_7; }
	inline AudioClipU5BU5D_t143221404** get_address_of_ballSounds_7() { return &___ballSounds_7; }
	inline void set_ballSounds_7(AudioClipU5BU5D_t143221404* value)
	{
		___ballSounds_7 = value;
		Il2CppCodeGenWriteBarrier((&___ballSounds_7), value);
	}

	inline static int32_t get_offset_of_randomFactor_8() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___randomFactor_8)); }
	inline float get_randomFactor_8() const { return ___randomFactor_8; }
	inline float* get_address_of_randomFactor_8() { return &___randomFactor_8; }
	inline void set_randomFactor_8(float value)
	{
		___randomFactor_8 = value;
	}

	inline static int32_t get_offset_of_paddleToBallVector_9() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___paddleToBallVector_9)); }
	inline Vector2_t2156229523  get_paddleToBallVector_9() const { return ___paddleToBallVector_9; }
	inline Vector2_t2156229523 * get_address_of_paddleToBallVector_9() { return &___paddleToBallVector_9; }
	inline void set_paddleToBallVector_9(Vector2_t2156229523  value)
	{
		___paddleToBallVector_9 = value;
	}

	inline static int32_t get_offset_of_unityMousePos_10() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___unityMousePos_10)); }
	inline Vector3_t3722313464  get_unityMousePos_10() const { return ___unityMousePos_10; }
	inline Vector3_t3722313464 * get_address_of_unityMousePos_10() { return &___unityMousePos_10; }
	inline void set_unityMousePos_10(Vector3_t3722313464  value)
	{
		___unityMousePos_10 = value;
	}

	inline static int32_t get_offset_of_hasStarted_11() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___hasStarted_11)); }
	inline bool get_hasStarted_11() const { return ___hasStarted_11; }
	inline bool* get_address_of_hasStarted_11() { return &___hasStarted_11; }
	inline void set_hasStarted_11(bool value)
	{
		___hasStarted_11 = value;
	}

	inline static int32_t get_offset_of_audioSource_12() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___audioSource_12)); }
	inline AudioSource_t3935305588 * get_audioSource_12() const { return ___audioSource_12; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_12() { return &___audioSource_12; }
	inline void set_audioSource_12(AudioSource_t3935305588 * value)
	{
		___audioSource_12 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_12), value);
	}

	inline static int32_t get_offset_of_myRigidbody2D_13() { return static_cast<int32_t>(offsetof(Ball_t2206666566, ___myRigidbody2D_13)); }
	inline Rigidbody2D_t939494601 * get_myRigidbody2D_13() const { return ___myRigidbody2D_13; }
	inline Rigidbody2D_t939494601 ** get_address_of_myRigidbody2D_13() { return &___myRigidbody2D_13; }
	inline void set_myRigidbody2D_13(Rigidbody2D_t939494601 * value)
	{
		___myRigidbody2D_13 = value;
		Il2CppCodeGenWriteBarrier((&___myRigidbody2D_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALL_T2206666566_H
#ifndef BLOCK_T1429612866_H
#define BLOCK_T1429612866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Block
struct  Block_t1429612866  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioClip Block::breakSound
	AudioClip_t3680889665 * ___breakSound_4;
	// UnityEngine.GameObject Block::blockSparklesVFX
	GameObject_t1113636619 * ___blockSparklesVFX_5;
	// UnityEngine.Sprite[] Block::hitSprites
	SpriteU5BU5D_t2581906349* ___hitSprites_6;
	// Level Block::level
	Level_t2237665516 * ___level_7;
	// System.Int32 Block::timesHit
	int32_t ___timesHit_8;

public:
	inline static int32_t get_offset_of_breakSound_4() { return static_cast<int32_t>(offsetof(Block_t1429612866, ___breakSound_4)); }
	inline AudioClip_t3680889665 * get_breakSound_4() const { return ___breakSound_4; }
	inline AudioClip_t3680889665 ** get_address_of_breakSound_4() { return &___breakSound_4; }
	inline void set_breakSound_4(AudioClip_t3680889665 * value)
	{
		___breakSound_4 = value;
		Il2CppCodeGenWriteBarrier((&___breakSound_4), value);
	}

	inline static int32_t get_offset_of_blockSparklesVFX_5() { return static_cast<int32_t>(offsetof(Block_t1429612866, ___blockSparklesVFX_5)); }
	inline GameObject_t1113636619 * get_blockSparklesVFX_5() const { return ___blockSparklesVFX_5; }
	inline GameObject_t1113636619 ** get_address_of_blockSparklesVFX_5() { return &___blockSparklesVFX_5; }
	inline void set_blockSparklesVFX_5(GameObject_t1113636619 * value)
	{
		___blockSparklesVFX_5 = value;
		Il2CppCodeGenWriteBarrier((&___blockSparklesVFX_5), value);
	}

	inline static int32_t get_offset_of_hitSprites_6() { return static_cast<int32_t>(offsetof(Block_t1429612866, ___hitSprites_6)); }
	inline SpriteU5BU5D_t2581906349* get_hitSprites_6() const { return ___hitSprites_6; }
	inline SpriteU5BU5D_t2581906349** get_address_of_hitSprites_6() { return &___hitSprites_6; }
	inline void set_hitSprites_6(SpriteU5BU5D_t2581906349* value)
	{
		___hitSprites_6 = value;
		Il2CppCodeGenWriteBarrier((&___hitSprites_6), value);
	}

	inline static int32_t get_offset_of_level_7() { return static_cast<int32_t>(offsetof(Block_t1429612866, ___level_7)); }
	inline Level_t2237665516 * get_level_7() const { return ___level_7; }
	inline Level_t2237665516 ** get_address_of_level_7() { return &___level_7; }
	inline void set_level_7(Level_t2237665516 * value)
	{
		___level_7 = value;
		Il2CppCodeGenWriteBarrier((&___level_7), value);
	}

	inline static int32_t get_offset_of_timesHit_8() { return static_cast<int32_t>(offsetof(Block_t1429612866, ___timesHit_8)); }
	inline int32_t get_timesHit_8() const { return ___timesHit_8; }
	inline int32_t* get_address_of_timesHit_8() { return &___timesHit_8; }
	inline void set_timesHit_8(int32_t value)
	{
		___timesHit_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCK_T1429612866_H
#ifndef CHATCONTROLLER_T3486202795_H
#define CHATCONTROLLER_T3486202795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatController
struct  ChatController_t3486202795  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_InputField ChatController::TMP_ChatInput
	TMP_InputField_t1099764886 * ___TMP_ChatInput_4;
	// TMPro.TMP_Text ChatController::TMP_ChatOutput
	TMP_Text_t2599618874 * ___TMP_ChatOutput_5;
	// UnityEngine.UI.Scrollbar ChatController::ChatScrollbar
	Scrollbar_t1494447233 * ___ChatScrollbar_6;

public:
	inline static int32_t get_offset_of_TMP_ChatInput_4() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___TMP_ChatInput_4)); }
	inline TMP_InputField_t1099764886 * get_TMP_ChatInput_4() const { return ___TMP_ChatInput_4; }
	inline TMP_InputField_t1099764886 ** get_address_of_TMP_ChatInput_4() { return &___TMP_ChatInput_4; }
	inline void set_TMP_ChatInput_4(TMP_InputField_t1099764886 * value)
	{
		___TMP_ChatInput_4 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatInput_4), value);
	}

	inline static int32_t get_offset_of_TMP_ChatOutput_5() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___TMP_ChatOutput_5)); }
	inline TMP_Text_t2599618874 * get_TMP_ChatOutput_5() const { return ___TMP_ChatOutput_5; }
	inline TMP_Text_t2599618874 ** get_address_of_TMP_ChatOutput_5() { return &___TMP_ChatOutput_5; }
	inline void set_TMP_ChatOutput_5(TMP_Text_t2599618874 * value)
	{
		___TMP_ChatOutput_5 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatOutput_5), value);
	}

	inline static int32_t get_offset_of_ChatScrollbar_6() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___ChatScrollbar_6)); }
	inline Scrollbar_t1494447233 * get_ChatScrollbar_6() const { return ___ChatScrollbar_6; }
	inline Scrollbar_t1494447233 ** get_address_of_ChatScrollbar_6() { return &___ChatScrollbar_6; }
	inline void set_ChatScrollbar_6(Scrollbar_t1494447233 * value)
	{
		___ChatScrollbar_6 = value;
		Il2CppCodeGenWriteBarrier((&___ChatScrollbar_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATCONTROLLER_T3486202795_H
#ifndef ENVMAPANIMATOR_T1140999784_H
#define ENVMAPANIMATOR_T1140999784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator
struct  EnvMapAnimator_t1140999784  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 EnvMapAnimator::RotationSpeeds
	Vector3_t3722313464  ___RotationSpeeds_4;
	// TMPro.TMP_Text EnvMapAnimator::m_textMeshPro
	TMP_Text_t2599618874 * ___m_textMeshPro_5;
	// UnityEngine.Material EnvMapAnimator::m_material
	Material_t340375123 * ___m_material_6;

public:
	inline static int32_t get_offset_of_RotationSpeeds_4() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___RotationSpeeds_4)); }
	inline Vector3_t3722313464  get_RotationSpeeds_4() const { return ___RotationSpeeds_4; }
	inline Vector3_t3722313464 * get_address_of_RotationSpeeds_4() { return &___RotationSpeeds_4; }
	inline void set_RotationSpeeds_4(Vector3_t3722313464  value)
	{
		___RotationSpeeds_4 = value;
	}

	inline static int32_t get_offset_of_m_textMeshPro_5() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___m_textMeshPro_5)); }
	inline TMP_Text_t2599618874 * get_m_textMeshPro_5() const { return ___m_textMeshPro_5; }
	inline TMP_Text_t2599618874 ** get_address_of_m_textMeshPro_5() { return &___m_textMeshPro_5; }
	inline void set_m_textMeshPro_5(TMP_Text_t2599618874 * value)
	{
		___m_textMeshPro_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_5), value);
	}

	inline static int32_t get_offset_of_m_material_6() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___m_material_6)); }
	inline Material_t340375123 * get_m_material_6() const { return ___m_material_6; }
	inline Material_t340375123 ** get_address_of_m_material_6() { return &___m_material_6; }
	inline void set_m_material_6(Material_t340375123 * value)
	{
		___m_material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVMAPANIMATOR_T1140999784_H
#ifndef GAMESESSION_T4087811243_H
#define GAMESESSION_T4087811243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSession
struct  GameSession_t4087811243  : public MonoBehaviour_t3962482529
{
public:
	// System.Single GameSession::gameSpeed
	float ___gameSpeed_4;
	// System.Int32 GameSession::pointsPerBlockDestroyed
	int32_t ___pointsPerBlockDestroyed_5;
	// TMPro.TextMeshProUGUI GameSession::scoreText
	TextMeshProUGUI_t529313277 * ___scoreText_6;
	// System.Boolean GameSession::isAutoPlayEnabled
	bool ___isAutoPlayEnabled_7;
	// System.Int32 GameSession::currentScore
	int32_t ___currentScore_8;

public:
	inline static int32_t get_offset_of_gameSpeed_4() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___gameSpeed_4)); }
	inline float get_gameSpeed_4() const { return ___gameSpeed_4; }
	inline float* get_address_of_gameSpeed_4() { return &___gameSpeed_4; }
	inline void set_gameSpeed_4(float value)
	{
		___gameSpeed_4 = value;
	}

	inline static int32_t get_offset_of_pointsPerBlockDestroyed_5() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___pointsPerBlockDestroyed_5)); }
	inline int32_t get_pointsPerBlockDestroyed_5() const { return ___pointsPerBlockDestroyed_5; }
	inline int32_t* get_address_of_pointsPerBlockDestroyed_5() { return &___pointsPerBlockDestroyed_5; }
	inline void set_pointsPerBlockDestroyed_5(int32_t value)
	{
		___pointsPerBlockDestroyed_5 = value;
	}

	inline static int32_t get_offset_of_scoreText_6() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___scoreText_6)); }
	inline TextMeshProUGUI_t529313277 * get_scoreText_6() const { return ___scoreText_6; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_scoreText_6() { return &___scoreText_6; }
	inline void set_scoreText_6(TextMeshProUGUI_t529313277 * value)
	{
		___scoreText_6 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_6), value);
	}

	inline static int32_t get_offset_of_isAutoPlayEnabled_7() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___isAutoPlayEnabled_7)); }
	inline bool get_isAutoPlayEnabled_7() const { return ___isAutoPlayEnabled_7; }
	inline bool* get_address_of_isAutoPlayEnabled_7() { return &___isAutoPlayEnabled_7; }
	inline void set_isAutoPlayEnabled_7(bool value)
	{
		___isAutoPlayEnabled_7 = value;
	}

	inline static int32_t get_offset_of_currentScore_8() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___currentScore_8)); }
	inline int32_t get_currentScore_8() const { return ___currentScore_8; }
	inline int32_t* get_address_of_currentScore_8() { return &___currentScore_8; }
	inline void set_currentScore_8(int32_t value)
	{
		___currentScore_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESESSION_T4087811243_H
#ifndef LEVEL_T2237665516_H
#define LEVEL_T2237665516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level
struct  Level_t2237665516  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 Level::breakableBlocks
	int32_t ___breakableBlocks_4;
	// SceneManeger Level::sceneManeger
	SceneManeger_t385596978 * ___sceneManeger_5;

public:
	inline static int32_t get_offset_of_breakableBlocks_4() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___breakableBlocks_4)); }
	inline int32_t get_breakableBlocks_4() const { return ___breakableBlocks_4; }
	inline int32_t* get_address_of_breakableBlocks_4() { return &___breakableBlocks_4; }
	inline void set_breakableBlocks_4(int32_t value)
	{
		___breakableBlocks_4 = value;
	}

	inline static int32_t get_offset_of_sceneManeger_5() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___sceneManeger_5)); }
	inline SceneManeger_t385596978 * get_sceneManeger_5() const { return ___sceneManeger_5; }
	inline SceneManeger_t385596978 ** get_address_of_sceneManeger_5() { return &___sceneManeger_5; }
	inline void set_sceneManeger_5(SceneManeger_t385596978 * value)
	{
		___sceneManeger_5 = value;
		Il2CppCodeGenWriteBarrier((&___sceneManeger_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL_T2237665516_H
#ifndef LOSECOLLIDER_T2109713310_H
#define LOSECOLLIDER_T2109713310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoseCollider
struct  LoseCollider_t2109713310  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOSECOLLIDER_T2109713310_H
#ifndef PADDLE_T4140593882_H
#define PADDLE_T4140593882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Paddle
struct  Paddle_t4140593882  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Paddle::minX
	float ___minX_4;
	// System.Single Paddle::maxX
	float ___maxX_5;
	// UnityEngine.Vector3 Paddle::unityPaddlePos
	Vector3_t3722313464  ___unityPaddlePos_6;
	// GameSession Paddle::gameSession
	GameSession_t4087811243 * ___gameSession_7;
	// Ball Paddle::ball
	Ball_t2206666566 * ___ball_8;
	// System.Single Paddle::screenWidthInUnits
	float ___screenWidthInUnits_9;

public:
	inline static int32_t get_offset_of_minX_4() { return static_cast<int32_t>(offsetof(Paddle_t4140593882, ___minX_4)); }
	inline float get_minX_4() const { return ___minX_4; }
	inline float* get_address_of_minX_4() { return &___minX_4; }
	inline void set_minX_4(float value)
	{
		___minX_4 = value;
	}

	inline static int32_t get_offset_of_maxX_5() { return static_cast<int32_t>(offsetof(Paddle_t4140593882, ___maxX_5)); }
	inline float get_maxX_5() const { return ___maxX_5; }
	inline float* get_address_of_maxX_5() { return &___maxX_5; }
	inline void set_maxX_5(float value)
	{
		___maxX_5 = value;
	}

	inline static int32_t get_offset_of_unityPaddlePos_6() { return static_cast<int32_t>(offsetof(Paddle_t4140593882, ___unityPaddlePos_6)); }
	inline Vector3_t3722313464  get_unityPaddlePos_6() const { return ___unityPaddlePos_6; }
	inline Vector3_t3722313464 * get_address_of_unityPaddlePos_6() { return &___unityPaddlePos_6; }
	inline void set_unityPaddlePos_6(Vector3_t3722313464  value)
	{
		___unityPaddlePos_6 = value;
	}

	inline static int32_t get_offset_of_gameSession_7() { return static_cast<int32_t>(offsetof(Paddle_t4140593882, ___gameSession_7)); }
	inline GameSession_t4087811243 * get_gameSession_7() const { return ___gameSession_7; }
	inline GameSession_t4087811243 ** get_address_of_gameSession_7() { return &___gameSession_7; }
	inline void set_gameSession_7(GameSession_t4087811243 * value)
	{
		___gameSession_7 = value;
		Il2CppCodeGenWriteBarrier((&___gameSession_7), value);
	}

	inline static int32_t get_offset_of_ball_8() { return static_cast<int32_t>(offsetof(Paddle_t4140593882, ___ball_8)); }
	inline Ball_t2206666566 * get_ball_8() const { return ___ball_8; }
	inline Ball_t2206666566 ** get_address_of_ball_8() { return &___ball_8; }
	inline void set_ball_8(Ball_t2206666566 * value)
	{
		___ball_8 = value;
		Il2CppCodeGenWriteBarrier((&___ball_8), value);
	}

	inline static int32_t get_offset_of_screenWidthInUnits_9() { return static_cast<int32_t>(offsetof(Paddle_t4140593882, ___screenWidthInUnits_9)); }
	inline float get_screenWidthInUnits_9() const { return ___screenWidthInUnits_9; }
	inline float* get_address_of_screenWidthInUnits_9() { return &___screenWidthInUnits_9; }
	inline void set_screenWidthInUnits_9(float value)
	{
		___screenWidthInUnits_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADDLE_T4140593882_H
#ifndef SCENEMANEGER_T385596978_H
#define SCENEMANEGER_T385596978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneManeger
struct  SceneManeger_t385596978  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEMANEGER_T385596978_H
#ifndef BENCHMARK01_T1571072624_H
#define BENCHMARK01_T1571072624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01
struct  Benchmark01_t1571072624  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark01::BenchmarkType
	int32_t ___BenchmarkType_4;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01::TMProFont
	TMP_FontAsset_t364381626 * ___TMProFont_5;
	// UnityEngine.Font TMPro.Examples.Benchmark01::TextMeshFont
	Font_t1956802104 * ___TextMeshFont_6;
	// TMPro.TextMeshPro TMPro.Examples.Benchmark01::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_7;
	// TMPro.TextContainer TMPro.Examples.Benchmark01::m_textContainer
	TextContainer_t97923372 * ___m_textContainer_8;
	// UnityEngine.TextMesh TMPro.Examples.Benchmark01::m_textMesh
	TextMesh_t1536577757 * ___m_textMesh_9;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material01
	Material_t340375123 * ___m_material01_12;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material02
	Material_t340375123 * ___m_material02_13;

public:
	inline static int32_t get_offset_of_BenchmarkType_4() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___BenchmarkType_4)); }
	inline int32_t get_BenchmarkType_4() const { return ___BenchmarkType_4; }
	inline int32_t* get_address_of_BenchmarkType_4() { return &___BenchmarkType_4; }
	inline void set_BenchmarkType_4(int32_t value)
	{
		___BenchmarkType_4 = value;
	}

	inline static int32_t get_offset_of_TMProFont_5() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___TMProFont_5)); }
	inline TMP_FontAsset_t364381626 * get_TMProFont_5() const { return ___TMProFont_5; }
	inline TMP_FontAsset_t364381626 ** get_address_of_TMProFont_5() { return &___TMProFont_5; }
	inline void set_TMProFont_5(TMP_FontAsset_t364381626 * value)
	{
		___TMProFont_5 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_5), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_6() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___TextMeshFont_6)); }
	inline Font_t1956802104 * get_TextMeshFont_6() const { return ___TextMeshFont_6; }
	inline Font_t1956802104 ** get_address_of_TextMeshFont_6() { return &___TextMeshFont_6; }
	inline void set_TextMeshFont_6(Font_t1956802104 * value)
	{
		___TextMeshFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_6), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_7() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textMeshPro_7)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_7() const { return ___m_textMeshPro_7; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_7() { return &___m_textMeshPro_7; }
	inline void set_m_textMeshPro_7(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_7), value);
	}

	inline static int32_t get_offset_of_m_textContainer_8() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textContainer_8)); }
	inline TextContainer_t97923372 * get_m_textContainer_8() const { return ___m_textContainer_8; }
	inline TextContainer_t97923372 ** get_address_of_m_textContainer_8() { return &___m_textContainer_8; }
	inline void set_m_textContainer_8(TextContainer_t97923372 * value)
	{
		___m_textContainer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainer_8), value);
	}

	inline static int32_t get_offset_of_m_textMesh_9() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textMesh_9)); }
	inline TextMesh_t1536577757 * get_m_textMesh_9() const { return ___m_textMesh_9; }
	inline TextMesh_t1536577757 ** get_address_of_m_textMesh_9() { return &___m_textMesh_9; }
	inline void set_m_textMesh_9(TextMesh_t1536577757 * value)
	{
		___m_textMesh_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_9), value);
	}

	inline static int32_t get_offset_of_m_material01_12() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_material01_12)); }
	inline Material_t340375123 * get_m_material01_12() const { return ___m_material01_12; }
	inline Material_t340375123 ** get_address_of_m_material01_12() { return &___m_material01_12; }
	inline void set_m_material01_12(Material_t340375123 * value)
	{
		___m_material01_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_12), value);
	}

	inline static int32_t get_offset_of_m_material02_13() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_material02_13)); }
	inline Material_t340375123 * get_m_material02_13() const { return ___m_material02_13; }
	inline Material_t340375123 ** get_address_of_m_material02_13() { return &___m_material02_13; }
	inline void set_m_material02_13(Material_t340375123 * value)
	{
		___m_material02_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_T1571072624_H
#ifndef BENCHMARK01_UGUI_T3264177817_H
#define BENCHMARK01_UGUI_T3264177817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI
struct  Benchmark01_UGUI_t3264177817  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI::BenchmarkType
	int32_t ___BenchmarkType_4;
	// UnityEngine.Canvas TMPro.Examples.Benchmark01_UGUI::canvas
	Canvas_t3310196443 * ___canvas_5;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01_UGUI::TMProFont
	TMP_FontAsset_t364381626 * ___TMProFont_6;
	// UnityEngine.Font TMPro.Examples.Benchmark01_UGUI::TextMeshFont
	Font_t1956802104 * ___TextMeshFont_7;
	// TMPro.TextMeshProUGUI TMPro.Examples.Benchmark01_UGUI::m_textMeshPro
	TextMeshProUGUI_t529313277 * ___m_textMeshPro_8;
	// UnityEngine.UI.Text TMPro.Examples.Benchmark01_UGUI::m_textMesh
	Text_t1901882714 * ___m_textMesh_9;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material01
	Material_t340375123 * ___m_material01_12;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material02
	Material_t340375123 * ___m_material02_13;

public:
	inline static int32_t get_offset_of_BenchmarkType_4() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___BenchmarkType_4)); }
	inline int32_t get_BenchmarkType_4() const { return ___BenchmarkType_4; }
	inline int32_t* get_address_of_BenchmarkType_4() { return &___BenchmarkType_4; }
	inline void set_BenchmarkType_4(int32_t value)
	{
		___BenchmarkType_4 = value;
	}

	inline static int32_t get_offset_of_canvas_5() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___canvas_5)); }
	inline Canvas_t3310196443 * get_canvas_5() const { return ___canvas_5; }
	inline Canvas_t3310196443 ** get_address_of_canvas_5() { return &___canvas_5; }
	inline void set_canvas_5(Canvas_t3310196443 * value)
	{
		___canvas_5 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_5), value);
	}

	inline static int32_t get_offset_of_TMProFont_6() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TMProFont_6)); }
	inline TMP_FontAsset_t364381626 * get_TMProFont_6() const { return ___TMProFont_6; }
	inline TMP_FontAsset_t364381626 ** get_address_of_TMProFont_6() { return &___TMProFont_6; }
	inline void set_TMProFont_6(TMP_FontAsset_t364381626 * value)
	{
		___TMProFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_6), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_7() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TextMeshFont_7)); }
	inline Font_t1956802104 * get_TextMeshFont_7() const { return ___TextMeshFont_7; }
	inline Font_t1956802104 ** get_address_of_TextMeshFont_7() { return &___TextMeshFont_7; }
	inline void set_TextMeshFont_7(Font_t1956802104 * value)
	{
		___TextMeshFont_7 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_7), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_8() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMeshPro_8)); }
	inline TextMeshProUGUI_t529313277 * get_m_textMeshPro_8() const { return ___m_textMeshPro_8; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_textMeshPro_8() { return &___m_textMeshPro_8; }
	inline void set_m_textMeshPro_8(TextMeshProUGUI_t529313277 * value)
	{
		___m_textMeshPro_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_8), value);
	}

	inline static int32_t get_offset_of_m_textMesh_9() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMesh_9)); }
	inline Text_t1901882714 * get_m_textMesh_9() const { return ___m_textMesh_9; }
	inline Text_t1901882714 ** get_address_of_m_textMesh_9() { return &___m_textMesh_9; }
	inline void set_m_textMesh_9(Text_t1901882714 * value)
	{
		___m_textMesh_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_9), value);
	}

	inline static int32_t get_offset_of_m_material01_12() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material01_12)); }
	inline Material_t340375123 * get_m_material01_12() const { return ___m_material01_12; }
	inline Material_t340375123 ** get_address_of_m_material01_12() { return &___m_material01_12; }
	inline void set_m_material01_12(Material_t340375123 * value)
	{
		___m_material01_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_12), value);
	}

	inline static int32_t get_offset_of_m_material02_13() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material02_13)); }
	inline Material_t340375123 * get_m_material02_13() const { return ___m_material02_13; }
	inline Material_t340375123 ** get_address_of_m_material02_13() { return &___m_material02_13; }
	inline void set_m_material02_13(Material_t340375123 * value)
	{
		___m_material02_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_UGUI_T3264177817_H
#ifndef BENCHMARK02_T1571269232_H
#define BENCHMARK02_T1571269232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark02
struct  Benchmark02_t1571269232  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark02::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.Benchmark02::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.Benchmark02::floatingText_Script
	TextMeshProFloatingText_t845872552 * ___floatingText_Script_6;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_floatingText_Script_6() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___floatingText_Script_6)); }
	inline TextMeshProFloatingText_t845872552 * get_floatingText_Script_6() const { return ___floatingText_Script_6; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_floatingText_Script_6() { return &___floatingText_Script_6; }
	inline void set_floatingText_Script_6(TextMeshProFloatingText_t845872552 * value)
	{
		___floatingText_Script_6 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK02_T1571269232_H
#ifndef BENCHMARK03_T1571203696_H
#define BENCHMARK03_T1571203696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark03
struct  Benchmark03_t1571203696  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark03::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.Benchmark03::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// UnityEngine.Font TMPro.Examples.Benchmark03::TheFont
	Font_t1956802104 * ___TheFont_6;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_TheFont_6() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___TheFont_6)); }
	inline Font_t1956802104 * get_TheFont_6() const { return ___TheFont_6; }
	inline Font_t1956802104 ** get_address_of_TheFont_6() { return &___TheFont_6; }
	inline void set_TheFont_6(Font_t1956802104 * value)
	{
		___TheFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK03_T1571203696_H
#ifndef BENCHMARK04_T1570876016_H
#define BENCHMARK04_T1570876016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark04
struct  Benchmark04_t1570876016  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark04::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.Benchmark04::MinPointSize
	int32_t ___MinPointSize_5;
	// System.Int32 TMPro.Examples.Benchmark04::MaxPointSize
	int32_t ___MaxPointSize_6;
	// System.Int32 TMPro.Examples.Benchmark04::Steps
	int32_t ___Steps_7;
	// UnityEngine.Transform TMPro.Examples.Benchmark04::m_Transform
	Transform_t3600365921 * ___m_Transform_8;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_MinPointSize_5() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___MinPointSize_5)); }
	inline int32_t get_MinPointSize_5() const { return ___MinPointSize_5; }
	inline int32_t* get_address_of_MinPointSize_5() { return &___MinPointSize_5; }
	inline void set_MinPointSize_5(int32_t value)
	{
		___MinPointSize_5 = value;
	}

	inline static int32_t get_offset_of_MaxPointSize_6() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___MaxPointSize_6)); }
	inline int32_t get_MaxPointSize_6() const { return ___MaxPointSize_6; }
	inline int32_t* get_address_of_MaxPointSize_6() { return &___MaxPointSize_6; }
	inline void set_MaxPointSize_6(int32_t value)
	{
		___MaxPointSize_6 = value;
	}

	inline static int32_t get_offset_of_Steps_7() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___Steps_7)); }
	inline int32_t get_Steps_7() const { return ___Steps_7; }
	inline int32_t* get_address_of_Steps_7() { return &___Steps_7; }
	inline void set_Steps_7(int32_t value)
	{
		___Steps_7 = value;
	}

	inline static int32_t get_offset_of_m_Transform_8() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___m_Transform_8)); }
	inline Transform_t3600365921 * get_m_Transform_8() const { return ___m_Transform_8; }
	inline Transform_t3600365921 ** get_address_of_m_Transform_8() { return &___m_Transform_8; }
	inline void set_m_Transform_8(Transform_t3600365921 * value)
	{
		___m_Transform_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transform_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK04_T1570876016_H
#ifndef CAMERACONTROLLER_T2264742161_H
#define CAMERACONTROLLER_T2264742161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController
struct  CameraController_t2264742161  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform TMPro.Examples.CameraController::cameraTransform
	Transform_t3600365921 * ___cameraTransform_4;
	// UnityEngine.Transform TMPro.Examples.CameraController::dummyTarget
	Transform_t3600365921 * ___dummyTarget_5;
	// UnityEngine.Transform TMPro.Examples.CameraController::CameraTarget
	Transform_t3600365921 * ___CameraTarget_6;
	// System.Single TMPro.Examples.CameraController::FollowDistance
	float ___FollowDistance_7;
	// System.Single TMPro.Examples.CameraController::MaxFollowDistance
	float ___MaxFollowDistance_8;
	// System.Single TMPro.Examples.CameraController::MinFollowDistance
	float ___MinFollowDistance_9;
	// System.Single TMPro.Examples.CameraController::ElevationAngle
	float ___ElevationAngle_10;
	// System.Single TMPro.Examples.CameraController::MaxElevationAngle
	float ___MaxElevationAngle_11;
	// System.Single TMPro.Examples.CameraController::MinElevationAngle
	float ___MinElevationAngle_12;
	// System.Single TMPro.Examples.CameraController::OrbitalAngle
	float ___OrbitalAngle_13;
	// TMPro.Examples.CameraController/CameraModes TMPro.Examples.CameraController::CameraMode
	int32_t ___CameraMode_14;
	// System.Boolean TMPro.Examples.CameraController::MovementSmoothing
	bool ___MovementSmoothing_15;
	// System.Boolean TMPro.Examples.CameraController::RotationSmoothing
	bool ___RotationSmoothing_16;
	// System.Boolean TMPro.Examples.CameraController::previousSmoothing
	bool ___previousSmoothing_17;
	// System.Single TMPro.Examples.CameraController::MovementSmoothingValue
	float ___MovementSmoothingValue_18;
	// System.Single TMPro.Examples.CameraController::RotationSmoothingValue
	float ___RotationSmoothingValue_19;
	// System.Single TMPro.Examples.CameraController::MoveSensitivity
	float ___MoveSensitivity_20;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::currentVelocity
	Vector3_t3722313464  ___currentVelocity_21;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::desiredPosition
	Vector3_t3722313464  ___desiredPosition_22;
	// System.Single TMPro.Examples.CameraController::mouseX
	float ___mouseX_23;
	// System.Single TMPro.Examples.CameraController::mouseY
	float ___mouseY_24;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::moveVector
	Vector3_t3722313464  ___moveVector_25;
	// System.Single TMPro.Examples.CameraController::mouseWheel
	float ___mouseWheel_26;

public:
	inline static int32_t get_offset_of_cameraTransform_4() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___cameraTransform_4)); }
	inline Transform_t3600365921 * get_cameraTransform_4() const { return ___cameraTransform_4; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_4() { return &___cameraTransform_4; }
	inline void set_cameraTransform_4(Transform_t3600365921 * value)
	{
		___cameraTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_4), value);
	}

	inline static int32_t get_offset_of_dummyTarget_5() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___dummyTarget_5)); }
	inline Transform_t3600365921 * get_dummyTarget_5() const { return ___dummyTarget_5; }
	inline Transform_t3600365921 ** get_address_of_dummyTarget_5() { return &___dummyTarget_5; }
	inline void set_dummyTarget_5(Transform_t3600365921 * value)
	{
		___dummyTarget_5 = value;
		Il2CppCodeGenWriteBarrier((&___dummyTarget_5), value);
	}

	inline static int32_t get_offset_of_CameraTarget_6() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___CameraTarget_6)); }
	inline Transform_t3600365921 * get_CameraTarget_6() const { return ___CameraTarget_6; }
	inline Transform_t3600365921 ** get_address_of_CameraTarget_6() { return &___CameraTarget_6; }
	inline void set_CameraTarget_6(Transform_t3600365921 * value)
	{
		___CameraTarget_6 = value;
		Il2CppCodeGenWriteBarrier((&___CameraTarget_6), value);
	}

	inline static int32_t get_offset_of_FollowDistance_7() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___FollowDistance_7)); }
	inline float get_FollowDistance_7() const { return ___FollowDistance_7; }
	inline float* get_address_of_FollowDistance_7() { return &___FollowDistance_7; }
	inline void set_FollowDistance_7(float value)
	{
		___FollowDistance_7 = value;
	}

	inline static int32_t get_offset_of_MaxFollowDistance_8() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MaxFollowDistance_8)); }
	inline float get_MaxFollowDistance_8() const { return ___MaxFollowDistance_8; }
	inline float* get_address_of_MaxFollowDistance_8() { return &___MaxFollowDistance_8; }
	inline void set_MaxFollowDistance_8(float value)
	{
		___MaxFollowDistance_8 = value;
	}

	inline static int32_t get_offset_of_MinFollowDistance_9() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MinFollowDistance_9)); }
	inline float get_MinFollowDistance_9() const { return ___MinFollowDistance_9; }
	inline float* get_address_of_MinFollowDistance_9() { return &___MinFollowDistance_9; }
	inline void set_MinFollowDistance_9(float value)
	{
		___MinFollowDistance_9 = value;
	}

	inline static int32_t get_offset_of_ElevationAngle_10() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___ElevationAngle_10)); }
	inline float get_ElevationAngle_10() const { return ___ElevationAngle_10; }
	inline float* get_address_of_ElevationAngle_10() { return &___ElevationAngle_10; }
	inline void set_ElevationAngle_10(float value)
	{
		___ElevationAngle_10 = value;
	}

	inline static int32_t get_offset_of_MaxElevationAngle_11() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MaxElevationAngle_11)); }
	inline float get_MaxElevationAngle_11() const { return ___MaxElevationAngle_11; }
	inline float* get_address_of_MaxElevationAngle_11() { return &___MaxElevationAngle_11; }
	inline void set_MaxElevationAngle_11(float value)
	{
		___MaxElevationAngle_11 = value;
	}

	inline static int32_t get_offset_of_MinElevationAngle_12() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MinElevationAngle_12)); }
	inline float get_MinElevationAngle_12() const { return ___MinElevationAngle_12; }
	inline float* get_address_of_MinElevationAngle_12() { return &___MinElevationAngle_12; }
	inline void set_MinElevationAngle_12(float value)
	{
		___MinElevationAngle_12 = value;
	}

	inline static int32_t get_offset_of_OrbitalAngle_13() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___OrbitalAngle_13)); }
	inline float get_OrbitalAngle_13() const { return ___OrbitalAngle_13; }
	inline float* get_address_of_OrbitalAngle_13() { return &___OrbitalAngle_13; }
	inline void set_OrbitalAngle_13(float value)
	{
		___OrbitalAngle_13 = value;
	}

	inline static int32_t get_offset_of_CameraMode_14() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___CameraMode_14)); }
	inline int32_t get_CameraMode_14() const { return ___CameraMode_14; }
	inline int32_t* get_address_of_CameraMode_14() { return &___CameraMode_14; }
	inline void set_CameraMode_14(int32_t value)
	{
		___CameraMode_14 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothing_15() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MovementSmoothing_15)); }
	inline bool get_MovementSmoothing_15() const { return ___MovementSmoothing_15; }
	inline bool* get_address_of_MovementSmoothing_15() { return &___MovementSmoothing_15; }
	inline void set_MovementSmoothing_15(bool value)
	{
		___MovementSmoothing_15 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothing_16() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___RotationSmoothing_16)); }
	inline bool get_RotationSmoothing_16() const { return ___RotationSmoothing_16; }
	inline bool* get_address_of_RotationSmoothing_16() { return &___RotationSmoothing_16; }
	inline void set_RotationSmoothing_16(bool value)
	{
		___RotationSmoothing_16 = value;
	}

	inline static int32_t get_offset_of_previousSmoothing_17() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___previousSmoothing_17)); }
	inline bool get_previousSmoothing_17() const { return ___previousSmoothing_17; }
	inline bool* get_address_of_previousSmoothing_17() { return &___previousSmoothing_17; }
	inline void set_previousSmoothing_17(bool value)
	{
		___previousSmoothing_17 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothingValue_18() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MovementSmoothingValue_18)); }
	inline float get_MovementSmoothingValue_18() const { return ___MovementSmoothingValue_18; }
	inline float* get_address_of_MovementSmoothingValue_18() { return &___MovementSmoothingValue_18; }
	inline void set_MovementSmoothingValue_18(float value)
	{
		___MovementSmoothingValue_18 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothingValue_19() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___RotationSmoothingValue_19)); }
	inline float get_RotationSmoothingValue_19() const { return ___RotationSmoothingValue_19; }
	inline float* get_address_of_RotationSmoothingValue_19() { return &___RotationSmoothingValue_19; }
	inline void set_RotationSmoothingValue_19(float value)
	{
		___RotationSmoothingValue_19 = value;
	}

	inline static int32_t get_offset_of_MoveSensitivity_20() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MoveSensitivity_20)); }
	inline float get_MoveSensitivity_20() const { return ___MoveSensitivity_20; }
	inline float* get_address_of_MoveSensitivity_20() { return &___MoveSensitivity_20; }
	inline void set_MoveSensitivity_20(float value)
	{
		___MoveSensitivity_20 = value;
	}

	inline static int32_t get_offset_of_currentVelocity_21() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___currentVelocity_21)); }
	inline Vector3_t3722313464  get_currentVelocity_21() const { return ___currentVelocity_21; }
	inline Vector3_t3722313464 * get_address_of_currentVelocity_21() { return &___currentVelocity_21; }
	inline void set_currentVelocity_21(Vector3_t3722313464  value)
	{
		___currentVelocity_21 = value;
	}

	inline static int32_t get_offset_of_desiredPosition_22() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___desiredPosition_22)); }
	inline Vector3_t3722313464  get_desiredPosition_22() const { return ___desiredPosition_22; }
	inline Vector3_t3722313464 * get_address_of_desiredPosition_22() { return &___desiredPosition_22; }
	inline void set_desiredPosition_22(Vector3_t3722313464  value)
	{
		___desiredPosition_22 = value;
	}

	inline static int32_t get_offset_of_mouseX_23() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseX_23)); }
	inline float get_mouseX_23() const { return ___mouseX_23; }
	inline float* get_address_of_mouseX_23() { return &___mouseX_23; }
	inline void set_mouseX_23(float value)
	{
		___mouseX_23 = value;
	}

	inline static int32_t get_offset_of_mouseY_24() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseY_24)); }
	inline float get_mouseY_24() const { return ___mouseY_24; }
	inline float* get_address_of_mouseY_24() { return &___mouseY_24; }
	inline void set_mouseY_24(float value)
	{
		___mouseY_24 = value;
	}

	inline static int32_t get_offset_of_moveVector_25() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___moveVector_25)); }
	inline Vector3_t3722313464  get_moveVector_25() const { return ___moveVector_25; }
	inline Vector3_t3722313464 * get_address_of_moveVector_25() { return &___moveVector_25; }
	inline void set_moveVector_25(Vector3_t3722313464  value)
	{
		___moveVector_25 = value;
	}

	inline static int32_t get_offset_of_mouseWheel_26() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseWheel_26)); }
	inline float get_mouseWheel_26() const { return ___mouseWheel_26; }
	inline float* get_address_of_mouseWheel_26() { return &___mouseWheel_26; }
	inline void set_mouseWheel_26(float value)
	{
		___mouseWheel_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T2264742161_H
#ifndef OBJECTSPIN_T341713598_H
#define OBJECTSPIN_T341713598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin
struct  ObjectSpin_t341713598  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.ObjectSpin::SpinSpeed
	float ___SpinSpeed_4;
	// System.Int32 TMPro.Examples.ObjectSpin::RotationRange
	int32_t ___RotationRange_5;
	// UnityEngine.Transform TMPro.Examples.ObjectSpin::m_transform
	Transform_t3600365921 * ___m_transform_6;
	// System.Single TMPro.Examples.ObjectSpin::m_time
	float ___m_time_7;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_prevPOS
	Vector3_t3722313464  ___m_prevPOS_8;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Rotation
	Vector3_t3722313464  ___m_initial_Rotation_9;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Position
	Vector3_t3722313464  ___m_initial_Position_10;
	// UnityEngine.Color32 TMPro.Examples.ObjectSpin::m_lightColor
	Color32_t2600501292  ___m_lightColor_11;
	// System.Int32 TMPro.Examples.ObjectSpin::frames
	int32_t ___frames_12;
	// TMPro.Examples.ObjectSpin/MotionType TMPro.Examples.ObjectSpin::Motion
	int32_t ___Motion_13;

public:
	inline static int32_t get_offset_of_SpinSpeed_4() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___SpinSpeed_4)); }
	inline float get_SpinSpeed_4() const { return ___SpinSpeed_4; }
	inline float* get_address_of_SpinSpeed_4() { return &___SpinSpeed_4; }
	inline void set_SpinSpeed_4(float value)
	{
		___SpinSpeed_4 = value;
	}

	inline static int32_t get_offset_of_RotationRange_5() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___RotationRange_5)); }
	inline int32_t get_RotationRange_5() const { return ___RotationRange_5; }
	inline int32_t* get_address_of_RotationRange_5() { return &___RotationRange_5; }
	inline void set_RotationRange_5(int32_t value)
	{
		___RotationRange_5 = value;
	}

	inline static int32_t get_offset_of_m_transform_6() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_transform_6)); }
	inline Transform_t3600365921 * get_m_transform_6() const { return ___m_transform_6; }
	inline Transform_t3600365921 ** get_address_of_m_transform_6() { return &___m_transform_6; }
	inline void set_m_transform_6(Transform_t3600365921 * value)
	{
		___m_transform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_6), value);
	}

	inline static int32_t get_offset_of_m_time_7() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_time_7)); }
	inline float get_m_time_7() const { return ___m_time_7; }
	inline float* get_address_of_m_time_7() { return &___m_time_7; }
	inline void set_m_time_7(float value)
	{
		___m_time_7 = value;
	}

	inline static int32_t get_offset_of_m_prevPOS_8() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_prevPOS_8)); }
	inline Vector3_t3722313464  get_m_prevPOS_8() const { return ___m_prevPOS_8; }
	inline Vector3_t3722313464 * get_address_of_m_prevPOS_8() { return &___m_prevPOS_8; }
	inline void set_m_prevPOS_8(Vector3_t3722313464  value)
	{
		___m_prevPOS_8 = value;
	}

	inline static int32_t get_offset_of_m_initial_Rotation_9() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_initial_Rotation_9)); }
	inline Vector3_t3722313464  get_m_initial_Rotation_9() const { return ___m_initial_Rotation_9; }
	inline Vector3_t3722313464 * get_address_of_m_initial_Rotation_9() { return &___m_initial_Rotation_9; }
	inline void set_m_initial_Rotation_9(Vector3_t3722313464  value)
	{
		___m_initial_Rotation_9 = value;
	}

	inline static int32_t get_offset_of_m_initial_Position_10() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_initial_Position_10)); }
	inline Vector3_t3722313464  get_m_initial_Position_10() const { return ___m_initial_Position_10; }
	inline Vector3_t3722313464 * get_address_of_m_initial_Position_10() { return &___m_initial_Position_10; }
	inline void set_m_initial_Position_10(Vector3_t3722313464  value)
	{
		___m_initial_Position_10 = value;
	}

	inline static int32_t get_offset_of_m_lightColor_11() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_lightColor_11)); }
	inline Color32_t2600501292  get_m_lightColor_11() const { return ___m_lightColor_11; }
	inline Color32_t2600501292 * get_address_of_m_lightColor_11() { return &___m_lightColor_11; }
	inline void set_m_lightColor_11(Color32_t2600501292  value)
	{
		___m_lightColor_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___frames_12)); }
	inline int32_t get_frames_12() const { return ___frames_12; }
	inline int32_t* get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(int32_t value)
	{
		___frames_12 = value;
	}

	inline static int32_t get_offset_of_Motion_13() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___Motion_13)); }
	inline int32_t get_Motion_13() const { return ___Motion_13; }
	inline int32_t* get_address_of_Motion_13() { return &___Motion_13; }
	inline void set_Motion_13(int32_t value)
	{
		___Motion_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSPIN_T341713598_H
#ifndef SHADERPROPANIMATOR_T3617420994_H
#define SHADERPROPANIMATOR_T3617420994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator
struct  ShaderPropAnimator_t3617420994  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Renderer TMPro.Examples.ShaderPropAnimator::m_Renderer
	Renderer_t2627027031 * ___m_Renderer_4;
	// UnityEngine.Material TMPro.Examples.ShaderPropAnimator::m_Material
	Material_t340375123 * ___m_Material_5;
	// UnityEngine.AnimationCurve TMPro.Examples.ShaderPropAnimator::GlowCurve
	AnimationCurve_t3046754366 * ___GlowCurve_6;
	// System.Single TMPro.Examples.ShaderPropAnimator::m_frame
	float ___m_frame_7;

public:
	inline static int32_t get_offset_of_m_Renderer_4() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_Renderer_4)); }
	inline Renderer_t2627027031 * get_m_Renderer_4() const { return ___m_Renderer_4; }
	inline Renderer_t2627027031 ** get_address_of_m_Renderer_4() { return &___m_Renderer_4; }
	inline void set_m_Renderer_4(Renderer_t2627027031 * value)
	{
		___m_Renderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Renderer_4), value);
	}

	inline static int32_t get_offset_of_m_Material_5() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_Material_5)); }
	inline Material_t340375123 * get_m_Material_5() const { return ___m_Material_5; }
	inline Material_t340375123 ** get_address_of_m_Material_5() { return &___m_Material_5; }
	inline void set_m_Material_5(Material_t340375123 * value)
	{
		___m_Material_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_5), value);
	}

	inline static int32_t get_offset_of_GlowCurve_6() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___GlowCurve_6)); }
	inline AnimationCurve_t3046754366 * get_GlowCurve_6() const { return ___GlowCurve_6; }
	inline AnimationCurve_t3046754366 ** get_address_of_GlowCurve_6() { return &___GlowCurve_6; }
	inline void set_GlowCurve_6(AnimationCurve_t3046754366 * value)
	{
		___GlowCurve_6 = value;
		Il2CppCodeGenWriteBarrier((&___GlowCurve_6), value);
	}

	inline static int32_t get_offset_of_m_frame_7() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_frame_7)); }
	inline float get_m_frame_7() const { return ___m_frame_7; }
	inline float* get_address_of_m_frame_7() { return &___m_frame_7; }
	inline void set_m_frame_7(float value)
	{
		___m_frame_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERPROPANIMATOR_T3617420994_H
#ifndef SIMPLESCRIPT_T3279312205_H
#define SIMPLESCRIPT_T3279312205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SimpleScript
struct  SimpleScript_t3279312205  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TextMeshPro TMPro.Examples.SimpleScript::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_4;
	// System.Single TMPro.Examples.SimpleScript::m_frame
	float ___m_frame_6;

public:
	inline static int32_t get_offset_of_m_textMeshPro_4() { return static_cast<int32_t>(offsetof(SimpleScript_t3279312205, ___m_textMeshPro_4)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_4() const { return ___m_textMeshPro_4; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_4() { return &___m_textMeshPro_4; }
	inline void set_m_textMeshPro_4(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_4), value);
	}

	inline static int32_t get_offset_of_m_frame_6() { return static_cast<int32_t>(offsetof(SimpleScript_t3279312205, ___m_frame_6)); }
	inline float get_m_frame_6() const { return ___m_frame_6; }
	inline float* get_address_of_m_frame_6() { return &___m_frame_6; }
	inline void set_m_frame_6(float value)
	{
		___m_frame_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLESCRIPT_T3279312205_H
#ifndef SKEWTEXTEXAMPLE_T3460249701_H
#define SKEWTEXTEXAMPLE_T3460249701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SkewTextExample
struct  SkewTextExample_t3460249701  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.Examples.SkewTextExample::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_4;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::VertexCurve
	AnimationCurve_t3046754366 * ___VertexCurve_5;
	// System.Single TMPro.Examples.SkewTextExample::CurveScale
	float ___CurveScale_6;
	// System.Single TMPro.Examples.SkewTextExample::ShearAmount
	float ___ShearAmount_7;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___m_TextComponent_4)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_4), value);
	}

	inline static int32_t get_offset_of_VertexCurve_5() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___VertexCurve_5)); }
	inline AnimationCurve_t3046754366 * get_VertexCurve_5() const { return ___VertexCurve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_VertexCurve_5() { return &___VertexCurve_5; }
	inline void set_VertexCurve_5(AnimationCurve_t3046754366 * value)
	{
		___VertexCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___VertexCurve_5), value);
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_ShearAmount_7() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___ShearAmount_7)); }
	inline float get_ShearAmount_7() const { return ___ShearAmount_7; }
	inline float* get_address_of_ShearAmount_7() { return &___ShearAmount_7; }
	inline void set_ShearAmount_7(float value)
	{
		___ShearAmount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKEWTEXTEXAMPLE_T3460249701_H
#ifndef TMP_EXAMPLESCRIPT_01_T3051742005_H
#define TMP_EXAMPLESCRIPT_01_T3051742005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_ExampleScript_01
struct  TMP_ExampleScript_01_t3051742005  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.Examples.TMP_ExampleScript_01/objectType TMPro.Examples.TMP_ExampleScript_01::ObjectType
	int32_t ___ObjectType_4;
	// System.Boolean TMPro.Examples.TMP_ExampleScript_01::isStatic
	bool ___isStatic_5;
	// TMPro.TMP_Text TMPro.Examples.TMP_ExampleScript_01::m_text
	TMP_Text_t2599618874 * ___m_text_6;
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01::count
	int32_t ___count_8;

public:
	inline static int32_t get_offset_of_ObjectType_4() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___ObjectType_4)); }
	inline int32_t get_ObjectType_4() const { return ___ObjectType_4; }
	inline int32_t* get_address_of_ObjectType_4() { return &___ObjectType_4; }
	inline void set_ObjectType_4(int32_t value)
	{
		___ObjectType_4 = value;
	}

	inline static int32_t get_offset_of_isStatic_5() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___isStatic_5)); }
	inline bool get_isStatic_5() const { return ___isStatic_5; }
	inline bool* get_address_of_isStatic_5() { return &___isStatic_5; }
	inline void set_isStatic_5(bool value)
	{
		___isStatic_5 = value;
	}

	inline static int32_t get_offset_of_m_text_6() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___m_text_6)); }
	inline TMP_Text_t2599618874 * get_m_text_6() const { return ___m_text_6; }
	inline TMP_Text_t2599618874 ** get_address_of_m_text_6() { return &___m_text_6; }
	inline void set_m_text_6(TMP_Text_t2599618874 * value)
	{
		___m_text_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_6), value);
	}

	inline static int32_t get_offset_of_count_8() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___count_8)); }
	inline int32_t get_count_8() const { return ___count_8; }
	inline int32_t* get_address_of_count_8() { return &___count_8; }
	inline void set_count_8(int32_t value)
	{
		___count_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_EXAMPLESCRIPT_01_T3051742005_H
#ifndef TMP_FRAMERATECOUNTER_T314972976_H
#define TMP_FRAMERATECOUNTER_T314972976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_FrameRateCounter
struct  TMP_FrameRateCounter_t314972976  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.TMP_FrameRateCounter::UpdateInterval
	float ___UpdateInterval_4;
	// System.Single TMPro.Examples.TMP_FrameRateCounter::m_LastInterval
	float ___m_LastInterval_5;
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter::m_Frames
	int32_t ___m_Frames_6;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::AnchorPosition
	int32_t ___AnchorPosition_7;
	// System.String TMPro.Examples.TMP_FrameRateCounter::htmlColorTag
	String_t* ___htmlColorTag_8;
	// TMPro.TextMeshPro TMPro.Examples.TMP_FrameRateCounter::m_TextMeshPro
	TextMeshPro_t2393593166 * ___m_TextMeshPro_10;
	// UnityEngine.Transform TMPro.Examples.TMP_FrameRateCounter::m_frameCounter_transform
	Transform_t3600365921 * ___m_frameCounter_transform_11;
	// UnityEngine.Camera TMPro.Examples.TMP_FrameRateCounter::m_camera
	Camera_t4157153871 * ___m_camera_12;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::last_AnchorPosition
	int32_t ___last_AnchorPosition_13;

public:
	inline static int32_t get_offset_of_UpdateInterval_4() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___UpdateInterval_4)); }
	inline float get_UpdateInterval_4() const { return ___UpdateInterval_4; }
	inline float* get_address_of_UpdateInterval_4() { return &___UpdateInterval_4; }
	inline void set_UpdateInterval_4(float value)
	{
		___UpdateInterval_4 = value;
	}

	inline static int32_t get_offset_of_m_LastInterval_5() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_LastInterval_5)); }
	inline float get_m_LastInterval_5() const { return ___m_LastInterval_5; }
	inline float* get_address_of_m_LastInterval_5() { return &___m_LastInterval_5; }
	inline void set_m_LastInterval_5(float value)
	{
		___m_LastInterval_5 = value;
	}

	inline static int32_t get_offset_of_m_Frames_6() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_Frames_6)); }
	inline int32_t get_m_Frames_6() const { return ___m_Frames_6; }
	inline int32_t* get_address_of_m_Frames_6() { return &___m_Frames_6; }
	inline void set_m_Frames_6(int32_t value)
	{
		___m_Frames_6 = value;
	}

	inline static int32_t get_offset_of_AnchorPosition_7() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___AnchorPosition_7)); }
	inline int32_t get_AnchorPosition_7() const { return ___AnchorPosition_7; }
	inline int32_t* get_address_of_AnchorPosition_7() { return &___AnchorPosition_7; }
	inline void set_AnchorPosition_7(int32_t value)
	{
		___AnchorPosition_7 = value;
	}

	inline static int32_t get_offset_of_htmlColorTag_8() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___htmlColorTag_8)); }
	inline String_t* get_htmlColorTag_8() const { return ___htmlColorTag_8; }
	inline String_t** get_address_of_htmlColorTag_8() { return &___htmlColorTag_8; }
	inline void set_htmlColorTag_8(String_t* value)
	{
		___htmlColorTag_8 = value;
		Il2CppCodeGenWriteBarrier((&___htmlColorTag_8), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_10() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_TextMeshPro_10)); }
	inline TextMeshPro_t2393593166 * get_m_TextMeshPro_10() const { return ___m_TextMeshPro_10; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_TextMeshPro_10() { return &___m_TextMeshPro_10; }
	inline void set_m_TextMeshPro_10(TextMeshPro_t2393593166 * value)
	{
		___m_TextMeshPro_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_10), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_11() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_frameCounter_transform_11)); }
	inline Transform_t3600365921 * get_m_frameCounter_transform_11() const { return ___m_frameCounter_transform_11; }
	inline Transform_t3600365921 ** get_address_of_m_frameCounter_transform_11() { return &___m_frameCounter_transform_11; }
	inline void set_m_frameCounter_transform_11(Transform_t3600365921 * value)
	{
		___m_frameCounter_transform_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_11), value);
	}

	inline static int32_t get_offset_of_m_camera_12() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_camera_12)); }
	inline Camera_t4157153871 * get_m_camera_12() const { return ___m_camera_12; }
	inline Camera_t4157153871 ** get_address_of_m_camera_12() { return &___m_camera_12; }
	inline void set_m_camera_12(Camera_t4157153871 * value)
	{
		___m_camera_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_12), value);
	}

	inline static int32_t get_offset_of_last_AnchorPosition_13() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___last_AnchorPosition_13)); }
	inline int32_t get_last_AnchorPosition_13() const { return ___last_AnchorPosition_13; }
	inline int32_t* get_address_of_last_AnchorPosition_13() { return &___last_AnchorPosition_13; }
	inline void set_last_AnchorPosition_13(int32_t value)
	{
		___last_AnchorPosition_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FRAMERATECOUNTER_T314972976_H
#ifndef TMP_TEXTEVENTCHECK_T1103849140_H
#define TMP_TEXTEVENTCHECK_T1103849140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextEventCheck
struct  TMP_TextEventCheck_t1103849140  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_TextEventHandler TMPro.Examples.TMP_TextEventCheck::TextEventHandler
	TMP_TextEventHandler_t1869054637 * ___TextEventHandler_4;

public:
	inline static int32_t get_offset_of_TextEventHandler_4() { return static_cast<int32_t>(offsetof(TMP_TextEventCheck_t1103849140, ___TextEventHandler_4)); }
	inline TMP_TextEventHandler_t1869054637 * get_TextEventHandler_4() const { return ___TextEventHandler_4; }
	inline TMP_TextEventHandler_t1869054637 ** get_address_of_TextEventHandler_4() { return &___TextEventHandler_4; }
	inline void set_TextEventHandler_4(TMP_TextEventHandler_t1869054637 * value)
	{
		___TextEventHandler_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextEventHandler_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTEVENTCHECK_T1103849140_H
#ifndef TMP_TEXTINFODEBUGTOOL_T1868681444_H
#define TMP_TEXTINFODEBUGTOOL_T1868681444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextInfoDebugTool
struct  TMP_TextInfoDebugTool_t1868681444  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowCharacters
	bool ___ShowCharacters_4;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowWords
	bool ___ShowWords_5;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowLinks
	bool ___ShowLinks_6;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowLines
	bool ___ShowLines_7;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowMeshBounds
	bool ___ShowMeshBounds_8;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowTextBounds
	bool ___ShowTextBounds_9;
	// System.String TMPro.Examples.TMP_TextInfoDebugTool::ObjectStats
	String_t* ___ObjectStats_10;
	// TMPro.TMP_Text TMPro.Examples.TMP_TextInfoDebugTool::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_11;
	// UnityEngine.Transform TMPro.Examples.TMP_TextInfoDebugTool::m_Transform
	Transform_t3600365921 * ___m_Transform_12;

public:
	inline static int32_t get_offset_of_ShowCharacters_4() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowCharacters_4)); }
	inline bool get_ShowCharacters_4() const { return ___ShowCharacters_4; }
	inline bool* get_address_of_ShowCharacters_4() { return &___ShowCharacters_4; }
	inline void set_ShowCharacters_4(bool value)
	{
		___ShowCharacters_4 = value;
	}

	inline static int32_t get_offset_of_ShowWords_5() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowWords_5)); }
	inline bool get_ShowWords_5() const { return ___ShowWords_5; }
	inline bool* get_address_of_ShowWords_5() { return &___ShowWords_5; }
	inline void set_ShowWords_5(bool value)
	{
		___ShowWords_5 = value;
	}

	inline static int32_t get_offset_of_ShowLinks_6() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowLinks_6)); }
	inline bool get_ShowLinks_6() const { return ___ShowLinks_6; }
	inline bool* get_address_of_ShowLinks_6() { return &___ShowLinks_6; }
	inline void set_ShowLinks_6(bool value)
	{
		___ShowLinks_6 = value;
	}

	inline static int32_t get_offset_of_ShowLines_7() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowLines_7)); }
	inline bool get_ShowLines_7() const { return ___ShowLines_7; }
	inline bool* get_address_of_ShowLines_7() { return &___ShowLines_7; }
	inline void set_ShowLines_7(bool value)
	{
		___ShowLines_7 = value;
	}

	inline static int32_t get_offset_of_ShowMeshBounds_8() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowMeshBounds_8)); }
	inline bool get_ShowMeshBounds_8() const { return ___ShowMeshBounds_8; }
	inline bool* get_address_of_ShowMeshBounds_8() { return &___ShowMeshBounds_8; }
	inline void set_ShowMeshBounds_8(bool value)
	{
		___ShowMeshBounds_8 = value;
	}

	inline static int32_t get_offset_of_ShowTextBounds_9() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowTextBounds_9)); }
	inline bool get_ShowTextBounds_9() const { return ___ShowTextBounds_9; }
	inline bool* get_address_of_ShowTextBounds_9() { return &___ShowTextBounds_9; }
	inline void set_ShowTextBounds_9(bool value)
	{
		___ShowTextBounds_9 = value;
	}

	inline static int32_t get_offset_of_ObjectStats_10() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ObjectStats_10)); }
	inline String_t* get_ObjectStats_10() const { return ___ObjectStats_10; }
	inline String_t** get_address_of_ObjectStats_10() { return &___ObjectStats_10; }
	inline void set_ObjectStats_10(String_t* value)
	{
		___ObjectStats_10 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectStats_10), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_11() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___m_TextComponent_11)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_11() const { return ___m_TextComponent_11; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_11() { return &___m_TextComponent_11; }
	inline void set_m_TextComponent_11(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_11), value);
	}

	inline static int32_t get_offset_of_m_Transform_12() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___m_Transform_12)); }
	inline Transform_t3600365921 * get_m_Transform_12() const { return ___m_Transform_12; }
	inline Transform_t3600365921 ** get_address_of_m_Transform_12() { return &___m_Transform_12; }
	inline void set_m_Transform_12(Transform_t3600365921 * value)
	{
		___m_Transform_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transform_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTINFODEBUGTOOL_T1868681444_H
#ifndef TMP_TEXTSELECTOR_A_T3982526506_H
#define TMP_TEXTSELECTOR_A_T3982526506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextSelector_A
struct  TMP_TextSelector_A_t3982526506  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TextMeshPro TMPro.Examples.TMP_TextSelector_A::m_TextMeshPro
	TextMeshPro_t2393593166 * ___m_TextMeshPro_4;
	// UnityEngine.Camera TMPro.Examples.TMP_TextSelector_A::m_Camera
	Camera_t4157153871 * ___m_Camera_5;
	// System.Boolean TMPro.Examples.TMP_TextSelector_A::m_isHoveringObject
	bool ___m_isHoveringObject_6;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_selectedLink
	int32_t ___m_selectedLink_7;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_lastCharIndex
	int32_t ___m_lastCharIndex_8;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_lastWordIndex
	int32_t ___m_lastWordIndex_9;

public:
	inline static int32_t get_offset_of_m_TextMeshPro_4() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_TextMeshPro_4)); }
	inline TextMeshPro_t2393593166 * get_m_TextMeshPro_4() const { return ___m_TextMeshPro_4; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_TextMeshPro_4() { return &___m_TextMeshPro_4; }
	inline void set_m_TextMeshPro_4(TextMeshPro_t2393593166 * value)
	{
		___m_TextMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_4), value);
	}

	inline static int32_t get_offset_of_m_Camera_5() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_Camera_5)); }
	inline Camera_t4157153871 * get_m_Camera_5() const { return ___m_Camera_5; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_5() { return &___m_Camera_5; }
	inline void set_m_Camera_5(Camera_t4157153871 * value)
	{
		___m_Camera_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_5), value);
	}

	inline static int32_t get_offset_of_m_isHoveringObject_6() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_isHoveringObject_6)); }
	inline bool get_m_isHoveringObject_6() const { return ___m_isHoveringObject_6; }
	inline bool* get_address_of_m_isHoveringObject_6() { return &___m_isHoveringObject_6; }
	inline void set_m_isHoveringObject_6(bool value)
	{
		___m_isHoveringObject_6 = value;
	}

	inline static int32_t get_offset_of_m_selectedLink_7() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_selectedLink_7)); }
	inline int32_t get_m_selectedLink_7() const { return ___m_selectedLink_7; }
	inline int32_t* get_address_of_m_selectedLink_7() { return &___m_selectedLink_7; }
	inline void set_m_selectedLink_7(int32_t value)
	{
		___m_selectedLink_7 = value;
	}

	inline static int32_t get_offset_of_m_lastCharIndex_8() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_lastCharIndex_8)); }
	inline int32_t get_m_lastCharIndex_8() const { return ___m_lastCharIndex_8; }
	inline int32_t* get_address_of_m_lastCharIndex_8() { return &___m_lastCharIndex_8; }
	inline void set_m_lastCharIndex_8(int32_t value)
	{
		___m_lastCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_m_lastWordIndex_9() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_lastWordIndex_9)); }
	inline int32_t get_m_lastWordIndex_9() const { return ___m_lastWordIndex_9; }
	inline int32_t* get_address_of_m_lastWordIndex_9() { return &___m_lastWordIndex_9; }
	inline void set_m_lastWordIndex_9(int32_t value)
	{
		___m_lastWordIndex_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTSELECTOR_A_T3982526506_H
#ifndef TMP_TEXTSELECTOR_B_T3982526505_H
#define TMP_TEXTSELECTOR_B_T3982526505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextSelector_B
struct  TMP_TextSelector_B_t3982526505  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform TMPro.Examples.TMP_TextSelector_B::TextPopup_Prefab_01
	RectTransform_t3704657025 * ___TextPopup_Prefab_01_4;
	// UnityEngine.RectTransform TMPro.Examples.TMP_TextSelector_B::m_TextPopup_RectTransform
	RectTransform_t3704657025 * ___m_TextPopup_RectTransform_5;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_TextSelector_B::m_TextPopup_TMPComponent
	TextMeshProUGUI_t529313277 * ___m_TextPopup_TMPComponent_6;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_TextSelector_B::m_TextMeshPro
	TextMeshProUGUI_t529313277 * ___m_TextMeshPro_9;
	// UnityEngine.Canvas TMPro.Examples.TMP_TextSelector_B::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_10;
	// UnityEngine.Camera TMPro.Examples.TMP_TextSelector_B::m_Camera
	Camera_t4157153871 * ___m_Camera_11;
	// System.Boolean TMPro.Examples.TMP_TextSelector_B::isHoveringObject
	bool ___isHoveringObject_12;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_selectedWord
	int32_t ___m_selectedWord_13;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_selectedLink
	int32_t ___m_selectedLink_14;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_lastIndex
	int32_t ___m_lastIndex_15;
	// UnityEngine.Matrix4x4 TMPro.Examples.TMP_TextSelector_B::m_matrix
	Matrix4x4_t1817901843  ___m_matrix_16;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.TMP_TextSelector_B::m_cachedMeshInfoVertexData
	TMP_MeshInfoU5BU5D_t3365986247* ___m_cachedMeshInfoVertexData_17;

public:
	inline static int32_t get_offset_of_TextPopup_Prefab_01_4() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___TextPopup_Prefab_01_4)); }
	inline RectTransform_t3704657025 * get_TextPopup_Prefab_01_4() const { return ___TextPopup_Prefab_01_4; }
	inline RectTransform_t3704657025 ** get_address_of_TextPopup_Prefab_01_4() { return &___TextPopup_Prefab_01_4; }
	inline void set_TextPopup_Prefab_01_4(RectTransform_t3704657025 * value)
	{
		___TextPopup_Prefab_01_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextPopup_Prefab_01_4), value);
	}

	inline static int32_t get_offset_of_m_TextPopup_RectTransform_5() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_TextPopup_RectTransform_5)); }
	inline RectTransform_t3704657025 * get_m_TextPopup_RectTransform_5() const { return ___m_TextPopup_RectTransform_5; }
	inline RectTransform_t3704657025 ** get_address_of_m_TextPopup_RectTransform_5() { return &___m_TextPopup_RectTransform_5; }
	inline void set_m_TextPopup_RectTransform_5(RectTransform_t3704657025 * value)
	{
		___m_TextPopup_RectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextPopup_RectTransform_5), value);
	}

	inline static int32_t get_offset_of_m_TextPopup_TMPComponent_6() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_TextPopup_TMPComponent_6)); }
	inline TextMeshProUGUI_t529313277 * get_m_TextPopup_TMPComponent_6() const { return ___m_TextPopup_TMPComponent_6; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_TextPopup_TMPComponent_6() { return &___m_TextPopup_TMPComponent_6; }
	inline void set_m_TextPopup_TMPComponent_6(TextMeshProUGUI_t529313277 * value)
	{
		___m_TextPopup_TMPComponent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextPopup_TMPComponent_6), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_9() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_TextMeshPro_9)); }
	inline TextMeshProUGUI_t529313277 * get_m_TextMeshPro_9() const { return ___m_TextMeshPro_9; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_TextMeshPro_9() { return &___m_TextMeshPro_9; }
	inline void set_m_TextMeshPro_9(TextMeshProUGUI_t529313277 * value)
	{
		___m_TextMeshPro_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_9), value);
	}

	inline static int32_t get_offset_of_m_Canvas_10() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_Canvas_10)); }
	inline Canvas_t3310196443 * get_m_Canvas_10() const { return ___m_Canvas_10; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_10() { return &___m_Canvas_10; }
	inline void set_m_Canvas_10(Canvas_t3310196443 * value)
	{
		___m_Canvas_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_10), value);
	}

	inline static int32_t get_offset_of_m_Camera_11() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_Camera_11)); }
	inline Camera_t4157153871 * get_m_Camera_11() const { return ___m_Camera_11; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_11() { return &___m_Camera_11; }
	inline void set_m_Camera_11(Camera_t4157153871 * value)
	{
		___m_Camera_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_11), value);
	}

	inline static int32_t get_offset_of_isHoveringObject_12() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___isHoveringObject_12)); }
	inline bool get_isHoveringObject_12() const { return ___isHoveringObject_12; }
	inline bool* get_address_of_isHoveringObject_12() { return &___isHoveringObject_12; }
	inline void set_isHoveringObject_12(bool value)
	{
		___isHoveringObject_12 = value;
	}

	inline static int32_t get_offset_of_m_selectedWord_13() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_selectedWord_13)); }
	inline int32_t get_m_selectedWord_13() const { return ___m_selectedWord_13; }
	inline int32_t* get_address_of_m_selectedWord_13() { return &___m_selectedWord_13; }
	inline void set_m_selectedWord_13(int32_t value)
	{
		___m_selectedWord_13 = value;
	}

	inline static int32_t get_offset_of_m_selectedLink_14() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_selectedLink_14)); }
	inline int32_t get_m_selectedLink_14() const { return ___m_selectedLink_14; }
	inline int32_t* get_address_of_m_selectedLink_14() { return &___m_selectedLink_14; }
	inline void set_m_selectedLink_14(int32_t value)
	{
		___m_selectedLink_14 = value;
	}

	inline static int32_t get_offset_of_m_lastIndex_15() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_lastIndex_15)); }
	inline int32_t get_m_lastIndex_15() const { return ___m_lastIndex_15; }
	inline int32_t* get_address_of_m_lastIndex_15() { return &___m_lastIndex_15; }
	inline void set_m_lastIndex_15(int32_t value)
	{
		___m_lastIndex_15 = value;
	}

	inline static int32_t get_offset_of_m_matrix_16() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_matrix_16)); }
	inline Matrix4x4_t1817901843  get_m_matrix_16() const { return ___m_matrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_m_matrix_16() { return &___m_matrix_16; }
	inline void set_m_matrix_16(Matrix4x4_t1817901843  value)
	{
		___m_matrix_16 = value;
	}

	inline static int32_t get_offset_of_m_cachedMeshInfoVertexData_17() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_cachedMeshInfoVertexData_17)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_m_cachedMeshInfoVertexData_17() const { return ___m_cachedMeshInfoVertexData_17; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_m_cachedMeshInfoVertexData_17() { return &___m_cachedMeshInfoVertexData_17; }
	inline void set_m_cachedMeshInfoVertexData_17(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___m_cachedMeshInfoVertexData_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_cachedMeshInfoVertexData_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTSELECTOR_B_T3982526505_H
#ifndef TMP_UIFRAMERATECOUNTER_T811747397_H
#define TMP_UIFRAMERATECOUNTER_T811747397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_UiFrameRateCounter
struct  TMP_UiFrameRateCounter_t811747397  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.TMP_UiFrameRateCounter::UpdateInterval
	float ___UpdateInterval_4;
	// System.Single TMPro.Examples.TMP_UiFrameRateCounter::m_LastInterval
	float ___m_LastInterval_5;
	// System.Int32 TMPro.Examples.TMP_UiFrameRateCounter::m_Frames
	int32_t ___m_Frames_6;
	// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_UiFrameRateCounter::AnchorPosition
	int32_t ___AnchorPosition_7;
	// System.String TMPro.Examples.TMP_UiFrameRateCounter::htmlColorTag
	String_t* ___htmlColorTag_8;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_UiFrameRateCounter::m_TextMeshPro
	TextMeshProUGUI_t529313277 * ___m_TextMeshPro_10;
	// UnityEngine.RectTransform TMPro.Examples.TMP_UiFrameRateCounter::m_frameCounter_transform
	RectTransform_t3704657025 * ___m_frameCounter_transform_11;
	// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_UiFrameRateCounter::last_AnchorPosition
	int32_t ___last_AnchorPosition_12;

public:
	inline static int32_t get_offset_of_UpdateInterval_4() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___UpdateInterval_4)); }
	inline float get_UpdateInterval_4() const { return ___UpdateInterval_4; }
	inline float* get_address_of_UpdateInterval_4() { return &___UpdateInterval_4; }
	inline void set_UpdateInterval_4(float value)
	{
		___UpdateInterval_4 = value;
	}

	inline static int32_t get_offset_of_m_LastInterval_5() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_LastInterval_5)); }
	inline float get_m_LastInterval_5() const { return ___m_LastInterval_5; }
	inline float* get_address_of_m_LastInterval_5() { return &___m_LastInterval_5; }
	inline void set_m_LastInterval_5(float value)
	{
		___m_LastInterval_5 = value;
	}

	inline static int32_t get_offset_of_m_Frames_6() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_Frames_6)); }
	inline int32_t get_m_Frames_6() const { return ___m_Frames_6; }
	inline int32_t* get_address_of_m_Frames_6() { return &___m_Frames_6; }
	inline void set_m_Frames_6(int32_t value)
	{
		___m_Frames_6 = value;
	}

	inline static int32_t get_offset_of_AnchorPosition_7() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___AnchorPosition_7)); }
	inline int32_t get_AnchorPosition_7() const { return ___AnchorPosition_7; }
	inline int32_t* get_address_of_AnchorPosition_7() { return &___AnchorPosition_7; }
	inline void set_AnchorPosition_7(int32_t value)
	{
		___AnchorPosition_7 = value;
	}

	inline static int32_t get_offset_of_htmlColorTag_8() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___htmlColorTag_8)); }
	inline String_t* get_htmlColorTag_8() const { return ___htmlColorTag_8; }
	inline String_t** get_address_of_htmlColorTag_8() { return &___htmlColorTag_8; }
	inline void set_htmlColorTag_8(String_t* value)
	{
		___htmlColorTag_8 = value;
		Il2CppCodeGenWriteBarrier((&___htmlColorTag_8), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_10() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_TextMeshPro_10)); }
	inline TextMeshProUGUI_t529313277 * get_m_TextMeshPro_10() const { return ___m_TextMeshPro_10; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_TextMeshPro_10() { return &___m_TextMeshPro_10; }
	inline void set_m_TextMeshPro_10(TextMeshProUGUI_t529313277 * value)
	{
		___m_TextMeshPro_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_10), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_11() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_frameCounter_transform_11)); }
	inline RectTransform_t3704657025 * get_m_frameCounter_transform_11() const { return ___m_frameCounter_transform_11; }
	inline RectTransform_t3704657025 ** get_address_of_m_frameCounter_transform_11() { return &___m_frameCounter_transform_11; }
	inline void set_m_frameCounter_transform_11(RectTransform_t3704657025 * value)
	{
		___m_frameCounter_transform_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_11), value);
	}

	inline static int32_t get_offset_of_last_AnchorPosition_12() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___last_AnchorPosition_12)); }
	inline int32_t get_last_AnchorPosition_12() const { return ___last_AnchorPosition_12; }
	inline int32_t* get_address_of_last_AnchorPosition_12() { return &___last_AnchorPosition_12; }
	inline void set_last_AnchorPosition_12(int32_t value)
	{
		___last_AnchorPosition_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UIFRAMERATECOUNTER_T811747397_H
#ifndef TMPRO_INSTRUCTIONOVERLAY_T4246705477_H
#define TMPRO_INSTRUCTIONOVERLAY_T4246705477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMPro_InstructionOverlay
struct  TMPro_InstructionOverlay_t4246705477  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions TMPro.Examples.TMPro_InstructionOverlay::AnchorPosition
	int32_t ___AnchorPosition_4;
	// TMPro.TextMeshPro TMPro.Examples.TMPro_InstructionOverlay::m_TextMeshPro
	TextMeshPro_t2393593166 * ___m_TextMeshPro_6;
	// TMPro.TextContainer TMPro.Examples.TMPro_InstructionOverlay::m_textContainer
	TextContainer_t97923372 * ___m_textContainer_7;
	// UnityEngine.Transform TMPro.Examples.TMPro_InstructionOverlay::m_frameCounter_transform
	Transform_t3600365921 * ___m_frameCounter_transform_8;
	// UnityEngine.Camera TMPro.Examples.TMPro_InstructionOverlay::m_camera
	Camera_t4157153871 * ___m_camera_9;

public:
	inline static int32_t get_offset_of_AnchorPosition_4() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t4246705477, ___AnchorPosition_4)); }
	inline int32_t get_AnchorPosition_4() const { return ___AnchorPosition_4; }
	inline int32_t* get_address_of_AnchorPosition_4() { return &___AnchorPosition_4; }
	inline void set_AnchorPosition_4(int32_t value)
	{
		___AnchorPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_TextMeshPro_6() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t4246705477, ___m_TextMeshPro_6)); }
	inline TextMeshPro_t2393593166 * get_m_TextMeshPro_6() const { return ___m_TextMeshPro_6; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_TextMeshPro_6() { return &___m_TextMeshPro_6; }
	inline void set_m_TextMeshPro_6(TextMeshPro_t2393593166 * value)
	{
		___m_TextMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_6), value);
	}

	inline static int32_t get_offset_of_m_textContainer_7() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t4246705477, ___m_textContainer_7)); }
	inline TextContainer_t97923372 * get_m_textContainer_7() const { return ___m_textContainer_7; }
	inline TextContainer_t97923372 ** get_address_of_m_textContainer_7() { return &___m_textContainer_7; }
	inline void set_m_textContainer_7(TextContainer_t97923372 * value)
	{
		___m_textContainer_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainer_7), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_8() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t4246705477, ___m_frameCounter_transform_8)); }
	inline Transform_t3600365921 * get_m_frameCounter_transform_8() const { return ___m_frameCounter_transform_8; }
	inline Transform_t3600365921 ** get_address_of_m_frameCounter_transform_8() { return &___m_frameCounter_transform_8; }
	inline void set_m_frameCounter_transform_8(Transform_t3600365921 * value)
	{
		___m_frameCounter_transform_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_8), value);
	}

	inline static int32_t get_offset_of_m_camera_9() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t4246705477, ___m_camera_9)); }
	inline Camera_t4157153871 * get_m_camera_9() const { return ___m_camera_9; }
	inline Camera_t4157153871 ** get_address_of_m_camera_9() { return &___m_camera_9; }
	inline void set_m_camera_9(Camera_t4157153871 * value)
	{
		___m_camera_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_INSTRUCTIONOVERLAY_T4246705477_H
#ifndef TELETYPE_T2409835159_H
#define TELETYPE_T2409835159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TeleType
struct  TeleType_t2409835159  : public MonoBehaviour_t3962482529
{
public:
	// System.String TMPro.Examples.TeleType::label01
	String_t* ___label01_4;
	// System.String TMPro.Examples.TeleType::label02
	String_t* ___label02_5;
	// TMPro.TMP_Text TMPro.Examples.TeleType::m_textMeshPro
	TMP_Text_t2599618874 * ___m_textMeshPro_6;

public:
	inline static int32_t get_offset_of_label01_4() { return static_cast<int32_t>(offsetof(TeleType_t2409835159, ___label01_4)); }
	inline String_t* get_label01_4() const { return ___label01_4; }
	inline String_t** get_address_of_label01_4() { return &___label01_4; }
	inline void set_label01_4(String_t* value)
	{
		___label01_4 = value;
		Il2CppCodeGenWriteBarrier((&___label01_4), value);
	}

	inline static int32_t get_offset_of_label02_5() { return static_cast<int32_t>(offsetof(TeleType_t2409835159, ___label02_5)); }
	inline String_t* get_label02_5() const { return ___label02_5; }
	inline String_t** get_address_of_label02_5() { return &___label02_5; }
	inline void set_label02_5(String_t* value)
	{
		___label02_5 = value;
		Il2CppCodeGenWriteBarrier((&___label02_5), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_6() { return static_cast<int32_t>(offsetof(TeleType_t2409835159, ___m_textMeshPro_6)); }
	inline TMP_Text_t2599618874 * get_m_textMeshPro_6() const { return ___m_textMeshPro_6; }
	inline TMP_Text_t2599618874 ** get_address_of_m_textMeshPro_6() { return &___m_textMeshPro_6; }
	inline void set_m_textMeshPro_6(TMP_Text_t2599618874 * value)
	{
		___m_textMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELETYPE_T2409835159_H
#ifndef TEXTCONSOLESIMULATOR_T3766250034_H
#define TEXTCONSOLESIMULATOR_T3766250034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator
struct  TextConsoleSimulator_t3766250034  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_4;
	// System.Boolean TMPro.Examples.TextConsoleSimulator::hasTextChanged
	bool ___hasTextChanged_5;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t3766250034, ___m_TextComponent_4)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_4), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_5() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t3766250034, ___hasTextChanged_5)); }
	inline bool get_hasTextChanged_5() const { return ___hasTextChanged_5; }
	inline bool* get_address_of_hasTextChanged_5() { return &___hasTextChanged_5; }
	inline void set_hasTextChanged_5(bool value)
	{
		___hasTextChanged_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONSOLESIMULATOR_T3766250034_H
#ifndef TEXTMESHPROFLOATINGTEXT_T845872552_H
#define TEXTMESHPROFLOATINGTEXT_T845872552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText
struct  TextMeshProFloatingText_t845872552  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Font TMPro.Examples.TextMeshProFloatingText::TheFont
	Font_t1956802104 * ___TheFont_4;
	// UnityEngine.GameObject TMPro.Examples.TextMeshProFloatingText::m_floatingText
	GameObject_t1113636619 * ___m_floatingText_5;
	// TMPro.TextMeshPro TMPro.Examples.TextMeshProFloatingText::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_6;
	// UnityEngine.TextMesh TMPro.Examples.TextMeshProFloatingText::m_textMesh
	TextMesh_t1536577757 * ___m_textMesh_7;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_transform
	Transform_t3600365921 * ___m_transform_8;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_floatingText_Transform
	Transform_t3600365921 * ___m_floatingText_Transform_9;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_cameraTransform
	Transform_t3600365921 * ___m_cameraTransform_10;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText::lastPOS
	Vector3_t3722313464  ___lastPOS_11;
	// UnityEngine.Quaternion TMPro.Examples.TextMeshProFloatingText::lastRotation
	Quaternion_t2301928331  ___lastRotation_12;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText::SpawnType
	int32_t ___SpawnType_13;

public:
	inline static int32_t get_offset_of_TheFont_4() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___TheFont_4)); }
	inline Font_t1956802104 * get_TheFont_4() const { return ___TheFont_4; }
	inline Font_t1956802104 ** get_address_of_TheFont_4() { return &___TheFont_4; }
	inline void set_TheFont_4(Font_t1956802104 * value)
	{
		___TheFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_4), value);
	}

	inline static int32_t get_offset_of_m_floatingText_5() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_floatingText_5)); }
	inline GameObject_t1113636619 * get_m_floatingText_5() const { return ___m_floatingText_5; }
	inline GameObject_t1113636619 ** get_address_of_m_floatingText_5() { return &___m_floatingText_5; }
	inline void set_m_floatingText_5(GameObject_t1113636619 * value)
	{
		___m_floatingText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_floatingText_5), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_6() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_textMeshPro_6)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_6() const { return ___m_textMeshPro_6; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_6() { return &___m_textMeshPro_6; }
	inline void set_m_textMeshPro_6(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_textMesh_7)); }
	inline TextMesh_t1536577757 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline TextMesh_t1536577757 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(TextMesh_t1536577757 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_transform_8() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_transform_8)); }
	inline Transform_t3600365921 * get_m_transform_8() const { return ___m_transform_8; }
	inline Transform_t3600365921 ** get_address_of_m_transform_8() { return &___m_transform_8; }
	inline void set_m_transform_8(Transform_t3600365921 * value)
	{
		___m_transform_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_8), value);
	}

	inline static int32_t get_offset_of_m_floatingText_Transform_9() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_floatingText_Transform_9)); }
	inline Transform_t3600365921 * get_m_floatingText_Transform_9() const { return ___m_floatingText_Transform_9; }
	inline Transform_t3600365921 ** get_address_of_m_floatingText_Transform_9() { return &___m_floatingText_Transform_9; }
	inline void set_m_floatingText_Transform_9(Transform_t3600365921 * value)
	{
		___m_floatingText_Transform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_floatingText_Transform_9), value);
	}

	inline static int32_t get_offset_of_m_cameraTransform_10() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_cameraTransform_10)); }
	inline Transform_t3600365921 * get_m_cameraTransform_10() const { return ___m_cameraTransform_10; }
	inline Transform_t3600365921 ** get_address_of_m_cameraTransform_10() { return &___m_cameraTransform_10; }
	inline void set_m_cameraTransform_10(Transform_t3600365921 * value)
	{
		___m_cameraTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_cameraTransform_10), value);
	}

	inline static int32_t get_offset_of_lastPOS_11() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___lastPOS_11)); }
	inline Vector3_t3722313464  get_lastPOS_11() const { return ___lastPOS_11; }
	inline Vector3_t3722313464 * get_address_of_lastPOS_11() { return &___lastPOS_11; }
	inline void set_lastPOS_11(Vector3_t3722313464  value)
	{
		___lastPOS_11 = value;
	}

	inline static int32_t get_offset_of_lastRotation_12() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___lastRotation_12)); }
	inline Quaternion_t2301928331  get_lastRotation_12() const { return ___lastRotation_12; }
	inline Quaternion_t2301928331 * get_address_of_lastRotation_12() { return &___lastRotation_12; }
	inline void set_lastRotation_12(Quaternion_t2301928331  value)
	{
		___lastRotation_12 = value;
	}

	inline static int32_t get_offset_of_SpawnType_13() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___SpawnType_13)); }
	inline int32_t get_SpawnType_13() const { return ___SpawnType_13; }
	inline int32_t* get_address_of_SpawnType_13() { return &___SpawnType_13; }
	inline void set_SpawnType_13(int32_t value)
	{
		___SpawnType_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPROFLOATINGTEXT_T845872552_H
#ifndef TEXTMESHSPAWNER_T177691618_H
#define TEXTMESHSPAWNER_T177691618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshSpawner
struct  TextMeshSpawner_t177691618  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.TextMeshSpawner::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.TextMeshSpawner::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// UnityEngine.Font TMPro.Examples.TextMeshSpawner::TheFont
	Font_t1956802104 * ___TheFont_6;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshSpawner::floatingText_Script
	TextMeshProFloatingText_t845872552 * ___floatingText_Script_7;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_TheFont_6() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___TheFont_6)); }
	inline Font_t1956802104 * get_TheFont_6() const { return ___TheFont_6; }
	inline Font_t1956802104 ** get_address_of_TheFont_6() { return &___TheFont_6; }
	inline void set_TheFont_6(Font_t1956802104 * value)
	{
		___TheFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_6), value);
	}

	inline static int32_t get_offset_of_floatingText_Script_7() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___floatingText_Script_7)); }
	inline TextMeshProFloatingText_t845872552 * get_floatingText_Script_7() const { return ___floatingText_Script_7; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_floatingText_Script_7() { return &___floatingText_Script_7; }
	inline void set_floatingText_Script_7(TextMeshProFloatingText_t845872552 * value)
	{
		___floatingText_Script_7 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHSPAWNER_T177691618_H
#ifndef VERTEXCOLORCYCLER_T3003193665_H
#define VERTEXCOLORCYCLER_T3003193665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexColorCycler
struct  VertexColorCycler_t3003193665  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.Examples.VertexColorCycler::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_4;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(VertexColorCycler_t3003193665, ___m_TextComponent_4)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXCOLORCYCLER_T3003193665_H
#ifndef VERTEXJITTER_T4087429332_H
#define VERTEXJITTER_T4087429332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexJitter
struct  VertexJitter_t4087429332  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.VertexJitter::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexJitter::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexJitter::CurveScale
	float ___CurveScale_6;
	// TMPro.TMP_Text TMPro.Examples.VertexJitter::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_7;
	// System.Boolean TMPro.Examples.VertexJitter::hasTextChanged
	bool ___hasTextChanged_8;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexJitter_t4087429332, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexJitter_t4087429332, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(VertexJitter_t4087429332, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_7() { return static_cast<int32_t>(offsetof(VertexJitter_t4087429332, ___m_TextComponent_7)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_7() const { return ___m_TextComponent_7; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_7() { return &___m_TextComponent_7; }
	inline void set_m_TextComponent_7(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_7), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_8() { return static_cast<int32_t>(offsetof(VertexJitter_t4087429332, ___hasTextChanged_8)); }
	inline bool get_hasTextChanged_8() const { return ___hasTextChanged_8; }
	inline bool* get_address_of_hasTextChanged_8() { return &___hasTextChanged_8; }
	inline void set_hasTextChanged_8(bool value)
	{
		___hasTextChanged_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXJITTER_T4087429332_H
#ifndef VERTEXSHAKEA_T4262048139_H
#define VERTEXSHAKEA_T4262048139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeA
struct  VertexShakeA_t4262048139  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.VertexShakeA::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexShakeA::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexShakeA::ScaleMultiplier
	float ___ScaleMultiplier_6;
	// System.Single TMPro.Examples.VertexShakeA::RotationMultiplier
	float ___RotationMultiplier_7;
	// TMPro.TMP_Text TMPro.Examples.VertexShakeA::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_8;
	// System.Boolean TMPro.Examples.VertexShakeA::hasTextChanged
	bool ___hasTextChanged_9;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexShakeA_t4262048139, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexShakeA_t4262048139, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_ScaleMultiplier_6() { return static_cast<int32_t>(offsetof(VertexShakeA_t4262048139, ___ScaleMultiplier_6)); }
	inline float get_ScaleMultiplier_6() const { return ___ScaleMultiplier_6; }
	inline float* get_address_of_ScaleMultiplier_6() { return &___ScaleMultiplier_6; }
	inline void set_ScaleMultiplier_6(float value)
	{
		___ScaleMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_RotationMultiplier_7() { return static_cast<int32_t>(offsetof(VertexShakeA_t4262048139, ___RotationMultiplier_7)); }
	inline float get_RotationMultiplier_7() const { return ___RotationMultiplier_7; }
	inline float* get_address_of_RotationMultiplier_7() { return &___RotationMultiplier_7; }
	inline void set_RotationMultiplier_7(float value)
	{
		___RotationMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_8() { return static_cast<int32_t>(offsetof(VertexShakeA_t4262048139, ___m_TextComponent_8)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_8() const { return ___m_TextComponent_8; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_8() { return &___m_TextComponent_8; }
	inline void set_m_TextComponent_8(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_8), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_9() { return static_cast<int32_t>(offsetof(VertexShakeA_t4262048139, ___hasTextChanged_9)); }
	inline bool get_hasTextChanged_9() const { return ___hasTextChanged_9; }
	inline bool* get_address_of_hasTextChanged_9() { return &___hasTextChanged_9; }
	inline void set_hasTextChanged_9(bool value)
	{
		___hasTextChanged_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSHAKEA_T4262048139_H
#ifndef VERTEXSHAKEB_T1533164784_H
#define VERTEXSHAKEB_T1533164784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeB
struct  VertexShakeB_t1533164784  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.VertexShakeB::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexShakeB::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexShakeB::CurveScale
	float ___CurveScale_6;
	// TMPro.TMP_Text TMPro.Examples.VertexShakeB::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_7;
	// System.Boolean TMPro.Examples.VertexShakeB::hasTextChanged
	bool ___hasTextChanged_8;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexShakeB_t1533164784, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexShakeB_t1533164784, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(VertexShakeB_t1533164784, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_7() { return static_cast<int32_t>(offsetof(VertexShakeB_t1533164784, ___m_TextComponent_7)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_7() const { return ___m_TextComponent_7; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_7() { return &___m_TextComponent_7; }
	inline void set_m_TextComponent_7(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_7), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_8() { return static_cast<int32_t>(offsetof(VertexShakeB_t1533164784, ___hasTextChanged_8)); }
	inline bool get_hasTextChanged_8() const { return ___hasTextChanged_8; }
	inline bool* get_address_of_hasTextChanged_8() { return &___hasTextChanged_8; }
	inline void set_hasTextChanged_8(bool value)
	{
		___hasTextChanged_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSHAKEB_T1533164784_H
#ifndef VERTEXZOOM_T550798657_H
#define VERTEXZOOM_T550798657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexZoom
struct  VertexZoom_t550798657  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.VertexZoom::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexZoom::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexZoom::CurveScale
	float ___CurveScale_6;
	// TMPro.TMP_Text TMPro.Examples.VertexZoom::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_7;
	// System.Boolean TMPro.Examples.VertexZoom::hasTextChanged
	bool ___hasTextChanged_8;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexZoom_t550798657, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexZoom_t550798657, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(VertexZoom_t550798657, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_7() { return static_cast<int32_t>(offsetof(VertexZoom_t550798657, ___m_TextComponent_7)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_7() const { return ___m_TextComponent_7; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_7() { return &___m_TextComponent_7; }
	inline void set_m_TextComponent_7(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_7), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_8() { return static_cast<int32_t>(offsetof(VertexZoom_t550798657, ___hasTextChanged_8)); }
	inline bool get_hasTextChanged_8() const { return ___hasTextChanged_8; }
	inline bool* get_address_of_hasTextChanged_8() { return &___hasTextChanged_8; }
	inline void set_hasTextChanged_8(bool value)
	{
		___hasTextChanged_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXZOOM_T550798657_H
#ifndef WARPTEXTEXAMPLE_T3821118074_H
#define WARPTEXTEXAMPLE_T3821118074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.WarpTextExample
struct  WarpTextExample_t3821118074  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.Examples.WarpTextExample::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_4;
	// UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::VertexCurve
	AnimationCurve_t3046754366 * ___VertexCurve_5;
	// System.Single TMPro.Examples.WarpTextExample::AngleMultiplier
	float ___AngleMultiplier_6;
	// System.Single TMPro.Examples.WarpTextExample::SpeedMultiplier
	float ___SpeedMultiplier_7;
	// System.Single TMPro.Examples.WarpTextExample::CurveScale
	float ___CurveScale_8;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(WarpTextExample_t3821118074, ___m_TextComponent_4)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_4), value);
	}

	inline static int32_t get_offset_of_VertexCurve_5() { return static_cast<int32_t>(offsetof(WarpTextExample_t3821118074, ___VertexCurve_5)); }
	inline AnimationCurve_t3046754366 * get_VertexCurve_5() const { return ___VertexCurve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_VertexCurve_5() { return &___VertexCurve_5; }
	inline void set_VertexCurve_5(AnimationCurve_t3046754366 * value)
	{
		___VertexCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___VertexCurve_5), value);
	}

	inline static int32_t get_offset_of_AngleMultiplier_6() { return static_cast<int32_t>(offsetof(WarpTextExample_t3821118074, ___AngleMultiplier_6)); }
	inline float get_AngleMultiplier_6() const { return ___AngleMultiplier_6; }
	inline float* get_address_of_AngleMultiplier_6() { return &___AngleMultiplier_6; }
	inline void set_AngleMultiplier_6(float value)
	{
		___AngleMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_7() { return static_cast<int32_t>(offsetof(WarpTextExample_t3821118074, ___SpeedMultiplier_7)); }
	inline float get_SpeedMultiplier_7() const { return ___SpeedMultiplier_7; }
	inline float* get_address_of_SpeedMultiplier_7() { return &___SpeedMultiplier_7; }
	inline void set_SpeedMultiplier_7(float value)
	{
		___SpeedMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_CurveScale_8() { return static_cast<int32_t>(offsetof(WarpTextExample_t3821118074, ___CurveScale_8)); }
	inline float get_CurveScale_8() const { return ___CurveScale_8; }
	inline float* get_address_of_CurveScale_8() { return &___CurveScale_8; }
	inline void set_CurveScale_8(float value)
	{
		___CurveScale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARPTEXTEXAMPLE_T3821118074_H
#ifndef TMP_TEXTEVENTHANDLER_T1869054637_H
#define TMP_TEXTEVENTHANDLER_T1869054637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler
struct  TMP_TextEventHandler_t1869054637  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::m_OnCharacterSelection
	CharacterSelectionEvent_t3109943174 * ___m_OnCharacterSelection_4;
	// TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::m_OnSpriteSelection
	SpriteSelectionEvent_t2798445241 * ___m_OnSpriteSelection_5;
	// TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::m_OnWordSelection
	WordSelectionEvent_t1841909953 * ___m_OnWordSelection_6;
	// TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::m_OnLineSelection
	LineSelectionEvent_t2868010532 * ___m_OnLineSelection_7;
	// TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::m_OnLinkSelection
	LinkSelectionEvent_t1590929858 * ___m_OnLinkSelection_8;
	// TMPro.TMP_Text TMPro.TMP_TextEventHandler::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_9;
	// UnityEngine.Camera TMPro.TMP_TextEventHandler::m_Camera
	Camera_t4157153871 * ___m_Camera_10;
	// UnityEngine.Canvas TMPro.TMP_TextEventHandler::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// System.Int32 TMPro.TMP_TextEventHandler::m_selectedLink
	int32_t ___m_selectedLink_12;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastCharIndex
	int32_t ___m_lastCharIndex_13;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastWordIndex
	int32_t ___m_lastWordIndex_14;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastLineIndex
	int32_t ___m_lastLineIndex_15;

public:
	inline static int32_t get_offset_of_m_OnCharacterSelection_4() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnCharacterSelection_4)); }
	inline CharacterSelectionEvent_t3109943174 * get_m_OnCharacterSelection_4() const { return ___m_OnCharacterSelection_4; }
	inline CharacterSelectionEvent_t3109943174 ** get_address_of_m_OnCharacterSelection_4() { return &___m_OnCharacterSelection_4; }
	inline void set_m_OnCharacterSelection_4(CharacterSelectionEvent_t3109943174 * value)
	{
		___m_OnCharacterSelection_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCharacterSelection_4), value);
	}

	inline static int32_t get_offset_of_m_OnSpriteSelection_5() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnSpriteSelection_5)); }
	inline SpriteSelectionEvent_t2798445241 * get_m_OnSpriteSelection_5() const { return ___m_OnSpriteSelection_5; }
	inline SpriteSelectionEvent_t2798445241 ** get_address_of_m_OnSpriteSelection_5() { return &___m_OnSpriteSelection_5; }
	inline void set_m_OnSpriteSelection_5(SpriteSelectionEvent_t2798445241 * value)
	{
		___m_OnSpriteSelection_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnSpriteSelection_5), value);
	}

	inline static int32_t get_offset_of_m_OnWordSelection_6() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnWordSelection_6)); }
	inline WordSelectionEvent_t1841909953 * get_m_OnWordSelection_6() const { return ___m_OnWordSelection_6; }
	inline WordSelectionEvent_t1841909953 ** get_address_of_m_OnWordSelection_6() { return &___m_OnWordSelection_6; }
	inline void set_m_OnWordSelection_6(WordSelectionEvent_t1841909953 * value)
	{
		___m_OnWordSelection_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnWordSelection_6), value);
	}

	inline static int32_t get_offset_of_m_OnLineSelection_7() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnLineSelection_7)); }
	inline LineSelectionEvent_t2868010532 * get_m_OnLineSelection_7() const { return ___m_OnLineSelection_7; }
	inline LineSelectionEvent_t2868010532 ** get_address_of_m_OnLineSelection_7() { return &___m_OnLineSelection_7; }
	inline void set_m_OnLineSelection_7(LineSelectionEvent_t2868010532 * value)
	{
		___m_OnLineSelection_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnLineSelection_7), value);
	}

	inline static int32_t get_offset_of_m_OnLinkSelection_8() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnLinkSelection_8)); }
	inline LinkSelectionEvent_t1590929858 * get_m_OnLinkSelection_8() const { return ___m_OnLinkSelection_8; }
	inline LinkSelectionEvent_t1590929858 ** get_address_of_m_OnLinkSelection_8() { return &___m_OnLinkSelection_8; }
	inline void set_m_OnLinkSelection_8(LinkSelectionEvent_t1590929858 * value)
	{
		___m_OnLinkSelection_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnLinkSelection_8), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_9() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_TextComponent_9)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_9() const { return ___m_TextComponent_9; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_9() { return &___m_TextComponent_9; }
	inline void set_m_TextComponent_9(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_9), value);
	}

	inline static int32_t get_offset_of_m_Camera_10() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_Camera_10)); }
	inline Camera_t4157153871 * get_m_Camera_10() const { return ___m_Camera_10; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_10() { return &___m_Camera_10; }
	inline void set_m_Camera_10(Camera_t4157153871 * value)
	{
		___m_Camera_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_selectedLink_12() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_selectedLink_12)); }
	inline int32_t get_m_selectedLink_12() const { return ___m_selectedLink_12; }
	inline int32_t* get_address_of_m_selectedLink_12() { return &___m_selectedLink_12; }
	inline void set_m_selectedLink_12(int32_t value)
	{
		___m_selectedLink_12 = value;
	}

	inline static int32_t get_offset_of_m_lastCharIndex_13() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_lastCharIndex_13)); }
	inline int32_t get_m_lastCharIndex_13() const { return ___m_lastCharIndex_13; }
	inline int32_t* get_address_of_m_lastCharIndex_13() { return &___m_lastCharIndex_13; }
	inline void set_m_lastCharIndex_13(int32_t value)
	{
		___m_lastCharIndex_13 = value;
	}

	inline static int32_t get_offset_of_m_lastWordIndex_14() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_lastWordIndex_14)); }
	inline int32_t get_m_lastWordIndex_14() const { return ___m_lastWordIndex_14; }
	inline int32_t* get_address_of_m_lastWordIndex_14() { return &___m_lastWordIndex_14; }
	inline void set_m_lastWordIndex_14(int32_t value)
	{
		___m_lastWordIndex_14 = value;
	}

	inline static int32_t get_offset_of_m_lastLineIndex_15() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_lastLineIndex_15)); }
	inline int32_t get_m_lastLineIndex_15() const { return ___m_lastLineIndex_15; }
	inline int32_t* get_address_of_m_lastLineIndex_15() { return &___m_lastLineIndex_15; }
	inline void set_m_lastLineIndex_15(int32_t value)
	{
		___m_lastLineIndex_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTEVENTHANDLER_T1869054637_H
#ifndef ANALYTICSTRACKER_T731021378_H
#define ANALYTICSTRACKER_T731021378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker
struct  AnalyticsTracker_t731021378  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityEngine.Analytics.AnalyticsTracker::m_EventName
	String_t* ___m_EventName_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsTracker::m_Dict
	Dictionary_2_t2865362463 * ___m_Dict_5;
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker::m_PrevDictHash
	int32_t ___m_PrevDictHash_6;
	// UnityEngine.Analytics.TrackableProperty UnityEngine.Analytics.AnalyticsTracker::m_TrackableProperty
	TrackableProperty_t3943537984 * ___m_TrackableProperty_7;
	// UnityEngine.Analytics.AnalyticsTracker/Trigger UnityEngine.Analytics.AnalyticsTracker::m_Trigger
	int32_t ___m_Trigger_8;

public:
	inline static int32_t get_offset_of_m_EventName_4() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_EventName_4)); }
	inline String_t* get_m_EventName_4() const { return ___m_EventName_4; }
	inline String_t** get_address_of_m_EventName_4() { return &___m_EventName_4; }
	inline void set_m_EventName_4(String_t* value)
	{
		___m_EventName_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_4), value);
	}

	inline static int32_t get_offset_of_m_Dict_5() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Dict_5)); }
	inline Dictionary_2_t2865362463 * get_m_Dict_5() const { return ___m_Dict_5; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_Dict_5() { return &___m_Dict_5; }
	inline void set_m_Dict_5(Dictionary_2_t2865362463 * value)
	{
		___m_Dict_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_5), value);
	}

	inline static int32_t get_offset_of_m_PrevDictHash_6() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_PrevDictHash_6)); }
	inline int32_t get_m_PrevDictHash_6() const { return ___m_PrevDictHash_6; }
	inline int32_t* get_address_of_m_PrevDictHash_6() { return &___m_PrevDictHash_6; }
	inline void set_m_PrevDictHash_6(int32_t value)
	{
		___m_PrevDictHash_6 = value;
	}

	inline static int32_t get_offset_of_m_TrackableProperty_7() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_TrackableProperty_7)); }
	inline TrackableProperty_t3943537984 * get_m_TrackableProperty_7() const { return ___m_TrackableProperty_7; }
	inline TrackableProperty_t3943537984 ** get_address_of_m_TrackableProperty_7() { return &___m_TrackableProperty_7; }
	inline void set_m_TrackableProperty_7(TrackableProperty_t3943537984 * value)
	{
		___m_TrackableProperty_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableProperty_7), value);
	}

	inline static int32_t get_offset_of_m_Trigger_8() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Trigger_8)); }
	inline int32_t get_m_Trigger_8() const { return ___m_Trigger_8; }
	inline int32_t* get_address_of_m_Trigger_8() { return &___m_Trigger_8; }
	inline void set_m_Trigger_8(int32_t value)
	{
		___m_Trigger_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSTRACKER_T731021378_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (TrackablePropertyBase_t2121532948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[2] = 
{
	TrackablePropertyBase_t2121532948::get_offset_of_m_Target_0(),
	TrackablePropertyBase_t2121532948::get_offset_of_m_Path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (ValueProperty_t1868393739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[11] = 
{
	ValueProperty_t1868393739::get_offset_of_m_EditingCustomValue_0(),
	ValueProperty_t1868393739::get_offset_of_m_PopupIndex_1(),
	ValueProperty_t1868393739::get_offset_of_m_CustomValue_2(),
	ValueProperty_t1868393739::get_offset_of_m_FixedType_3(),
	ValueProperty_t1868393739::get_offset_of_m_EnumType_4(),
	ValueProperty_t1868393739::get_offset_of_m_EnumTypeIsCustomizable_5(),
	ValueProperty_t1868393739::get_offset_of_m_CanDisable_6(),
	ValueProperty_t1868393739::get_offset_of_m_PropertyType_7(),
	ValueProperty_t1868393739::get_offset_of_m_ValueType_8(),
	ValueProperty_t1868393739::get_offset_of_m_Value_9(),
	ValueProperty_t1868393739::get_offset_of_m_Target_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (PropertyType_t4040930247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2202[4] = 
{
	PropertyType_t4040930247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (AnalyticsTracker_t731021378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[5] = 
{
	AnalyticsTracker_t731021378::get_offset_of_m_EventName_4(),
	AnalyticsTracker_t731021378::get_offset_of_m_Dict_5(),
	AnalyticsTracker_t731021378::get_offset_of_m_PrevDictHash_6(),
	AnalyticsTracker_t731021378::get_offset_of_m_TrackableProperty_7(),
	AnalyticsTracker_t731021378::get_offset_of_m_Trigger_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (Trigger_t4199345191)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2204[8] = 
{
	Trigger_t4199345191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (TrackableProperty_t3943537984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[2] = 
{
	0,
	TrackableProperty_t3943537984::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (FieldWithTarget_t3058750293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[6] = 
{
	FieldWithTarget_t3058750293::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t3058750293::get_offset_of_m_Target_1(),
	FieldWithTarget_t3058750293::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t3058750293::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t3058750293::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t3058750293::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (TriggerBool_t501031542)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2207[4] = 
{
	TriggerBool_t501031542::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (TriggerLifecycleEvent_t3193146760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2208[9] = 
{
	TriggerLifecycleEvent_t3193146760::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (TriggerOperator_t3611898925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2209[9] = 
{
	TriggerOperator_t3611898925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (TriggerType_t105272677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2210[5] = 
{
	TriggerType_t105272677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (U3CModuleU3E_t692745546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (Ball_t2206666566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[10] = 
{
	Ball_t2206666566::get_offset_of_paddle1_4(),
	Ball_t2206666566::get_offset_of_xPush_5(),
	Ball_t2206666566::get_offset_of_yPush_6(),
	Ball_t2206666566::get_offset_of_ballSounds_7(),
	Ball_t2206666566::get_offset_of_randomFactor_8(),
	Ball_t2206666566::get_offset_of_paddleToBallVector_9(),
	Ball_t2206666566::get_offset_of_unityMousePos_10(),
	Ball_t2206666566::get_offset_of_hasStarted_11(),
	Ball_t2206666566::get_offset_of_audioSource_12(),
	Ball_t2206666566::get_offset_of_myRigidbody2D_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (Block_t1429612866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[5] = 
{
	Block_t1429612866::get_offset_of_breakSound_4(),
	Block_t1429612866::get_offset_of_blockSparklesVFX_5(),
	Block_t1429612866::get_offset_of_hitSprites_6(),
	Block_t1429612866::get_offset_of_level_7(),
	Block_t1429612866::get_offset_of_timesHit_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (GameSession_t4087811243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[5] = 
{
	GameSession_t4087811243::get_offset_of_gameSpeed_4(),
	GameSession_t4087811243::get_offset_of_pointsPerBlockDestroyed_5(),
	GameSession_t4087811243::get_offset_of_scoreText_6(),
	GameSession_t4087811243::get_offset_of_isAutoPlayEnabled_7(),
	GameSession_t4087811243::get_offset_of_currentScore_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (Level_t2237665516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[2] = 
{
	Level_t2237665516::get_offset_of_breakableBlocks_4(),
	Level_t2237665516::get_offset_of_sceneManeger_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (LoseCollider_t2109713310), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (Paddle_t4140593882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[6] = 
{
	Paddle_t4140593882::get_offset_of_minX_4(),
	Paddle_t4140593882::get_offset_of_maxX_5(),
	Paddle_t4140593882::get_offset_of_unityPaddlePos_6(),
	Paddle_t4140593882::get_offset_of_gameSession_7(),
	Paddle_t4140593882::get_offset_of_ball_8(),
	Paddle_t4140593882::get_offset_of_screenWidthInUnits_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (SceneManeger_t385596978), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (Benchmark01_t1571072624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[10] = 
{
	Benchmark01_t1571072624::get_offset_of_BenchmarkType_4(),
	Benchmark01_t1571072624::get_offset_of_TMProFont_5(),
	Benchmark01_t1571072624::get_offset_of_TextMeshFont_6(),
	Benchmark01_t1571072624::get_offset_of_m_textMeshPro_7(),
	Benchmark01_t1571072624::get_offset_of_m_textContainer_8(),
	Benchmark01_t1571072624::get_offset_of_m_textMesh_9(),
	0,
	0,
	Benchmark01_t1571072624::get_offset_of_m_material01_12(),
	Benchmark01_t1571072624::get_offset_of_m_material02_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (U3CStartU3Ec__Iterator0_t2216151886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[5] = 
{
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (Benchmark01_UGUI_t3264177817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[10] = 
{
	Benchmark01_UGUI_t3264177817::get_offset_of_BenchmarkType_4(),
	Benchmark01_UGUI_t3264177817::get_offset_of_canvas_5(),
	Benchmark01_UGUI_t3264177817::get_offset_of_TMProFont_6(),
	Benchmark01_UGUI_t3264177817::get_offset_of_TextMeshFont_7(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_textMeshPro_8(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_textMesh_9(),
	0,
	0,
	Benchmark01_UGUI_t3264177817::get_offset_of_m_material01_12(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_material02_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (U3CStartU3Ec__Iterator0_t2622988697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[5] = 
{
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (Benchmark02_t1571269232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[3] = 
{
	Benchmark02_t1571269232::get_offset_of_SpawnType_4(),
	Benchmark02_t1571269232::get_offset_of_NumberOfNPC_5(),
	Benchmark02_t1571269232::get_offset_of_floatingText_Script_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (Benchmark03_t1571203696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[3] = 
{
	Benchmark03_t1571203696::get_offset_of_SpawnType_4(),
	Benchmark03_t1571203696::get_offset_of_NumberOfNPC_5(),
	Benchmark03_t1571203696::get_offset_of_TheFont_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (Benchmark04_t1570876016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[5] = 
{
	Benchmark04_t1570876016::get_offset_of_SpawnType_4(),
	Benchmark04_t1570876016::get_offset_of_MinPointSize_5(),
	Benchmark04_t1570876016::get_offset_of_MaxPointSize_6(),
	Benchmark04_t1570876016::get_offset_of_Steps_7(),
	Benchmark04_t1570876016::get_offset_of_m_Transform_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (CameraController_t2264742161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2232[25] = 
{
	CameraController_t2264742161::get_offset_of_cameraTransform_4(),
	CameraController_t2264742161::get_offset_of_dummyTarget_5(),
	CameraController_t2264742161::get_offset_of_CameraTarget_6(),
	CameraController_t2264742161::get_offset_of_FollowDistance_7(),
	CameraController_t2264742161::get_offset_of_MaxFollowDistance_8(),
	CameraController_t2264742161::get_offset_of_MinFollowDistance_9(),
	CameraController_t2264742161::get_offset_of_ElevationAngle_10(),
	CameraController_t2264742161::get_offset_of_MaxElevationAngle_11(),
	CameraController_t2264742161::get_offset_of_MinElevationAngle_12(),
	CameraController_t2264742161::get_offset_of_OrbitalAngle_13(),
	CameraController_t2264742161::get_offset_of_CameraMode_14(),
	CameraController_t2264742161::get_offset_of_MovementSmoothing_15(),
	CameraController_t2264742161::get_offset_of_RotationSmoothing_16(),
	CameraController_t2264742161::get_offset_of_previousSmoothing_17(),
	CameraController_t2264742161::get_offset_of_MovementSmoothingValue_18(),
	CameraController_t2264742161::get_offset_of_RotationSmoothingValue_19(),
	CameraController_t2264742161::get_offset_of_MoveSensitivity_20(),
	CameraController_t2264742161::get_offset_of_currentVelocity_21(),
	CameraController_t2264742161::get_offset_of_desiredPosition_22(),
	CameraController_t2264742161::get_offset_of_mouseX_23(),
	CameraController_t2264742161::get_offset_of_mouseY_24(),
	CameraController_t2264742161::get_offset_of_moveVector_25(),
	CameraController_t2264742161::get_offset_of_mouseWheel_26(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (CameraModes_t3200559075)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2233[4] = 
{
	CameraModes_t3200559075::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (ChatController_t3486202795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[3] = 
{
	ChatController_t3486202795::get_offset_of_TMP_ChatInput_4(),
	ChatController_t3486202795::get_offset_of_TMP_ChatOutput_5(),
	ChatController_t3486202795::get_offset_of_ChatScrollbar_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (EnvMapAnimator_t1140999784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[3] = 
{
	EnvMapAnimator_t1140999784::get_offset_of_RotationSpeeds_4(),
	EnvMapAnimator_t1140999784::get_offset_of_m_textMeshPro_5(),
	EnvMapAnimator_t1140999784::get_offset_of_m_material_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (U3CStartU3Ec__Iterator0_t1520811813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[5] = 
{
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U3CmatrixU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (ObjectSpin_t341713598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[10] = 
{
	ObjectSpin_t341713598::get_offset_of_SpinSpeed_4(),
	ObjectSpin_t341713598::get_offset_of_RotationRange_5(),
	ObjectSpin_t341713598::get_offset_of_m_transform_6(),
	ObjectSpin_t341713598::get_offset_of_m_time_7(),
	ObjectSpin_t341713598::get_offset_of_m_prevPOS_8(),
	ObjectSpin_t341713598::get_offset_of_m_initial_Rotation_9(),
	ObjectSpin_t341713598::get_offset_of_m_initial_Position_10(),
	ObjectSpin_t341713598::get_offset_of_m_lightColor_11(),
	ObjectSpin_t341713598::get_offset_of_frames_12(),
	ObjectSpin_t341713598::get_offset_of_Motion_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (MotionType_t1905163921)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2238[4] = 
{
	MotionType_t1905163921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (ShaderPropAnimator_t3617420994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[4] = 
{
	ShaderPropAnimator_t3617420994::get_offset_of_m_Renderer_4(),
	ShaderPropAnimator_t3617420994::get_offset_of_m_Material_5(),
	ShaderPropAnimator_t3617420994::get_offset_of_GlowCurve_6(),
	ShaderPropAnimator_t3617420994::get_offset_of_m_frame_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (U3CAnimatePropertiesU3Ec__Iterator0_t4041402054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2240[5] = 
{
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U3CglowPowerU3E__1_0(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24this_1(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24current_2(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24disposing_3(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (SimpleScript_t3279312205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[3] = 
{
	SimpleScript_t3279312205::get_offset_of_m_textMeshPro_4(),
	0,
	SimpleScript_t3279312205::get_offset_of_m_frame_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (SkewTextExample_t3460249701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[4] = 
{
	SkewTextExample_t3460249701::get_offset_of_m_TextComponent_4(),
	SkewTextExample_t3460249701::get_offset_of_VertexCurve_5(),
	SkewTextExample_t3460249701::get_offset_of_CurveScale_6(),
	SkewTextExample_t3460249701::get_offset_of_ShearAmount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (U3CWarpTextU3Ec__Iterator0_t116130919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[13] = 
{
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3Cold_CurveScaleU3E__0_0(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3Cold_ShearValueU3E__0_1(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3Cold_curveU3E__0_2(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CtextInfoU3E__1_3(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CcharacterCountU3E__1_4(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CboundsMinXU3E__1_5(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CboundsMaxXU3E__1_6(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CverticesU3E__2_7(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CmatrixU3E__2_8(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24this_9(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24current_10(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24disposing_11(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (TeleType_t2409835159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2244[3] = 
{
	TeleType_t2409835159::get_offset_of_label01_4(),
	TeleType_t2409835159::get_offset_of_label02_5(),
	TeleType_t2409835159::get_offset_of_m_textMeshPro_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (U3CStartU3Ec__Iterator0_t3341539328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[7] = 
{
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U3CtotalVisibleCharactersU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U3CcounterU3E__0_1(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U3CvisibleCountU3E__0_2(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24this_3(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24current_4(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24disposing_5(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (TextConsoleSimulator_t3766250034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[2] = 
{
	TextConsoleSimulator_t3766250034::get_offset_of_m_TextComponent_4(),
	TextConsoleSimulator_t3766250034::get_offset_of_hasTextChanged_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (U3CRevealCharactersU3Ec__Iterator0_t860191687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[8] = 
{
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_textComponent_0(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U3CtextInfoU3E__0_1(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U3CtotalVisibleCharactersU3E__0_2(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U3CvisibleCountU3E__0_3(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24this_4(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24current_5(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24disposing_6(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (U3CRevealWordsU3Ec__Iterator1_t1343183262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[9] = 
{
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_textComponent_0(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CtotalWordCountU3E__0_1(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CtotalVisibleCharactersU3E__0_2(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CcounterU3E__0_3(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CcurrentWordU3E__0_4(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CvisibleCountU3E__0_5(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U24current_6(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U24disposing_7(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (TextMeshProFloatingText_t845872552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[10] = 
{
	TextMeshProFloatingText_t845872552::get_offset_of_TheFont_4(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_floatingText_5(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_textMeshPro_6(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_textMesh_7(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_transform_8(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_floatingText_Transform_9(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_cameraTransform_10(),
	TextMeshProFloatingText_t845872552::get_offset_of_lastPOS_11(),
	TextMeshProFloatingText_t845872552::get_offset_of_lastRotation_12(),
	TextMeshProFloatingText_t845872552::get_offset_of_SpawnType_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2250[12] = 
{
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3CCountDurationU3E__0_0(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cstarting_CountU3E__0_1(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Ccurrent_CountU3E__0_2(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cstart_posU3E__0_3(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cstart_colorU3E__0_4(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3CalphaU3E__0_5(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cint_counterU3E__0_6(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3CfadeDurationU3E__0_7(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24this_8(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24current_9(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24disposing_10(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[12] = 
{
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3CCountDurationU3E__0_0(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cstarting_CountU3E__0_1(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Ccurrent_CountU3E__0_2(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cstart_posU3E__0_3(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cstart_colorU3E__0_4(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3CalphaU3E__0_5(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cint_counterU3E__0_6(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3CfadeDurationU3E__0_7(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24this_8(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24current_9(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24disposing_10(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (TextMeshSpawner_t177691618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2252[4] = 
{
	TextMeshSpawner_t177691618::get_offset_of_SpawnType_4(),
	TextMeshSpawner_t177691618::get_offset_of_NumberOfNPC_5(),
	TextMeshSpawner_t177691618::get_offset_of_TheFont_6(),
	TextMeshSpawner_t177691618::get_offset_of_floatingText_Script_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (TMP_DigitValidator_t573672104), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (TMP_ExampleScript_01_t3051742005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[5] = 
{
	TMP_ExampleScript_01_t3051742005::get_offset_of_ObjectType_4(),
	TMP_ExampleScript_01_t3051742005::get_offset_of_isStatic_5(),
	TMP_ExampleScript_01_t3051742005::get_offset_of_m_text_6(),
	0,
	TMP_ExampleScript_01_t3051742005::get_offset_of_count_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (objectType_t4082700821)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2255[3] = 
{
	objectType_t4082700821::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (TMP_FrameRateCounter_t314972976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[10] = 
{
	TMP_FrameRateCounter_t314972976::get_offset_of_UpdateInterval_4(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_LastInterval_5(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_Frames_6(),
	TMP_FrameRateCounter_t314972976::get_offset_of_AnchorPosition_7(),
	TMP_FrameRateCounter_t314972976::get_offset_of_htmlColorTag_8(),
	0,
	TMP_FrameRateCounter_t314972976::get_offset_of_m_TextMeshPro_10(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_frameCounter_transform_11(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_camera_12(),
	TMP_FrameRateCounter_t314972976::get_offset_of_last_AnchorPosition_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (FpsCounterAnchorPositions_t1585798158)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2257[5] = 
{
	FpsCounterAnchorPositions_t1585798158::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (TMP_PhoneNumberValidator_t743649728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (TMP_TextEventCheck_t1103849140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[1] = 
{
	TMP_TextEventCheck_t1103849140::get_offset_of_TextEventHandler_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (TMP_TextEventHandler_t1869054637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[12] = 
{
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnCharacterSelection_4(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnSpriteSelection_5(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnWordSelection_6(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnLineSelection_7(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnLinkSelection_8(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_TextComponent_9(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_Camera_10(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_Canvas_11(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_selectedLink_12(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_lastCharIndex_13(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_lastWordIndex_14(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_lastLineIndex_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (CharacterSelectionEvent_t3109943174), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (SpriteSelectionEvent_t2798445241), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (WordSelectionEvent_t1841909953), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (LineSelectionEvent_t2868010532), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (LinkSelectionEvent_t1590929858), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (TMP_TextInfoDebugTool_t1868681444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[9] = 
{
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowCharacters_4(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowWords_5(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowLinks_6(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowLines_7(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowMeshBounds_8(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowTextBounds_9(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ObjectStats_10(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_m_TextComponent_11(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_m_Transform_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (TMP_TextSelector_A_t3982526506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[6] = 
{
	TMP_TextSelector_A_t3982526506::get_offset_of_m_TextMeshPro_4(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_Camera_5(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_isHoveringObject_6(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_selectedLink_7(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_lastCharIndex_8(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_lastWordIndex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (TMP_TextSelector_B_t3982526505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[14] = 
{
	TMP_TextSelector_B_t3982526505::get_offset_of_TextPopup_Prefab_01_4(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_TextPopup_RectTransform_5(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_TextPopup_TMPComponent_6(),
	0,
	0,
	TMP_TextSelector_B_t3982526505::get_offset_of_m_TextMeshPro_9(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_Canvas_10(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_Camera_11(),
	TMP_TextSelector_B_t3982526505::get_offset_of_isHoveringObject_12(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_selectedWord_13(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_selectedLink_14(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_lastIndex_15(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_matrix_16(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_cachedMeshInfoVertexData_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (TMP_UiFrameRateCounter_t811747397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2269[9] = 
{
	TMP_UiFrameRateCounter_t811747397::get_offset_of_UpdateInterval_4(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_LastInterval_5(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_Frames_6(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_AnchorPosition_7(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_htmlColorTag_8(),
	0,
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_TextMeshPro_10(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_frameCounter_transform_11(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_last_AnchorPosition_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (FpsCounterAnchorPositions_t2550331785)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2270[5] = 
{
	FpsCounterAnchorPositions_t2550331785::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (TMPro_InstructionOverlay_t4246705477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[6] = 
{
	TMPro_InstructionOverlay_t4246705477::get_offset_of_AnchorPosition_4(),
	0,
	TMPro_InstructionOverlay_t4246705477::get_offset_of_m_TextMeshPro_6(),
	TMPro_InstructionOverlay_t4246705477::get_offset_of_m_textContainer_7(),
	TMPro_InstructionOverlay_t4246705477::get_offset_of_m_frameCounter_transform_8(),
	TMPro_InstructionOverlay_t4246705477::get_offset_of_m_camera_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (FpsCounterAnchorPositions_t2334657565)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2272[5] = 
{
	FpsCounterAnchorPositions_t2334657565::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (VertexColorCycler_t3003193665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[1] = 
{
	VertexColorCycler_t3003193665::get_offset_of_m_TextComponent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t897284962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[11] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U3CcurrentCharacterU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U3Cc0U3E__0_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U3CcharacterCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U3CmaterialIndexU3E__1_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U3CnewVertexColorsU3E__1_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U3CvertexIndexU3E__1_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U24this_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U24current_8(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U24disposing_9(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t897284962::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (VertexJitter_t4087429332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[5] = 
{
	VertexJitter_t4087429332::get_offset_of_AngleMultiplier_4(),
	VertexJitter_t4087429332::get_offset_of_SpeedMultiplier_5(),
	VertexJitter_t4087429332::get_offset_of_CurveScale_6(),
	VertexJitter_t4087429332::get_offset_of_m_TextComponent_7(),
	VertexJitter_t4087429332::get_offset_of_hasTextChanged_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (VertexAnim_t2231884842)+ sizeof (RuntimeObject), sizeof(VertexAnim_t2231884842 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2276[3] = 
{
	VertexAnim_t2231884842::get_offset_of_angleRange_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexAnim_t2231884842::get_offset_of_angle_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexAnim_t2231884842::get_offset_of_speed_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t225534713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[10] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U3CloopCountU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U3CvertexAnimU3E__0_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U3CcachedMeshInfoU3E__0_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U3CcharacterCountU3E__1_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U3CmatrixU3E__2_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U24this_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U24current_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U24disposing_8(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t225534713::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (VertexShakeA_t4262048139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[6] = 
{
	VertexShakeA_t4262048139::get_offset_of_AngleMultiplier_4(),
	VertexShakeA_t4262048139::get_offset_of_SpeedMultiplier_5(),
	VertexShakeA_t4262048139::get_offset_of_ScaleMultiplier_6(),
	VertexShakeA_t4262048139::get_offset_of_RotationMultiplier_7(),
	VertexShakeA_t4262048139::get_offset_of_m_TextComponent_8(),
	VertexShakeA_t4262048139::get_offset_of_hasTextChanged_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t956521787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[9] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U3CcopyOfVerticesU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U3CcharacterCountU3E__1_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U3ClineCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U3CmatrixU3E__2_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U24this_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U24current_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U24disposing_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t956521787::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (VertexShakeB_t1533164784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[5] = 
{
	VertexShakeB_t1533164784::get_offset_of_AngleMultiplier_4(),
	VertexShakeB_t1533164784::get_offset_of_SpeedMultiplier_5(),
	VertexShakeB_t1533164784::get_offset_of_CurveScale_6(),
	VertexShakeB_t1533164784::get_offset_of_m_TextComponent_7(),
	VertexShakeB_t1533164784::get_offset_of_hasTextChanged_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t168300594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2281[9] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U3CcopyOfVerticesU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U3CcharacterCountU3E__1_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U3ClineCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U3CmatrixU3E__2_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U24this_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U24current_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U24disposing_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t168300594::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (VertexZoom_t550798657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[5] = 
{
	VertexZoom_t550798657::get_offset_of_AngleMultiplier_4(),
	VertexZoom_t550798657::get_offset_of_SpeedMultiplier_5(),
	VertexZoom_t550798657::get_offset_of_CurveScale_6(),
	VertexZoom_t550798657::get_offset_of_m_TextComponent_7(),
	VertexZoom_t550798657::get_offset_of_hasTextChanged_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2283[10] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U3CcachedMeshInfoVertexDataU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U3CscaleSortingOrderU3E__0_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U3CcharacterCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U3CmatrixU3E__2_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U24this_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U24current_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U24disposing_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U24PC_8(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3792186008::get_offset_of_U24locvar0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[2] = 
{
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514::get_offset_of_modifiedCharScale_0(),
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (WarpTextExample_t3821118074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[5] = 
{
	WarpTextExample_t3821118074::get_offset_of_m_TextComponent_4(),
	WarpTextExample_t3821118074::get_offset_of_VertexCurve_5(),
	WarpTextExample_t3821118074::get_offset_of_AngleMultiplier_6(),
	WarpTextExample_t3821118074::get_offset_of_SpeedMultiplier_7(),
	WarpTextExample_t3821118074::get_offset_of_CurveScale_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (U3CWarpTextU3Ec__Iterator0_t4025661343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[12] = 
{
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3Cold_CurveScaleU3E__0_0(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3Cold_curveU3E__0_1(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CtextInfoU3E__1_2(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CcharacterCountU3E__1_3(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CboundsMinXU3E__1_4(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CboundsMaxXU3E__1_5(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CverticesU3E__2_6(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CmatrixU3E__2_7(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24this_8(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24current_9(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24disposing_10(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24PC_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
