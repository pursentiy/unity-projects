﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;

[TestFixture]
public class ActionMasterTest
{
	private ActionMaster.Action endTurn = ActionMaster.Action.EndTurn;
	private ActionMaster.Action endGame = ActionMaster.Action.EndGame;
	private ActionMaster.Action tidy = ActionMaster.Action.Tidy;
	private ActionMaster.Action reset = ActionMaster.Action.Reset;

	private ActionMaster actionMaster;

	[SetUp]
	public void Setup()
	{
		actionMaster = new ActionMaster();
	}

	[Test]
    public void T00PassingTest()
    {
        Assert.AreEqual(1, 1);
    }

	[Test]
	public void T01OneStrikeReturnsEndTurn()
	{
		Assert.AreEqual(endTurn, actionMaster.Bowl(10));
	}

	[Test]
	public void T02Bowl8ReturnsTidy()
	{
		Assert.AreEqual(tidy, actionMaster.Bowl(8));
	}

	[Test]
	public void T03Bowl28SpareReturnsEndTurn()
	{
		actionMaster.Bowl(2);
		Assert.AreEqual(endTurn, actionMaster.Bowl(8));
	}

	[Test]
	public void T04CheckResetAtStrikeLastFrame()
	{
		int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
		foreach(int roll in rolls)
		{
			actionMaster.Bowl(roll);
		}
		Assert.AreEqual(reset, actionMaster.Bowl(10));
	}

	[Test]
	public void T05CheckResetAtSpareLastFrame()
	{
		int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
		foreach (int roll in rolls)
		{
			actionMaster.Bowl(roll);
		}
		actionMaster.Bowl(1);
		Assert.AreEqual(reset, actionMaster.Bowl(9));
	}

	[Test]
	public void T06YouTubeRollsEndInEndGame()
	{
		int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 8};
		foreach (int roll in rolls)
		{
			actionMaster.Bowl(roll);
		}
		//actionMaster.Bowl(1);
		Assert.AreEqual(endGame, actionMaster.Bowl(9));
	}

	[Test]
	public void T08GameEndsAtBowl20()
	{
		int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
		foreach (int roll in rolls)
		{
			actionMaster.Bowl(roll);
		}
		Assert.AreEqual(endGame, actionMaster.Bowl(1));
	}

	[Test]
	public void T08DarylBowl20Test()
	{
		int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10 };
		foreach (int roll in rolls)
		{
			actionMaster.Bowl(roll);
		}
		Assert.AreEqual(tidy, actionMaster.Bowl(5));
	}

	[Test]
	public void T09BensBowl20Test()
	{
		int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10 };
		foreach (int roll in rolls)
		{
			actionMaster.Bowl(roll);
		}
		Assert.AreEqual(tidy, actionMaster.Bowl(0));
	}

	[Test]
	public void T10Spare10Pins2Bowl()
	{
		int[] rolls = { 0, 10};
		foreach (int roll in rolls)
		{
			actionMaster.Bowl(roll);
		}
		Assert.AreEqual(tidy, actionMaster.Bowl(5));
	}

	[Test]
	public void T11Dondi10thFrameTurkey()
	{
		int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
		foreach (int roll in rolls)
		{
			actionMaster.Bowl(roll);
		}
		Assert.AreEqual(reset, actionMaster.Bowl(10));
		Assert.AreEqual(reset, actionMaster.Bowl(10));
		Assert.AreEqual(endGame, actionMaster.Bowl(10));
	}

	[Test]
	public void T12ZeroOneGivesEndTurn()
	{
		actionMaster.Bowl(0);
		Assert.AreEqual(endTurn, actionMaster.Bowl(1));
	}
}