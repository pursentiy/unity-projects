﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	public Vector3 launchVelocity;
	private bool inGame = false;
	private Vector3 startPosition;

	private Rigidbody rigidbody;
	private AudioSource audioSource;


	// Use this for initialization
	void Start ()
	{
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
		startPosition = transform.position;
	}

	public void Launch(Vector3 velocity)
	{
		inGame = true;
		audioSource = GetComponent<AudioSource>();
		audioSource.Play();
		rigidbody.useGravity = true;
		rigidbody.velocity = velocity;
	}

	public bool IfBallLaunched()
	{
		return inGame;
	}

	public void Reset()
	{
		transform.position = startPosition;
		rigidbody.velocity = new Vector3(0, 0, 0);
		rigidbody.angularVelocity = new Vector3(0, 0, 0);
		rigidbody.useGravity = false;
		inGame = false;
	}

	// Update is called once per frame
	void Update () {
	
	}
}
