﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Ball))]
public class BallDragLaunch : MonoBehaviour {

	private Ball ball;
	private float startTime, endTime;

	private Vector3 dragStart,  dragEnd;

	// Use this for initialization
	void Start () {
		ball = GetComponent<Ball>();
	}
	
	public void DragStart()
	{
		dragStart = Input.mousePosition;
		startTime = Time.time;
	}

	public void DragEnd()
	{
		dragEnd = Input.mousePosition;
		endTime = Time.time;

		float dragDuration = startTime - endTime;
		float launchSpeedX = (-1)*(dragEnd.x - dragStart.x) / dragDuration;
		float launchSpeedZ = (-1)*(dragEnd.y - dragStart.y) / dragDuration;

		Vector3 launchVelocity = new Vector3(launchSpeedX, 0, launchSpeedZ);
		ball.Launch(launchVelocity);
	}

	public void MoveStart(float amount)
	{
		if (!ball.IfBallLaunched())
		{
			if(ball.transform.position.x + amount < 31 && ball.transform.position.x + amount > -31)
			{
				ball.transform.Translate(new Vector3(amount, 0, 0));
			}
		}
	}
}
