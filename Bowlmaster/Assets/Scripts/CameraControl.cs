﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

	public Ball ball;
	private bool ifCameraShouldStop = false;

	private Vector3 offset;
	private static Vector3 cameraPos;

	// Use this for initialization
	void Start () {
		offset = transform.position - ball.transform.position;
		//Debug.Log(offset);
	}
	
	// Update is called once per frame
	void Update () {
		if(ball.transform.position.z <= 1829f)
		{
			transform.position = ball.transform.position + offset;
		}
	}
}
