﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaneBox : MonoBehaviour {

	PinSetter pinSetter;

	private void Start()
	{
		pinSetter = FindObjectOfType<PinSetter>();
	}

	private void OnTriggerExit(Collider collider)
	{
		if(collider.gameObject.name == "Ball")
		{
			pinSetter.BallHasLeftBox();
			Debug.Log("Ball has left the line");
		}
	}
}
