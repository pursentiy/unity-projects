﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pin : MonoBehaviour {

	public float standingThreshholdX = 273f;
	public float standingThreshholdZ = 3f;
	public float distanceToRaise = 60f;

	void Awake()
	{
		this.GetComponent<Rigidbody>().solverVelocityIterations = 10;
	}

	public bool IsStanding()
	{
		Vector3 rotationInEuler = transform.rotation.eulerAngles;
		float tiltInX = Mathf.Abs(rotationInEuler.x);
		float titlInZ = Mathf.Abs(rotationInEuler.z);
		return (tiltInX  < standingThreshholdX && titlInZ < standingThreshholdZ);
	}

	private void Update()
	{
		//print(name + IsStanding());
	}

	public void RaiseIfStanding()
	{
		if (IsStanding())
		{
			transform.Translate(new Vector3(0, distanceToRaise, 0), Space.World);
			GetComponent<Rigidbody>().useGravity = false;
		}
	}

	public void Lower()
	{
			transform.Translate(new Vector3(0, -distanceToRaise, 0), Space.World);
			GetComponent<Rigidbody>().useGravity = true;
			GetComponent<Rigidbody>().useGravity = true;
	}
}
