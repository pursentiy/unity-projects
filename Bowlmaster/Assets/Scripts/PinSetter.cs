﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinSetter : MonoBehaviour {

	private int lastStandingCound = -1;
	public Text standingDisplay;
	public GameObject pinSet;

	private Pin[] standingPinsObj;

	private Ball ball;
	private float lastChangeTime;
	private bool ballOutOfPlay = false;
	private int lastSettledCount = 10;
	private Animator animator;

	private ActionMaster actionMaster = new ActionMaster();

	public Vector3 pinSetPosition = new Vector3(0, 0, 1829);

	private void Start()
	{
		ball = FindObjectOfType<Ball>();
		animator = GetComponent<Animator>();
	}

	int CountStanding()
	{
		int standingPins = 0;
		foreach (Pin pin in FindObjectsOfType<Pin>())
		{
			if (pin.IsStanding())
			{
				standingPins++;
			}
		}
		return standingPins;
	}

	private void OnTriggerEnter(Collider collider)
	{
		GameObject thingHit = collider.gameObject;

		if (thingHit.GetComponent<Ball>())
		{
			ballOutOfPlay = true;
			standingDisplay.color = new Color(255, 0, 255);
		}

	}


	private void Update()
	{
		standingDisplay.text = CountStanding().ToString();

		if (ballOutOfPlay)
		{
			UpdateStandingCountAndSettle();
			standingDisplay.color = Color.red;
		}
	}

	void UpdateStandingCountAndSettle()
	{
		int currentStanding = CountStanding();

		if(currentStanding != lastStandingCound)
		{
			lastChangeTime = Time.time;
			lastStandingCound = currentStanding;
			return;
		}

		float settleTime = 3f;
		if((Time.time - lastChangeTime) > settleTime)
		{
			PinsHaveSettled();
		}

	}

	void PinsHaveSettled()
	{
		int standing = CountStanding();
		int pinFall = lastSettledCount - standing;
		lastSettledCount = standing;
		ActionMaster.Action action = actionMaster.Bowl(pinFall);
		Debug.Log(action);
		if(action == ActionMaster.Action.Tidy)
		{
			animator.SetTrigger("tidyTrigger");
		}
		else if(action == ActionMaster.Action.EndTurn)
		{
			animator.SetTrigger("resetTrigger");
			lastSettledCount = 10;
		}
		else if (action == ActionMaster.Action.Reset)
		{
			animator.SetTrigger("resetTrigger");
			lastSettledCount = 10;
		}
		else if (action == ActionMaster.Action.EndGame)
		{
			throw new UnityException("No animator setup");
		}
		ball.Reset();
		lastStandingCound = -1;
		ballOutOfPlay = false;
		standingDisplay.color = Color.green;
	}

	public void RaisePins()
	{
		foreach (Pin pin in FindObjectsOfType<Pin>())
		{
			pin.RaiseIfStanding();
		}
	}

	public void LowerPins()
	{
		foreach (Pin pin in FindObjectsOfType<Pin>())
		{
			pin.Lower();
		}
	}

	public void BallHasLeftBox()
	{
		ballOutOfPlay = true;
	}

	public void RenewPins()
	{
		GameObject newPins = Instantiate(pinSet, pinSetPosition, Quaternion.identity);
		foreach (Pin pin in FindObjectsOfType<Pin>())
		{
			pin.GetComponent<Rigidbody>().useGravity = false;
		}
		newPins.transform.position += new Vector3(0, 60, 0);
	}
}
