﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
	[SerializeField] Transform flyToTarget;
	[SerializeField] float speed = 0.1f;
	Vector3 direction;
	Animator animator;
	// Start is called before the first frame update
	void Start()
    {
		direction = flyToTarget.position - transform.position;
		animator = GetComponent<Animator>();
	}

    // Update is called once per frame
    void Update()
    {
		transform.Translate(direction * Time.deltaTime * speed);
    }

	private void OnTriggerEnter2D(Collider2D collision)
	{
		animator.SetTrigger("collected");
	}
}
