﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyHero : MonoBehaviour
{

	[SerializeField] Text damageText;
	[SerializeField] Text priceText;
	[SerializeField] int damage;
	[SerializeField] int price;
	[SerializeField] Transform positionToSpawnHero;
	[SerializeField] HeroHelper hero;

	int heroCount = 0;

	GameHelper gameHelper;

	// Start is called before the first frame update
	void Start()
    {
		gameHelper = FindObjectOfType<GameHelper>();
		damageText.text = damage.ToString();
		priceText.text = price.ToString();
	}

	public void BuyHeroes()
	{
		if (gameHelper.IfCanBuyUpgrade(price))
		{
			heroCount++;
			gameHelper.BuyHero(price, hero, positionToSpawnHero, heroCount);
		}
	}
}
