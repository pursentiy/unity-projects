﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyUpgrade : MonoBehaviour
{

	[SerializeField] Text damageText;
	[SerializeField] Text priceText;
	[SerializeField] int damage;
	[SerializeField] int price;

	GameHelper gameHelper;

	// Start is called before the first frame update
	void Start()
    {
		gameHelper = FindObjectOfType<GameHelper>();
		damageText.text = damage.ToString();
		priceText.text = price.ToString();
	}

	public void BuyUpgrades()
	{
		if (gameHelper.IfCanBuyUpgrade(price))
		{
			gameHelper.BuyUpgrade(price, damage, gameObject.transform.GetChild(2).GetComponent<Image>().sprite);
			Destroy(gameObject);
		}
	}
}
