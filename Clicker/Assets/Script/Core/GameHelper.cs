﻿using EZCameraShake;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameHelper : MonoBehaviour
{
	//TODO вырезать скелет динозавра
	//TODO  выравнивание стаств
	//TODO крутящиеся иконки
	[SerializeField] Text goldScore;
	[SerializeField] Text attackPoints;
	[SerializeField] Text rubyPoints;
	[SerializeField] GameObject goldToInstantiate;
	[SerializeField] Transform goldFlyToTarget;
	[SerializeField] GameObject[] MonsterPrefabs;
	[SerializeField] GameObject[] MonsterRarePrefabs;
	[SerializeField] float damagePoints = 10;

	int playerGold = 0;
	int playerRuby = 0;
	int spawnedMonsters = 0;
	private float deleteGoldTime = 2f;

	private void Update()
	{
		goldScore.text = playerGold.ToString();
		rubyPoints.text = playerRuby.ToString();
		attackPoints.text = damagePoints.ToString();
	}

	public float getPlayerHitPoints()
	{
		return damagePoints;
	}

	public int GetPlayerGold()
	{
		return playerGold;
	}

	public void AddGold(int goldToAdd)
	{
		playerGold += goldToAdd;
	}

	public void AddRuby()
	{
		playerRuby++;
	}

	public bool IfCanBuyUpgrade(int amount)
	{
		return playerGold >= amount;
	}

	public void BuyUpgrade(int amount, int newDamagePoints, Sprite weaponImage)
	{
		playerGold -= amount;
		damagePoints = newDamagePoints;
		SetUpgradeImage(weaponImage);
	}

	private void SetUpgradeImage(Sprite weaponImage)
	{
		GameObject attackImage = GameObject.FindGameObjectWithTag("WeaponImage");
		attackImage.GetComponent<Image>().sprite = weaponImage;
	}

	public void BuyHero(int amount, HeroHelper heroToInstantiate, Transform positionToSpawnHero, int heroCount)
	{
		//TODO PLACING HEROES
		playerGold -= amount;
		Vector3 posToSpawn = new Vector3(positionToSpawnHero.position.x - heroCount + 0.25f, positionToSpawnHero.position.y, positionToSpawnHero.position.z);
		Instantiate(heroToInstantiate, posToSpawn, Quaternion.identity);
	}

	public void InstantiateGold()
	{
		GameObject goldObj = Instantiate(goldToInstantiate, transform.position, Quaternion.identity);
		//TODO FLY TOWARDS GOLD ICON
		goldObj.transform.position = Vector2.MoveTowards(goldObj.transform.position, goldFlyToTarget.position, Time.deltaTime * 5);
		Destroy(goldObj, deleteGoldTime);
	}

	public void SpawnMonster()
	{
		spawnedMonsters++;
		if (spawnedMonsters % 10 == 0)
		{
			InstantiateMosnter(MonsterRarePrefabs);
		}
		else
		{
			InstantiateMosnter(MonsterPrefabs);
		}
	}

	public void ShakeCamera()
	{
		CameraShaker.Instance.ShakeOnce(1f, 1f, .9f, .9f);
	}

	private void InstantiateMosnter(GameObject[] prefabsArray)
	{ 
		int randomMaxValue = Random.Range(0, prefabsArray.Length);
		GameObject monsterObj = Instantiate(prefabsArray[randomMaxValue], transform.position, Quaternion.identity);
	}
}
