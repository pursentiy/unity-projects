﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingText : MonoBehaviour
{
	[SerializeField] float destroyTime = 1f;
	[SerializeField] Vector3 randomizeIntensity = new Vector3(0.5f, 0, 0);
    // Start is called before the first frame update
    void Start()
    {
		Destroy(gameObject, destroyTime);
		transform.localPosition += new Vector3(Random.Range(-randomizeIntensity.x, randomizeIntensity.x), Random.Range(-randomizeIntensity.y, randomizeIntensity.y), 0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
