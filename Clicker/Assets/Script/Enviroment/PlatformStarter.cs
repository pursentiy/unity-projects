﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformStarter : MonoBehaviour
{
	[SerializeField] float minTime = 5f;
    void Start()
    {
		StartCoroutine(StartPlatform());
    }

	private IEnumerator StartPlatform()
	{
		yield return new WaitForSeconds(Random.Range(minTime, minTime*2));
		GetComponent<Animator>().SetBool("start", true);
	}
}
