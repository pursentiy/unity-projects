﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EZCameraShake;
using System;
using Random = UnityEngine.Random;

public class HealthEnemy : MonoBehaviour
{
	[SerializeField] GameObject damagePopupTextPrefab;
	[SerializeField] float maxHealth = 100;
	[SerializeField] float health = 100;
	[SerializeField] int goldAfterDeath = 90;
	[SerializeField] int rubyDropChance = 0;
	[SerializeField] GameObject rubyPrefab;
	Animator animator;
	Slider healthSlider;
	GameObject floatingText;
	bool isAlreadyDead;

	GameHelper gameHelper;

	private void Start()
	{
		floatingText = GameObject.FindGameObjectWithTag("floatingTextPos");
		gameHelper = FindObjectOfType<GameHelper>();
		SetHealthSlider();
		animator = GetComponent<Animator>();
		animator.SetTrigger("spawning");
	}

	private void SetHealthSlider()
	{
		GameObject slider = GameObject.FindGameObjectWithTag("sliderHealth");
		healthSlider = slider.GetComponent<Slider>();
		healthSlider.maxValue = maxHealth;
		healthSlider.value = maxHealth;
	}

	public void HandleHit(float amount)
	{
		health -= amount;
		healthSlider.value = health;
		ShowFloatingText(amount);
		//TODO if just critical hit
		if(amount > 150)
		{
			gameHelper.ShakeCamera();
		}
		if (health <= 0 && !isAlreadyDead)
		{
			animator.SetTrigger("die");
			isAlreadyDead = true;
			Invoke("Die", 2f);
		}
	}

	public bool IsAlreadyDead()
	{
		return isAlreadyDead;
	}

	private void ShowFloatingText(float amount)
	{
		GameObject text = Instantiate(damagePopupTextPrefab, floatingText.transform.position, Quaternion.identity, floatingText.transform);
		text.transform.localScale = new Vector3(Mathf.Log10(amount), Mathf.Log10(amount), 1f);
		text.transform.GetChild(0).GetComponent<TextMesh>().text = amount.ToString();
		//TODO if critical hit
		//if(amount > 150)
		//{
		//	text.transform.GetChild(0).GetComponent<TextMesh>().color = Color.red;
		//}
	}

	private void Die()
	{
		gameHelper.AddGold(goldAfterDeath);
		gameHelper.InstantiateGold();
		gameHelper.SpawnMonster();
		DropRubyChance();
		Destroy(gameObject);

	}

	public void HitAnimation()
	{
		if (!isAlreadyDead)
		{
			animator.SetTrigger("hit");
		}
	}

	private void DropRubyChance()
	{
		if (Random.Range(0, 100) <= rubyDropChance)
		{
			CameraShaker.Instance.ShakeOnce(2f, 2f, 1f, 1f);
			gameHelper.AddRuby();
			Debug.Log("Ruby has dropped");
		}
	}
}
