﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class HeroHelper : MonoBehaviour
{

	[SerializeField] float heroHitPoints = 20;
	[SerializeField] float attackSpeed = 7f;
	[SerializeField] AnimationClip[] skeletonSlicesPrefabs;


	Animator animator;
	HitHandler hitHandler;
	SliceHelper slicesHelper;

	private void Start()
	{
		animator = GetComponent<Animator>();
		FindHitHandler();
		StartCoroutine(StartAttackEnemie());
		
	}

	public void SetSliceHelper(string tagName)
	{
		slicesHelper = GameObject.FindWithTag(tagName).GetComponent<SliceHelper>();
	}

	private void FindHitHandler()
	{
		hitHandler = FindObjectOfType<HitHandler>();
	}

	private IEnumerator StartAttackEnemie()
	{
		while (true)
		{
			yield return new WaitForSeconds(attackSpeed);
			if (hitHandler == null) FindHitHandler();
			animator.SetTrigger("hit");
		}
	}

	public void HitEnemie()
	{
		hitHandler.Hit(heroHitPoints);
		ShowSlice();
	}

	public void ShowSlice()
	{
		int index = AnimationRandomIndex(skeletonSlicesPrefabs.Length);
		slicesHelper.PlaySliceAnimation(skeletonSlicesPrefabs[index].name.ToString());
		//slicesAnimator.SetTrigger("Slice3");//sliceAnimations[index].name.ToString());

	}

	private int AnimationRandomIndex(int arrayLength)
	{
		return Random.Range(0, arrayLength);
	}

}
