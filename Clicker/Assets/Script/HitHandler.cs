﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitHandler : MonoBehaviour
{

	Animator animator;
	HealthEnemy healthEnemy;
	GameHelper gameHelper;
	Player player;

	void Start()
    {
		animator = GetComponent<Animator>();
		healthEnemy = GetComponent<HealthEnemy>();
		gameHelper = FindObjectOfType<GameHelper>();
		player = FindObjectOfType<Player>();
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			Hit(gameHelper.getPlayerHitPoints());
			PlayerAnimation();
		}
	}

	private void PlayerAnimation()
	{
		player.RunAttack();

	}

	public void Hit(float hitPoints)
	{
		healthEnemy.HitAnimation();
		healthEnemy.HandleHit(hitPoints);
	}
}
