﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class Player : MonoBehaviour
{
	Animator animator;
	[SerializeField] AnimationClip[] sliceAnimations;
	[SerializeField] AnimationClip[] attackAnimations;
	[SerializeField] int minRangeScale = 2;
	[SerializeField] int maxRangeScale = 8;

	SliceHelper slicesHelper;

	private void Start()
	{
		animator = GetComponent<Animator>();
		slicesHelper = GameObject.FindWithTag("PlayerSlice").GetComponent<SliceHelper>();
	}
	public void RunAttack()
	{
		int index = AnimationRandomIndex(attackAnimations.Length);
		//TODO SYNC ATTACK ANIMATION AND ENEMIE HIT 
		animator.SetTrigger(attackAnimations[index].name.ToString());
	}

	public void ShowSlice()
	{
		int index = AnimationRandomIndex(sliceAnimations.Length);
		slicesHelper.PlaySliceAnimation(sliceAnimations[index].name.ToString());
		//slicesAnimator.SetTrigger("Slice3");//sliceAnimations[index].name.ToString());

		float randScaleX = Random.Range(minRangeScale, maxRangeScale);
		float randScaleY = Random.Range(minRangeScale, maxRangeScale);

	}

	private int AnimationRandomIndex(int arrayLength)
	{
		return Random.Range(0, arrayLength);
	}
}
