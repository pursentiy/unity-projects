﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour {

	[Range(0f, 5f)]
	float currentSpeed = 1f;
	Health health;
	GameObject currentTarget;

	private void Awake()
	{
		FindObjectOfType<LevelController>().AttackerSpawned();
	}

	private void OnDestroy()
	{
		LevelController levelController = FindObjectOfType<LevelController>();
		if(levelController != null)
		{
			levelController.AttackerKilled();
		}
	}

	// Use this for initialization
	void Start () {
		health = gameObject.GetComponent<Health>();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		Projectile damageDealer = collision.GetComponent<Projectile>();
		if (damageDealer != null)
		{
			ProcessHit(damageDealer);
		}
	}

	private void ProcessHit(Projectile damageDealer)
	{
		health.DealDamage(damageDealer.GetDamage());
		damageDealer.Hit();
	}

	void Die()
	{
		Destroy(gameObject);
	}

	// Update is called once per frame
	void Update () {
		transform.Translate(Vector2.left * currentSpeed * Time.deltaTime );
		UpdateAnimationState();
	}

	private void UpdateAnimationState()
	{
		if (!currentTarget)
		{
			gameObject.GetComponent<Animator>().SetBool("isAttacking", false);
		}
	}

	public void SetMovementSpeed(float speed)
	{
		currentSpeed = speed;
	}

	public void Attack(GameObject target)
	{
		gameObject.GetComponent<Animator>().SetBool("isAttacking", true);
		currentTarget = target;
	}

	public void StrikeCurrentTarget(float damage)
	{
		if (!currentTarget) { return; };
		Health health = currentTarget.GetComponent<Health>();
		if (health)
		{
			health.DealDamage(damage);
		}
	}

}
