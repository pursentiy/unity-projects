﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AttackerSpawner : MonoBehaviour {

	bool spawn = true;
	float timer = 0f;
	[SerializeField] float minSpawnDelay = 1f;
	[SerializeField] float maxSpawnDelay = 5f;
	[SerializeField] Attacker[] attackerPrefabArray;

	// Use this for initialization
	IEnumerator Start()
	{
		while (spawn)
		{
			yield return new WaitForSeconds(Random.Range(minSpawnDelay, maxSpawnDelay));
			SpawnAttacker();
		}
	}

	private void SpawnAttacker()
	{
		int attackerindex = Random.Range(0, attackerPrefabArray.Length);
		Spawn(attackerPrefabArray[attackerindex]);
	}

	private void Spawn(Attacker attackerToSpawn)
	{
		Attacker newAttacker = Instantiate
	(attackerToSpawn, transform.position, transform.rotation) as Attacker;
		newAttacker.transform.parent = transform;
	}

	public void StopSpawning()
	{
		spawn = false;
	}
}
