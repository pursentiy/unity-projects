﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseCollider : MonoBehaviour {

	LifesScript lifesScript;

	private void Start()
	{
		lifesScript = FindObjectOfType<LifesScript>();
	}

	private void OnTriggerEnter2D(Collider2D attacker)
	{
		lifesScript.ReduceLife();
		StartCoroutine(DestroyAttacker(attacker.gameObject));
	}

	private IEnumerator DestroyAttacker(GameObject attacker)
	{
		yield return new WaitForSeconds(5f);
		Destroy(attacker);
	}
}
