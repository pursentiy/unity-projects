﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefenderButton : MonoBehaviour {

	[SerializeField] Defender defenderPrefub;

	private void Start()
	{
		LabelButtonReadCost();
	}

	private void LabelButtonReadCost()
	{
		Text costText = GetComponentInChildren<Text>();
		if (!costText)
		{
			Debug.LogError("Has no cost text");
		}
		else
		{
			costText.text = defenderPrefub.GetStarCost().ToString();
		}
	}

	private void OnMouseDown()
	{
		PickUp();
		FindObjectOfType<DefenderSpawner>().SetSelectedDefender(defenderPrefub);
	}

	private void PickUp()
	{
		var buttons = FindObjectsOfType<DefenderButton>();
		foreach (DefenderButton button in buttons)
		{
			button.GetComponent<SpriteRenderer>().color = new Color32(84, 84, 84, 255);
		}
		gameObject.GetComponent<SpriteRenderer>().color = Color.white;
	}
}
