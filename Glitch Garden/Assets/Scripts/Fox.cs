﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fox : MonoBehaviour {

	GameObject otherObject;


	private void OnTriggerEnter2D(Collider2D collision)
	{
		otherObject = collision.gameObject;

		if (otherObject.GetComponent<GraveStone>())
		{
			GetComponent<Animator>().SetTrigger("JumpTrigger");
		}
		else if (otherObject.GetComponent<Defender>())
		{
			GetComponent<Attacker>().Attack(otherObject);
		}
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
