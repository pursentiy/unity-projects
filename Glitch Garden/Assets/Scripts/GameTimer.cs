﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour {

	[Tooltip("Our level timer in SECONDS")]
	[SerializeField] float levelTime = 10;

	bool timerFinished;
	bool triggerLevelFinished = false;

	private void Update()
	{
		if (triggerLevelFinished) { return; }
		GetComponent<Slider>().value = Time.timeSinceLevelLoad / levelTime;

		timerFinished = (Time.timeSinceLevelLoad >= levelTime);
		if (timerFinished)
		{
			triggerLevelFinished = true;
			FindObjectOfType<LevelController>().LevelTimerFinished();
		}
	}
}
