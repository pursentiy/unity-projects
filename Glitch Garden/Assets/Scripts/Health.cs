﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

	[SerializeField] float health = 100f;
	[SerializeField] GameObject deathVFX;

	// Use this for initialization
	void Start () {
		
	}

	public void DealDamage(float damage)
	{
		health -= damage;
		if (health <= 0)
		{
			Die();
		}
	}

	public void Die()
	{
		TriggerDeathVFX();
		Destroy(gameObject);
	}
	
	private void TriggerDeathVFX()
	{
		if (!deathVFX) { return; }
		GameObject deathVFXObject = Instantiate(deathVFX, transform.position, transform.rotation);
		Destroy(deathVFXObject, 1f);
	}
}
