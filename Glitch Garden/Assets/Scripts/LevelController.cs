﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {

	[SerializeField] GameObject winLabel;
	[SerializeField] GameObject loseLabel;
	int numberOfAttackers = 0;
	bool ifTimerFinished = false;

	[SerializeField] float waitToLoad = 5f;


	// Use this for initialization
	void Start () {
		winLabel.SetActive(false);
		loseLabel.SetActive(false);
	}

	public void LevelTimerFinished()
	{
		ifTimerFinished = true;
		StopSpawners();
	}

	private void StopSpawners()
	{
		AttackerSpawner[] spawnerArray = FindObjectsOfType<AttackerSpawner>();
		foreach(AttackerSpawner spawner in spawnerArray)
		{
			spawner.StopSpawning();
		}
	}

	public void AttackerSpawned()
	{
		numberOfAttackers++;
	}

	public void AttackerKilled()
	{
		numberOfAttackers--;
			if(numberOfAttackers <= 0 && ifTimerFinished)
		{
			StartCoroutine(HandleWinCondition());
		}
	}

	IEnumerator HandleWinCondition()
	{
		winLabel.SetActive(true);
		GetComponent<AudioSource>().Play();
		GameObject musicPlayer = GameObject.Find("Music Player");
		musicPlayer.GetComponent<AudioSource>().Stop();
		yield return new WaitForSeconds(waitToLoad);
		musicPlayer.GetComponent<AudioSource>().Play();
		FindObjectOfType<LevelLoader>().StartMenu();
	}

	public void HandleLosoeConditions()
	{
		loseLabel.SetActive(true);
		Time.timeScale = 0;
	}
}
