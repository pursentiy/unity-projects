﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour {

	[SerializeField] int timeToWait = 4;
	int currentSceneIndex;

	// Use this for initialization
	void Start () {
		currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
		if (currentSceneIndex == 0)
		{
			StartCoroutine(WaitForTime());
		}
	}

	private IEnumerator WaitForTime()
	{
		yield return new WaitForSeconds(timeToWait);
		LoadNextScene();
	}

	public void RestartScene()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene(currentSceneIndex);
	}

	public void LoadNextScene()
	{
		SceneManager.LoadScene(currentSceneIndex + 1);
	}

	public void OptionScreen()
	{
		SceneManager.LoadScene("Option Screen");
	}


	public void StartGame()
	{
		SceneManager.LoadScene(2);
	}

	public void StartMenu()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene(1);
	}

	public void QuitGame()
	{
		Application.Quit();
	}

	// Update is called once per frame
	void Update () {
		
	}
}
