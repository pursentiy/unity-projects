﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifesScript : MonoBehaviour {

	int difficultyLifesNumber;
	int lifesNumber = 4;

	private void Start()
	{
		difficultyLifesNumber = (int)PlayerPrefsController.GetDifficulty() ;
		if (difficultyLifesNumber == 0) { return; }
		for (int index = 0; index < difficultyLifesNumber; index++)
		{
			ReduceLife();
		}
	}

	public void ReduceLife()
	{
		if (lifesNumber >= 0)
		{
			GameObject lifeToDestroy = transform.GetChild(lifesNumber).gameObject;
			Destroy(lifeToDestroy);
			lifesNumber--;
		}
		CheckLife();
	}


	private void CheckLife()
	{
		if(lifesNumber < 0)
		{
			FindObjectOfType<LevelController>().HandleLosoeConditions();
		}
	}
}
