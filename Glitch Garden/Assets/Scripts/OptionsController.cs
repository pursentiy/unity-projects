﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour {

	[SerializeField] Slider volumeSlider;
	[SerializeField] Slider difficultySlider;
	[SerializeField] float defaultVolume = 0.5f;
	[SerializeField] int defaultDifficulty = 1;

	// Use this for initialization
	void Start () {
		volumeSlider.value = PlayerPrefsController.GetMasterVolume();
		difficultySlider.value = PlayerPrefsController.GetDifficulty();
		Debug.Log(difficultySlider.value);
	}
	
	// Update is called once per frame
	void Update () {
		var musicPlayer = FindObjectOfType<MusicPlayer>();
		if (musicPlayer)
		{
			musicPlayer.SetVolume(volumeSlider.value);
		}
		else
		{
			Debug.LogWarning("No music player found...Start from Splash SCreen?");
		}
	}


	public void SafeAndExit()
	{
		PlayerPrefsController.SetMasterVolume(volumeSlider.value);
		PlayerPrefsController.SetDifficulty(difficultySlider.value);
		FindObjectOfType<LevelLoader>().StartMenu();
	}

	public void Defaults()
	{
		volumeSlider.value = defaultVolume;
		difficultySlider.value = defaultDifficulty;
	}
}
