﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	[SerializeField] float currentSpeed = 1f;
	[SerializeField] int projectileDamage = 50;


	public int GetDamage() { return projectileDamage; }

	public void Hit()
	{
		Destroy(gameObject);
	}

	// Update is called once per frame
	void Update () {
		transform.Translate(Vector2.right * currentSpeed * Time.deltaTime);
		//transform.Rotate(new Vector3(0, 0, 1) * currentSpeed * Time.deltaTime);
	}
}
