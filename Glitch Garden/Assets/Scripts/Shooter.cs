﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {

	[SerializeField] GameObject projectile, gun;
	AttackerSpawner myLineSpawner;
	Animator animator;
	GameObject projectilesParent;
	const string PROJECTILE_PARENT_NAME = "Projectiles";

	private void CreateProjectileParent()
	{
		projectilesParent = GameObject.Find(PROJECTILE_PARENT_NAME);
		if (!projectilesParent)
		{
			projectilesParent = new GameObject(PROJECTILE_PARENT_NAME);
		}
	}

	private void Start()
	{
		CreateProjectileParent();
		SetLineSpawner();
		animator = GetComponent<Animator>();
	}

	private void Update()
	{
		if (IsAttackerInLine())
		{
			animator.SetBool("isAttacking", true);
		}
		else
		{
			animator.SetBool("isAttacking", false);
		}
	}


	private void SetLineSpawner()
	{
		AttackerSpawner[] spawners = FindObjectsOfType<AttackerSpawner>();

		foreach(AttackerSpawner spawner in spawners)
		{
			bool IsCloseEnough = (Mathf.Abs(spawner.transform.position.y - transform.position.y) <= Mathf.Epsilon);
			if (IsCloseEnough)
			{
				myLineSpawner = spawner;
			}
		}
	}

	private bool IsAttackerInLine()
	{
		if(myLineSpawner.transform.childCount <= 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public void Fire()
	{
		GameObject newProjectile = Instantiate(projectile, gun.transform.position, Quaternion.identity) as GameObject;
		newProjectile.transform.parent = projectilesParent.transform;
	}
}
