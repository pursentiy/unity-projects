﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudScroll : MonoBehaviour {

	[SerializeField] public float scrollingSpeed = 1f;
	[SerializeField] float minTimeBetweenSpawning = 5f;
	[SerializeField] float maxTimeBetweenSpawning = 15f;
	[SerializeField] GameObject cloud;
	float timer;

	// Use this for initialization
	void Start ()
	{
		SettingTimer();
		
	}

	private void SettingTimer()
	{
		timer = Random.Range(minTimeBetweenSpawning, maxTimeBetweenSpawning);
	}

	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
		if(timer <= 0)
		{
			Instantiate(cloud, transform.position, Quaternion.identity);
			SettingTimer();
		}
	}
}
