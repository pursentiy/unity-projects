﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour {

	[SerializeField] float health = 100;
	[SerializeField] float shotCounter;
	[SerializeField] float minTimeBetweenShots = 0.2f;
	[SerializeField] float maxTimeBetweenShots = 3f;
	[SerializeField] float bulletSpeed = 2f;
	[SerializeField] int scoreValue = 20;
	[SerializeField] Vector3 paddingBullet = new Vector3 (0, -10f, 0);
	[SerializeField] GameObject bullet;
	[SerializeField] AudioClip shot;
	[SerializeField] AudioClip deathSound;
	[SerializeField] [Range(0,1)] float deathSoundVolume = 0.3f;
	[SerializeField] [Range(0, 1)] float shotSoundVolume = 0.3f;

	GameSession gameSession;

	// Use this for initialization
	void Start () {
		shotCounter = Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
		gameSession = FindObjectOfType<GameSession>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		CountDownAndShoot();
	}

	private void CountDownAndShoot()
	{
		shotCounter -= Time.deltaTime;
		if(shotCounter <= 0f)
		{
			Fire();
			shotCounter = Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
		}
	}

	private void Fire()
	{
		var bulletObj = Instantiate(bullet, transform.position, Quaternion.identity) as GameObject;
		bulletObj.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -bulletSpeed);
		AudioSource.PlayClipAtPoint(shot, Camera.main.transform.position, shotSoundVolume);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		DamageDealer damageDealer = collision.GetComponent<DamageDealer>();
		if (!damageDealer){ return;}
			ProcessHit(damageDealer);
	}

	private void ProcessHit(DamageDealer damageDealer)
	{
		health -= damageDealer.GetDamage();
		damageDealer.Hit();
		if (health < 0)
		{
			Die();
			
		}
	}

	private void Die()
	{
		gameSession.AddToScore(scoreValue);
		Destroy(gameObject);
		AudioSource.PlayClipAtPoint(deathSound, Camera.main.transform.position, deathSoundVolume);

	}
}
