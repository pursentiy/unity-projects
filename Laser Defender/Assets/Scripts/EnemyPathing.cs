﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPathing : MonoBehaviour {

	WaveConfig waveConfig;
	List<Transform> waypoints;
	int waypointIndex = 0;


	// Use this for initialization
	void Start () {
		waypoints = waveConfig.GetWaypoins();
		transform.position = waypoints[waypointIndex].transform.position;
	}
	
	// Update is called once per frame
	void Update ()
	{
		Move();
	}

	public void SetWaveConfig(WaveConfig waveConfig)
	{
		this.waveConfig = waveConfig;
	}

	private void Move()
	{
		if (waypoints != null)
		{
			if (waypointIndex < waypoints.Count)
			{
				var targetPosotion = waypoints[waypointIndex].transform.position;
				var movementThisFrame = waveConfig.GetMoveSpeed() * Time.deltaTime;
				transform.position = Vector2.MoveTowards(transform.position, targetPosotion, movementThisFrame);

				if (transform.position == targetPosotion)
				{
					waypointIndex++;
				}
			}
			else
			{
				Destroy(gameObject);
			}
		}
	}
}
