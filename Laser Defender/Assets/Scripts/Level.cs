﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour {

	public void GameStart()
	{
		SceneManager.LoadScene(1);
		FindObjectOfType<GameSession>().ResetGame();
	}

	public void Death()
	{
		StartCoroutine(DeathDelay());
		//FindObjectOfType<GameSession>().ResetGame();
	}

	private IEnumerator DeathDelay()
	{
		yield return new WaitForSeconds(3f);
		SceneManager.LoadScene(2);
	}

	public void RestartGame()
	{
		SceneManager.LoadScene(1);
		FindObjectOfType<GameSession>().ResetGame();
	}

	public void QuitGame()
	{
		Application.Quit();
	}
}
