﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	[Header("Player")]
	[SerializeField] float moveSpeed = 10f;
	[SerializeField] GameObject laserPrefub;
	[SerializeField] int health = 200;

	[SerializeField] float projectileSpeed = 10f;
	[SerializeField] float projecileFiringPerios = 0.1f;

	[SerializeField] AudioClip shot;
	[SerializeField] AudioClip deathSound;
	[SerializeField] [Range(0, 1)] float deathSoundVolume = 0.3f;
	[SerializeField] [Range(0, 1)] float shotSoundVolume = 0.3f;

	Coroutine firingCoroutine;

	private float paddingX = 0.27f;
	private float paddingY = 0.47f;

	float xMin;
	float xMax;

	float yMin;
	float yMax;

	// Use this for initialization
	void Start () {
		SetUpMoveBoundaries();
	}


	// Update is called once per frame
	void Update () {
		Move();
		Shoot();
	}

	private void Shoot()
	{
		if (Input.GetButtonDown("Fire1"))
		{
			firingCoroutine = StartCoroutine(FireContinuously());
		}
		if (Input.GetButtonUp("Fire1"))
		{
			StopCoroutine(firingCoroutine);
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		DamageDealer damageDealer = collision.GetComponent<DamageDealer>();
		if (collision.tag == "Enemy") {
			health = 0;
			DestroyPlayer();
		}
		else if (!damageDealer)
		{
			return;
		}
		ProcessHit(damageDealer);
	}

	private void ProcessHit(DamageDealer damageDealer)
	{
		health -= damageDealer.GetDamage();
		damageDealer.Hit();
		if (health < 0)
		{
			DestroyPlayer();
		}
	}

	private void DestroyPlayer()
	{
		Destroy(gameObject);
		AudioSource.PlayClipAtPoint(deathSound, Camera.main.transform.position, deathSoundVolume);
		FindObjectOfType<Level>().Death();
	}

	IEnumerator FireContinuously()
	{
		while (true)
		{
			GameObject laser = Instantiate(laserPrefub, transform.position, Quaternion.identity) as GameObject;
			laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, projectileSpeed);
			AudioSource.PlayClipAtPoint(shot, Camera.main.transform.position, shotSoundVolume);
			yield return new WaitForSeconds(projecileFiringPerios);
		}
	}

	private void Move()
	{
		var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
		var deltaY = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;

		var newXPos = Mathf.Clamp(transform.position.x + deltaX, xMin, xMax);
		var newYPos = Mathf.Clamp(transform.position.y + deltaY, yMin, yMax);
		transform.position = new Vector2(newXPos, newYPos);
	}

	private void SetUpMoveBoundaries()
	{
		Camera gameCamera = Camera.main;
		xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + paddingX;
		xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - paddingX;

		yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + paddingY;
		yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 0.75f, 0)).y;
	}

	public int GetHealth()
	{
		return health;
	}
}
