﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy Wave Config")]
public class WaveConfig : ScriptableObject {

	[SerializeField] GameObject enemyPrefub;
	[SerializeField] GameObject pathPrefab;
	[SerializeField] float timeBetweenSpawns = 0.5f;
	[SerializeField] float spawnRandomFactor = 0.3f;
	[SerializeField] int numberOfEnemies = 5;
	[SerializeField] float moveSpeed = 2f;

	public GameObject GetEnemyPrefub(){ return enemyPrefub; }

	public List<Transform> GetWaypoins()
	{
		var waveWayPoints = new List<Transform>();
		foreach(Transform child in pathPrefab.transform)
		{
			waveWayPoints.Add(child);
		}

		return waveWayPoints;
	}

	public float GetTimeBetweenSpawns() { return timeBetweenSpawns; }

	public float GetSpawnRandomFactor() { return spawnRandomFactor; }

	public int GetNumberOFEnemies() { return numberOfEnemies; }

	public float GetMoveSpeed() { return moveSpeed; }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
