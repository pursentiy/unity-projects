﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
	[SerializeField] float timeToWait = 4;
	int currentSceneIndex;

	void Start()
	{
		currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
	}

	public void LoadNextLevel()
	{
		int Index = currentSceneIndex + 1;
		if(Index >= SceneManager.sceneCountInBuildSettings)
		{
			Index = 0;
		}
		StartCoroutine(WaitForTime(Index));
		
	}

	public void Restart()
	{
		StartCoroutine(WaitForTime(0));
	}

	private IEnumerator WaitForTime(int sceneIndex)
	{
		yield return new WaitForSeconds(timeToWait);
		SceneManager.LoadScene(sceneIndex);
	}
}
