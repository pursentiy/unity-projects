﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour {

	private Vector3 initialPosition;
	private float timer = 0;

	[Range(0, 1)]
	public float reduceCoef = 1;
	[SerializeField] Vector3 movementVector;
	
	// Use this for initialization
	void Start () {
		initialPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
			timer +=  Time.deltaTime/ reduceCoef;
		Vector3 offset = movementVector * Mathf.Sin(timer);
		transform.position = initialPosition + offset;
		
	}
}
