﻿using System;
using UnityEngine;

public class Rocket : MonoBehaviour {

	[SerializeField] float rcsThrust = 100f;
	[SerializeField] float acsThrust = 15f;

	private Rigidbody rigidbody;
	private AudioSource audioSource;
	private LevelManager levelManager;

	private bool detectCollision = true;
	public AudioClip[] audioClip;
	public ParticleSystem mainEngineParticles;
	public ParticleSystem successParticles;
	public ParticleSystem deathParticles;

	enum State { Alive, Dying, Transcending, Finish}
	State state = State.Alive;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		audioSource = GetComponent<AudioSource>();
		levelManager = FindObjectOfType<LevelManager>();
	}
	
	// Update is called once per frame
	void Update () {

		ProcessInput();
	}

	private void ProcessInput()
	{
		if (Debug.isDebugBuild)
		{
			RespondToDebugKeys();
		}

		if(state != State.Alive) { return; };
		Thrust();
		Rotate();
	}

	private void RespondToDebugKeys()
	{
		if (Input.GetKey(KeyCode.L))
		{
			levelManager.LoadNextLevel();
		}

		if (Input.GetKey(KeyCode.C))
		{
			detectCollision = !detectCollision;
		}
		
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (state != State.Alive || !detectCollision) { return; }

		switch (collision.gameObject.tag)
		{
			case "Friendly":
				Debug.Log("OK");
				break;
			case "Fuel":
				Debug.Log("Fuel");
				break;
			case "Finish":
				if (state == State.Finish) { break; };
				levelManager.LoadNextLevel();
				PlaySound(2);
				successParticles.Play();
				state = State.Finish;
				break;
			default:
				if(state == State.Finish)  { break; };
				levelManager.Restart();
				PlaySound(1);
				state = State.Dying;
				deathParticles.Play();
				break;
		}
	}


	private void Thrust()
	{
		float accelerationThisFrame = acsThrust * Time.deltaTime;
		if (Input.GetKey(KeyCode.Space))
		{
			ApplyThrust(accelerationThisFrame);
		}
		else if (Input.GetKeyUp(KeyCode.Space))
		{
			StopApplyingThrust();
		}
	}

	private void StopApplyingThrust()
	{
		audioSource.Stop();
		mainEngineParticles.Stop();
	}

	private void ApplyThrust(float accelerationThisFrame)
	{
		rigidbody.AddRelativeForce(Vector3.up * accelerationThisFrame);
		if (!audioSource.isPlaying)
		{
			PlaySound(0);
		}
		mainEngineParticles.Play();
	}

	private void Rotate()
	{
		
		float rotationThisFrame = rcsThrust * Time.deltaTime;
		float rotate = Input.GetAxis("Horizontal");
		if (Input.GetKey(KeyCode.A))
		{
			RotateManually(rotationThisFrame, rotate);
		}
		else if (Input.GetKey(KeyCode.D))
		{
			RotateManually(rotationThisFrame, rotate);
		}

		
	}

	private void RotateManually(float rotationThisFrame, float rotate)
	{
		rigidbody.freezeRotation = true;
		transform.Rotate(new Vector3(0, 0, -rotate) * rotationThisFrame);
		rigidbody.freezeRotation = false;
	}

	private void PlaySound(int index)
	{
		audioSource.clip = audioClip[index];
		audioSource.Play();
	}
}
