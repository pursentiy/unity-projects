﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BaseHealth : MonoBehaviour {

	[SerializeField] int baseHealth = 10;
	[SerializeField] Text health;

	private void Start()
	{
		health.text = baseHealth.ToString();
	}

	public void ReduceHealth()
	{
		baseHealth--;
		health.text = baseHealth.ToString();
		if (baseHealth <= 0)
		{
			baseHealth = 0;
			Invoke("DestroyBase", 2f);
		}
	}

	private void DestroyBase()
	{
		print("Game is over");
		SceneManager.LoadScene(0);
	}
}
