﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[SelectionBase]
[RequireComponent(typeof(Waypoint))]
public class CubeEditor : MonoBehaviour {

	Waypoint waypoint;
	int gridSize;
	

	private void Awake()
	{
		waypoint = GetComponent<Waypoint>();
		gridSize = waypoint.GetGridSize();
	}

	// Update is called once per frame
	void Update ()
	{
		SnapToGrid();
		//UpdateLabel();

	}

	private void SnapToGrid()
	{
		Vector2Int gridPos = waypoint.GetGridPos();
		transform.position = new Vector3(gridPos.x * gridSize, 0f, gridPos.y * gridSize);
	}

	private void UpdateLabel()
	{
		TextMesh textMesh = GetComponentInChildren<TextMesh>();
		Vector2Int gridPos = waypoint.GetGridPos();
		string labelText = (gridPos.x * gridSize / gridSize).ToString() + ',' + (gridPos.y * gridSize/ gridSize).ToString();
		textMesh.text = labelText;
		gameObject.name = labelText;
	}
}
