﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

	[SerializeField] int health = 100;
	[SerializeField] int amountOfScore = 15;
	[SerializeField] ParticleSystem enemyHitPrefab;
	[SerializeField] ParticleSystem enemyDeathPrefab;

	private void OnParticleCollision(GameObject other)
	{

		print(other.name);
		GetDamage(10);
		//Destroy(other);

	}

	public void GetDamage(int amount)
	{
		health -= amount;
		enemyHitPrefab.Play();
		if(health <= 0)
		{
			Invoke("KillObject", 0.5f);
		}
	}

	private void KillObject()
	{
		var vfx = Instantiate(enemyDeathPrefab, transform.position, transform.rotation);
		float destryDelay = vfx.main.duration;
		FindObjectOfType<PlayerScore>().AddScore(amountOfScore);
		Destroy(vfx.gameObject, destryDelay);
		Destroy(gameObject);
	}

	// Update is called once per frame
	void Update () {
		
	}
}
