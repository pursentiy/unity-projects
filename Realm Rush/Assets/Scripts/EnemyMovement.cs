﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

	[SerializeField] float movementPeriod = .5f;
	[SerializeField] ParticleSystem enemySelfDistructionPrefab;

	// Use this for initialization
	void Start () {
		PathFinder pathFinder = FindObjectOfType<PathFinder>();
		var path = pathFinder.GetPath();
		StartCoroutine(FollowPath(path));
	}

	private IEnumerator FollowPath(List<Waypoint> path)
	{
		foreach (Waypoint waypoint in path)
		{
			transform.position = waypoint.gameObject.transform.position; //todo offset for position
			yield return new WaitForSeconds(movementPeriod);
		}

		SelfDestruct();
		FindObjectOfType<BaseHealth>().ReduceHealth();

	}

	private void SelfDestruct()
	{
		var vfx = Instantiate(enemySelfDistructionPrefab, transform.position, transform.rotation);
		float destryDelay = vfx.main.duration;
		Destroy(vfx.gameObject, destryDelay);
		Destroy(gameObject);
	}
}
