﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	[SerializeField] EnemyMovement enemyPrefab;
	[SerializeField] float secondsBetweenSpawns = 3f;
	[SerializeField] Transform enemiesParent;

	// Use this for initialization
	void Start () {
		StartCoroutine(SpawnEnemies());
	}
	
	private IEnumerator SpawnEnemies()
	{
		while (true)
		{
			yield return new WaitForSeconds(secondsBetweenSpawns);
			var spawnedEnemie = Instantiate(enemyPrefab, transform.position, transform.rotation);
			spawnedEnemie.transform.parent = enemiesParent;
		}

	}
}
