﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour
{

	[SerializeField] Waypoint startWaypoint, endWaypoint;
	private List<Waypoint> path = new List<Waypoint>();

	Queue<Waypoint> queue = new Queue<Waypoint>();

	bool isRunning = true;
    public bool IsPathVisible = false;
	Waypoint searchCenter;

	Dictionary<Vector2Int, Waypoint> grid = new Dictionary<Vector2Int, Waypoint>();
	Vector2Int[] directions = {
		Vector2Int.up, //up
		Vector2Int.right, //right
		Vector2Int.down,
		Vector2Int.left //left
	};

	public List<Waypoint> GetPath()
	{
		if(path.Count == 0)
		{
			CalculatePath();
		}
			return path;
	}

	private void CalculatePath()
	{
		LoadBlocks();
		ColorStartAndEndWaypoints();
		BreadthFirstSearch();
		CreatePath();
		if (!IsPathVisible) {
			MakePathVisible();
		}
		
	}

	private void BreadthFirstSearch()
	{
		queue.Enqueue(startWaypoint);

		while (queue.Count > 0 && isRunning)
		{
			searchCenter = queue.Dequeue();
			if (HaltIfEndFound(searchCenter))
			{
				print("Points are the same");
				return;
			}
			ExploreNeighbours();
			searchCenter.isExplored = true;
		}
	}

	private void ExploreNeighbours()
	{
		if (!isRunning) { return; }
		foreach (Vector2Int direction in directions)
		{
			Vector2Int explorationCoordinates = searchCenter.GetGridPos() + direction;
			if (grid.ContainsKey(explorationCoordinates))
			{
				QueueNewNeighbours(explorationCoordinates);
			}
		}
	}

	private void QueueNewNeighbours(Vector2Int explorationCoordinates)
	{
		Waypoint neighbour = grid[explorationCoordinates];

		if (!neighbour.isExplored)
		{
			grid[explorationCoordinates].exploredFrom = searchCenter;
			queue.Enqueue(grid[explorationCoordinates]);
			if (HaltIfEndFound(neighbour))
			{
				isRunning = false;
				neighbour.SetTopColor(Color.yellow);
				print("Target was found");
				return;
			}
		}
	}

	private void CreatePath()
	{
		SetAsPath(endWaypoint);

		Waypoint previous = endWaypoint.exploredFrom;
		//previous.transform.GetChild(2).gameObject.SetActive(true);
		while (previous != startWaypoint)
		{
			SetAsPath(previous);
			previous = previous.exploredFrom;
		}
		SetAsPath(startWaypoint);
		path.Reverse();

	}

	private void SetAsPath(Waypoint waypoint)
	{
		path.Add(waypoint);
		waypoint.isPlaceable = false;
	}

	private bool HaltIfEndFound(Waypoint searchCenter)
	{
		if (searchCenter == endWaypoint)
		{
			isRunning = false;
		}
		return (searchCenter == endWaypoint);
	}

	private void LoadBlocks()
	{
		var waypoints = FindObjectsOfType<Waypoint>();
		foreach (Waypoint waypoint in waypoints)
		{
			var gridPos = waypoint.GetGridPos();
			if (grid.ContainsKey(gridPos))
			{
				Debug.LogWarning("Skipping overlapping block " + waypoint);
			}
			else
			{
				grid.Add(gridPos, waypoint);
			}
		}
	}

	private void ColorStartAndEndWaypoints()
	{
		startWaypoint.SetTopColor(Color.red);
		endWaypoint.SetTopColor(Color.yellow);
	}

	private void MakePathVisible()
	{
		foreach(Waypoint waypoint in path)
		{
			waypoint.transform.GetChild(2).gameObject.SetActive(true);
		}
		IsPathVisible = true;
	}
}
