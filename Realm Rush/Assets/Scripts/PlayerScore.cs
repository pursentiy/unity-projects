﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour {

	[SerializeField] Text scoreText;
	[SerializeField] int playerScore;

	// Use this for initialization
	void Start () {
		scoreText.text = playerScore.ToString();
	}
	
	public void AddScore(int value)
	{
		playerScore += value;
		scoreText.text = playerScore.ToString();
	}
}
