﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

	[SerializeField] Transform objectToPan;
	EnemyMovement targetEnemy;
	[SerializeField] float maxDistanceToTrack = 20f;
	[SerializeField] ParticleSystem shootParticles;

	bool isEnemyInRange = false;

	public Waypoint baseWaypoint;

	// Use this for initialization
	void Start () {
		SetGunsActive(false);
	}
	
	// Update is called once per frame
	void Update ()
	{
		SetTargetEnemy();
		if (targetEnemy == null) { return; }
			DetectIfEnemyInRange();
			if (isEnemyInRange)
			{
				TrackEnemy();
			}
			SetGunsActive(isEnemyInRange);
	}

	public void SetBasePoint(Waypoint waypoint)
	{
		baseWaypoint = waypoint;
	}

	private void SetTargetEnemy()
	{
		EnemyMovement[] targetsEnemy = FindObjectsOfType<EnemyMovement>();
		if(targetsEnemy.Length == 0) { return; }
		EnemyMovement closestEnemy = targetsEnemy[0];
		foreach (EnemyMovement target in targetsEnemy)
			{
				if(GetDistance(closestEnemy) > GetDistance(target))
				{
				closestEnemy = target;
				}
			}
		targetEnemy = closestEnemy;
	}

	private void SetGunsActive(bool isActive)
	{
			var emissionModule = shootParticles.emission;
			emissionModule.enabled = isActive;
	}

	private void TrackEnemy()
	{
			objectToPan.LookAt(targetEnemy.transform);
	}

	private void DetectIfEnemyInRange()
	{
		float distanceToTarget = GetDistance(targetEnemy);
		isEnemyInRange = maxDistanceToTrack >= distanceToTarget;
	}

	private float GetDistance(EnemyMovement target)
	{
		return Vector3.Distance(transform.position, target.transform.position);
	}

}
