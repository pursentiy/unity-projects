﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour {

	[SerializeField] Tower towerPrefab;
	[SerializeField] int towerLimit = 5;
	public Transform towersParent;

	Queue<Tower> towerQueue = new Queue<Tower>();

	public void AddTower(Waypoint baseWaypoint)
	{

		var numtowers = towerQueue.Count;
		if(numtowers < towerLimit)
		{
			InstantiateNewTower(baseWaypoint);
		}
		else
		{
			MoveExistingTower(baseWaypoint);
		}
	}

	private void MoveExistingTower(Waypoint newBaseWaypoint)
	{
		var oldTower = towerQueue.Dequeue();
		oldTower.baseWaypoint.isPlaceable = true;
		newBaseWaypoint.isPlaceable = false;
		oldTower.transform.position = newBaseWaypoint.transform.position;
		oldTower.SetBasePoint(newBaseWaypoint);
		towerQueue.Enqueue(oldTower);
	}

	private void InstantiateNewTower(Waypoint baseWaypoint)
	{
		var newTower = Instantiate(towerPrefab, baseWaypoint.transform.position, Quaternion.identity);
		newTower.SetBasePoint(baseWaypoint);
		newTower.transform.parent = towersParent;
		baseWaypoint.isPlaceable = false;
		towerQueue.Enqueue(newTower);
	}
}
