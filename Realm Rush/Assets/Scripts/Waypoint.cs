﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour {

	[SerializeField] Color exploredColor;

	public bool isExplored = false;
	public bool isPlaceable = true;
	public Waypoint exploredFrom;

	private TowerFactory towerFactory;

	const int gridSize = 10;
	Vector2Int gridPos;


	// Use this for initialization
	void Start () {
		towerFactory = FindObjectOfType<TowerFactory>();
	}

	public Vector2Int GetGridPos()
	{
		return new Vector2Int(
			 Mathf.RoundToInt(transform.position.x / gridSize),
		     Mathf.RoundToInt(transform.position.z / gridSize)
			);
	}

	public int GetGridSize()
	{
		return gridSize;
	}

	public void SetTopColor(Color color)
	{
		MeshRenderer topMeshRenderer = transform.Find("Top").GetComponent<MeshRenderer>();
		topMeshRenderer.material.color = color;
	}

	// Update is called once per frame
	void Update () {
		//if (isExplored)
		//{
		//	SetTopColor(exploredColor);
		//}
	}

	void OnMouseOver()
	{

		if(Input.GetMouseButtonDown(0))
		{
			if (isPlaceable)
			{
				if (FindObjectOfType<PathFinder>().IsPathVisible)
				{
					SpawnTower();
				}
			}
			else
			{
				print("Cant place here");
			}
		}
	}

	private void SpawnTower()
	{
		
		towerFactory.AddTower(this);

	}
}
