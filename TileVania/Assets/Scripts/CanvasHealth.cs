﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasHealth : MonoBehaviour {

	private void Awake()
	{
		int canvasHealth = FindObjectsOfType<CanvasHealth>().Length;
		if (canvasHealth > 1)
		{
			Destroy(gameObject);
		}
		else
		{
			DontDestroyOnLoad(gameObject);
		}
	}

	public void RemoveHeart(int index)
	{
		Destroy(transform.GetChild(index).gameObject);
	}
	
}
