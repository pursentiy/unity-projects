﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickup : MonoBehaviour {

	public AudioClip coinPickupSFX;
	public int coinValue = 10;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		AudioSource.PlayClipAtPoint(coinPickupSFX, Camera.main.transform.position);
		FindObjectOfType<GameSession>().AddScore(coinValue);
		Destroy(gameObject);
	}
}
