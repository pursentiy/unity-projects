﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

	public float moveSpeed = 1f;

	Rigidbody2D myRigidBody;
	BoxCollider2D flipCollider;
	// Use this for initialization
	void Start () {
		myRigidBody = GetComponent<Rigidbody2D>();
		flipCollider = GetComponent<BoxCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if (IsFacingRight())
		{
			myRigidBody.velocity = new Vector2(moveSpeed, 0f);
		}
		else
		{
			myRigidBody.velocity = new Vector2(-moveSpeed, 0f);
		}
		
	}

	private bool IsFacingRight()
	{
		return transform.localScale.x > 0;
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
			transform.localScale = new Vector3(-Mathf.Sign(myRigidBody.velocity.x), 1f, 1f);
	}
}
