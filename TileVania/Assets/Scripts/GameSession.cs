﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSession : MonoBehaviour {

	public int playerLives = 3;
	public int playerScore;
	public Text scoreText;
	public GameObject lives;

	Menu menu;

	private void Awake()
	{
		int numGameSession = FindObjectsOfType<GameSession>().Length;
		if(numGameSession > 1)
		{
			Destroy(gameObject);
		}
		else
		{
			DontDestroyOnLoad(gameObject);
		}
	}

	// Use this for initialization
	void Start() {
		scoreText.text = playerScore.ToString();
		int canvasLifes = lives.transform.childCount;
		if (canvasLifes == playerLives) { return; }
		else
		{
			for(int x = playerLives - canvasLifes; x <= 0; x--)
			{
				Destroy(lives.transform.GetChild(x).gameObject);
			}
		}
	}
	
	public void ProcessPlayerDeath()
	{
		if(playerLives > 1)
		{
			TakeLife();
		}
		else
		{
			ResetGameSession();
		}
	}

	private void ResetGameSession()
	{
		SceneManager.LoadScene(0);
		Destroy(gameObject);
	}

	private void TakeLife()
	{
		playerLives--;
		Destroy(lives.transform.GetChild(playerLives).gameObject);
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void AddScore(int value)
	{
		playerScore += value;
		scoreText.text = playerScore.ToString();
	}
}
