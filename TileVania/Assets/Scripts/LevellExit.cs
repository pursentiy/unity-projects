﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevellExit : MonoBehaviour {

	public float waitForSeconds = 4f;
	public float levelExitSlowMoFactor = 0.2f;

	//void FinishLevel()
	//{
	//	Debug.Log("Level Finished");
	//}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		StartCoroutine(LoadNextLevel());
	}

	private IEnumerator LoadNextLevel()
	{
		Time.timeScale = levelExitSlowMoFactor;
		yield return new WaitForSeconds(waitForSeconds);
		Time.timeScale = 1f;
		var currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
		SceneManager.LoadScene(currentSceneIndex + 1);
	}
}


