﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

	public void StartFirstLevel()
	{
		if (FindObjectsOfType<ScenePersist>().Length > 0) { Destroy(FindObjectOfType<ScenePersist>().gameObject); }
		SceneManager.LoadScene(1);
	}

	public void Quit()
	{
		Application.Quit();
	}

	public void LoadMenu()
	{
		SceneManager.LoadScene(0);
	}

	public void Restart()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
}
