﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	// Config
	public float speed = 10f;
	public float jumpForce = 10f;
	public float climbingSpeed = 10f;
	public Vector2 deathKick = new Vector2 (25f, 25f);

	private float gravityScaleAtStart;


	// State
	bool isAlive = true;

	// Cached
	Rigidbody2D myRigidbody2D;
	Animator myAnimator;
	CapsuleCollider2D myBodyCollider;
	BoxCollider2D myFeet;

	// Use this for initialization
	void Start()
	{
		myRigidbody2D = GetComponent<Rigidbody2D>();
		myAnimator = GetComponent<Animator>();
		myBodyCollider = GetComponent<CapsuleCollider2D>();
		myFeet = GetComponent<BoxCollider2D>();
		gravityScaleAtStart = myRigidbody2D.gravityScale;
	}

	// Update is called once per frame
	void Update()
	{
		
		if (!isAlive) { return; }
		
			Run();
			FlipSprite();
			Jump();
			ClimbLadder();
		Die();
	}

	private void Run()
	{
		
		float controlThrow = Input.GetAxis("Horizontal") * speed * Time.deltaTime; // value is between -1 to +
		Vector2 playerVelocity = new Vector2(controlThrow, myRigidbody2D.velocity.y);
		myRigidbody2D.velocity = playerVelocity;
		//transform.position = new Vector3(transform.position.x + controlThrow, 0, 0);

		bool playerHasHorizontalSpeed = Mathf.Abs(myRigidbody2D.velocity.x) > Mathf.Epsilon;
		myAnimator.SetBool("Running", playerHasHorizontalSpeed);
	}

	private void Jump()
	{
		if (!myFeet.IsTouchingLayers(LayerMask.GetMask("Ground"))) { return; }
		
			if (Input.GetButtonDown("Jump"))
			{
				Vector2 jumpVelocityToAdd = new Vector2(0f, jumpForce);
				myRigidbody2D.velocity += jumpVelocityToAdd;
			}
	
	}

	private void ClimbLadder()
	{
		if (!myFeet.IsTouchingLayers(LayerMask.GetMask("Ladders")))
		{
			myAnimator.SetBool("Climbing", false);
			myRigidbody2D.gravityScale = gravityScaleAtStart;
			return;
		}
		
				float climbingThrow = Input.GetAxis("Vertical") * climbingSpeed * Time.deltaTime; // value is between -1 to +
				//Debug.Log(climbingThrow);
				Vector2 playerVelocity = new Vector2(myRigidbody2D.velocity.x, climbingThrow);
				myRigidbody2D.velocity = playerVelocity;
		myRigidbody2D.gravityScale = 0f;

		bool playerHasHorizontalSpeed = Mathf.Abs(myRigidbody2D.velocity.y) > Mathf.Epsilon;
		myAnimator.SetBool("Climbing", playerHasHorizontalSpeed);
	}

	private void FlipSprite()
	{
		bool playerHasHorizontalSpeed = Mathf.Abs(myRigidbody2D.velocity.x) > Mathf.Epsilon;
		if (playerHasHorizontalSpeed)
		{
			transform.localScale = new Vector3(Mathf.Sign(myRigidbody2D.velocity.x), 1, 1);
		}
	}

	private void Die()
	{
		if (myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Enemy", "Hazards")))
		{
			isAlive = false;
			myAnimator.SetTrigger("Diying");
			myRigidbody2D.velocity = deathKick;
			FindObjectOfType<GameSession>().ProcessPlayerDeath();
			//myRigidbody2D.freezeRotation = 
		}
	}
}
