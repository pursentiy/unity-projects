﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {

	public float speed = 10f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log("Update");
		MoveLeft();
		
	}

	private void MoveLeft()
	{
		var deltaX = Input.GetAxis("Horizontal") * speed * Time.deltaTime;

		Debug.Log("Pressed");
		transform.position = new Vector3(transform.position.x + deltaX, 0, 0);
	}
}
