﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {


	public AudioClip audioClip;


	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider collider)
	{
		if(collider.tag == "Player")
		{
			AudioSource.PlayClipAtPoint(audioClip, transform.parent.transform.position);
			Destroy(transform.parent.gameObject, 3f);
		}
	}
}
