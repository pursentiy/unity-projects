﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearArea : MonoBehaviour {

	public float timesSinceLastTrigger = 0f;

	private bool foundClearArea = false;

	// Use this for initialization
	void Start () {
		
	}

	private void OnTriggerStay(Collider other)
	{
		if(other.tag != "Player")
		{
			timesSinceLastTrigger = 0f;
		}
		
	}


	// Update is called once per frame
	void Update () {
			timesSinceLastTrigger += Time.deltaTime;
			if (timesSinceLastTrigger >= 1 && Time.realtimeSinceStartup > 15f && !foundClearArea)
			{
			SendMessageUpwards("OnFindClearArea");
			foundClearArea = true;
			}
		}
}
