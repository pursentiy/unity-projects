﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Daycycle : MonoBehaviour {

	[Tooltip ("Number of minutes per secong that pass, try 60")]
	public float minutesPerSecond = 60;

	private Quaternion startRotation;
	float timer = 0;

	// Use this for initialization
	void Start () {
		timer = Time.deltaTime;
		startRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
		float angleThisFrame = Time.deltaTime / 360 * minutesPerSecond;
		transform.RotateAround(transform.position, Vector3.forward, angleThisFrame);
		//}

	}
}
