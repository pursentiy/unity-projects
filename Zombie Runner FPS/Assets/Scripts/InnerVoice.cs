﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InnerVoice : MonoBehaviour {

	public AudioClip whatHappeded;
	public AudioClip goodArea;

	private AudioSource innerVoice;

	// Use this for initialization
	void Start () {
		innerVoice = GetComponent<AudioSource>();
		innerVoice.clip = whatHappeded;
		innerVoice.PlayDelayed(5f);
	}
	

	public void OnFindClearArea()
	{
		//print(name + "OnFindClearArea");
		//innerVoice.clip = goodArea;
		//innerVoice.Play();

		Invoke("CallHeli", goodArea.length + 1f);
	}

	void CallHeli()
	{
		SendMessageUpwards("OnMakeInitialHeliCall");
	}
}
