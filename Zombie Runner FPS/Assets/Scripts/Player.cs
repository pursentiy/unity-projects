﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public Transform playerSpawnPoints;
	public bool reSpawn = false;
	public GameObject landingArea;

	private Transform[] spawnPoints;
	private bool lastRespawnToggle = false;
	

	private void Update()
	{
		if(lastRespawnToggle != reSpawn)
		{
			ReSpawn();
			reSpawn = false;
		}
		else
		{
			lastRespawnToggle = reSpawn;
		}
	}

	private void Start()
	{
		spawnPoints = playerSpawnPoints.GetComponentsInChildren<Transform>();
		//BroadcastMessage("WhatHappended");
	}

	private void ReSpawn()
	{
			int randomSpot = UnityEngine.Random.Range(1, spawnPoints.Length);
			transform.position = spawnPoints[randomSpot].transform.position;
	}

	void OnFindClearArea()
	{

		Debug.Log("OnFindClearArea");
		Invoke("DropFlare", 3f);
	}

	void DropFlare()
	{
		Instantiate(landingArea, transform.position, transform.rotation);
	}

}
