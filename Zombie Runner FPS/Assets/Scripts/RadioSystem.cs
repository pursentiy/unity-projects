﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioSystem : MonoBehaviour {

	public AudioClip initialHeliCall;
	public AudioClip initialCallReply;

	private AudioSource audio;

	private void Start()
	{
		audio = GetComponent<AudioSource>();
	}

	void OnMakeInitialHeliCall()
	{
		//audio.clip = initialHeliCall; 
		//audio.Play();

		Invoke("InitialCallReply", initialHeliCall.length + 1f);
	}

	void InitialCallReply()
	{
		//audio.clip = initialCallReply;
		//audio.Play();
		BroadcastMessage("OnDispatchHelicopter");
	}
}
