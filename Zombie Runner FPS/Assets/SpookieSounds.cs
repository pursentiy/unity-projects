﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpookieSounds : MonoBehaviour {

	public AudioClip[] spookieSounds;

	private AudioSource audio;
	float timer = 0;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if(timer > 15)
		{
			Debug.Log("Sound was played");
			audio.clip = spookieSounds[Random.Range(0, spookieSounds.Length - 1)];
			audio.Play();
			timer = 0;
		}
	}
}
